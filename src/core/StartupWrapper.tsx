import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {startupAction} from '../adapters/redux/startup';
import {
  setUserOnlineStatusAction,
  userIdSelector,
} from '../adapters/redux/user';

export const StartupWrapper: React.FC = ({children}) => {
  const dispatch = useDispatch();
  const uuid = useSelector(userIdSelector);
  useEffect(() => {
    dispatch(setUserOnlineStatusAction(true, uuid!));
    dispatch(startupAction());
    // eslint-disable-next-line
	}, [])

  return <>{children}</>;
};
