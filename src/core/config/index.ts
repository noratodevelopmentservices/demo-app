import {Platform, NativeModules} from 'react-native';

export const isIOS = Platform.OS === 'ios';

export const version = '';

export const getLanguage = () => {
  let locale: string;

  if (isIOS) {
    locale = NativeModules.SettingsManager.settings.AppleLocale;
    if (!locale) {
      // iOS 13 workaround, take first of AppleLanguages array  ["en", "en-NZ"]
      locale = NativeModules.SettingsManager.settings.AppleLanguages[0];
    }
  } else {
    locale = NativeModules.I18nManager.localeIdentifier;
  }

  return locale;
};

export const isMock = false;
