import moment from 'moment';
import {useInterval} from './UseInterval';
import {useState} from 'react';
import {UserServerTimeDifference} from './UseServerTimeDifference';

export const UseCountDownLabel = (timestamp: number) => {
  const serverTimeDifference = UserServerTimeDifference();
  const [timeLabel, setCurrentTimeLabel] = useState('');
  const getTimeLabel = (duration: moment.Duration) => {
    if (duration.days() > 0) {
      return `${duration.days()} days`;
    }
    if (duration.hours() > 0) {
      return `${duration.hours()}h ${duration.minutes()}m`;
    }

    if (duration.minutes() > 0) {
      return `${duration.minutes()}m ${duration.seconds()}s`;
    }
    return `${duration.seconds()}s`;
  };
  useInterval(() => {
    let duration = moment.duration(
      timestamp -
        new Date().getTime() +
        (serverTimeDifference ? serverTimeDifference : 0),
      'milliseconds',
    );
    const newTimeLabel = getTimeLabel(duration);
    if (newTimeLabel !== timeLabel) {
      setCurrentTimeLabel(newTimeLabel);
    }
  }, 1000);

  return timeLabel;
};
