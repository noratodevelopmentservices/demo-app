import {useSelector} from 'react-redux';
import {serverTimeDifferenceSelector} from '../../adapters/redux/user';

export const UserServerTimeDifference = () => {
  return useSelector(serverTimeDifferenceSelector);
};
