import React, {useRef, useState} from 'react';
import {isIOS} from '@core';
import {Animated} from 'react-native';
import {styles} from './AnimatedBackgroundImageStyles';

interface AnimatedBackgroundImageProps {
  imageSource: string;
}

export const AnimatedBackgroundImage: React.FC<AnimatedBackgroundImageProps> = ({
  imageSource,
}) => {
  const backgroundValueRef = useRef(new Animated.Value(0));
  const [backgroundValue] = useState(backgroundValueRef.current);
  Animated.sequence([
    Animated.delay(2000),
    Animated.timing(backgroundValue, {
      toValue: 600,
      duration: 12000,
      useNativeDriver: true,
    }),
    Animated.delay(2000),
  ]).start(() => {
    console.log('navigate');
  });
  return (
    <Animated.Image
      blurRadius={isIOS ? 1 : 1}
      source={{uri: imageSource}}
      style={[
        styles.animatedBackground,
        {
          transform: [
            {
              translateX: backgroundValue,
            },
          ],
        },
      ]}
    />
  );
};
