import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  animatedBackground: {
    position: 'absolute',
    left: -600,
    height: '100%',
    width: '300%',
    resizeMode: 'cover',
  },
});
