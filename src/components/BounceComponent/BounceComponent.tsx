import React, {useRef, useState} from 'react';
import {Animated, Easing} from 'react-native';

export const BounceComponent: React.FC = ({children}) => {
  const bounceValueRef = useRef(new Animated.Value(0));
  const [bounceValue] = useState(bounceValueRef.current);
  const bounceDownAnimation = Animated.timing(bounceValue, {
    toValue: 10,
    useNativeDriver: true,
    duration: 1000,
    easing: Easing.bounce,
  });

  const bounceUpAnimation = Animated.timing(bounceValue, {
    toValue: 0,
    useNativeDriver: true,
    duration: 500,
  });

  Animated.loop(
    Animated.sequence([
      bounceDownAnimation,
      bounceUpAnimation,
      Animated.delay(1000),
    ]),
  ).start();

  return (
    <Animated.View style={{transform: [{translateY: bounceValue}]}}>
      {children}
    </Animated.View>
  );
};
