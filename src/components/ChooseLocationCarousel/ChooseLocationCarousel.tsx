import React from 'react';
import {Dimensions} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {Location, LocationIndexNumber, totalPoints} from '@entities';
import {ChooseLocationCard} from '@components';
import {scale} from '@themes';

const {width} = Dimensions.get('window');

interface ChooseLocationCarouselProps {
  locations: Location[];
  points?: {[key: string]: number};
  onPlayPressed: (location: string) => void;
}

export const ChooseLocationCarousel: React.FC<ChooseLocationCarouselProps> = ({
  locations,
  points,
  onPlayPressed,
}) => {
  const pnts = points ? totalPoints(points) : 0;
  const getFirstItem = () => {
    let firstItem = 0;
    locations.forEach((location: Location, index) => {
      if (pnts > location.unblocks) {
        firstItem = index;
      }
    });
    return firstItem;
  };

  const renderItem = ({item, index}: {item: Location; index: number}) => {
    return (
      <ChooseLocationCard
        active={pnts >= item.unblocks}
        location={item}
        pointsWon={points ? points[LocationIndexNumber[index]] : 0}
        onPlayPressed={() => onPlayPressed(item.name)}
      />
    );
  };
  return (
    <Carousel
      firstItem={getFirstItem()}
      removeClippedSubviews={false}
      data={locations}
      renderItem={renderItem}
      sliderWidth={width}
      itemWidth={width - scale(100)}
    />
  );
};
