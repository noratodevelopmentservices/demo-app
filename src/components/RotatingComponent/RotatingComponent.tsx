import React, {useState, useRef} from 'react';
import {Animated, Easing} from 'react-native';

export const RotatingComponent: React.FC = ({children}) => {
  const rotationValueRef = useRef(new Animated.Value(0));
  const [rotationValue] = useState(rotationValueRef.current);

  Animated.loop(
    Animated.sequence([
      Animated.timing(rotationValue, {
        toValue: 1,
        useNativeDriver: true,
        duration: 15000,
        easing: Easing.linear,
      }),
      Animated.timing(rotationValue, {
        toValue: 0,
        useNativeDriver: true,
        duration: 1,
      }),
    ]),
  ).start();

  const spin = rotationValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <Animated.View style={{transform: [{rotate: spin}]}}>
      {children}
    </Animated.View>
  );
};
