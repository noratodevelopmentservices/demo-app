import React from 'react';
import {OpponentMatchPoints, UserMatchPoints} from '@components';
import {Images} from '@themes';
import {
  Wrapper,
  PlayerWrapper,
  VersusComponentWrapper,
  VersusImage,
} from './MatchUsersStyles';

interface MatchUsersProps {
  userName: string;
  userPoints: number;
  opponentName: string;
  opponentPoints: number;
}

export const MatchUsers: React.FC<MatchUsersProps> = ({
  userName,
  userPoints,
  opponentName,
  opponentPoints,
}) => {
  return (
    <Wrapper>
      <PlayerWrapper>
        <UserMatchPoints name={userName} points={userPoints} />
      </PlayerWrapper>
      <VersusComponentWrapper>
        <VersusImage source={Images.versus} />
      </VersusComponentWrapper>
      <PlayerWrapper>
        <OpponentMatchPoints name={opponentName} points={opponentPoints} />
      </PlayerWrapper>
    </Wrapper>
  );
};
