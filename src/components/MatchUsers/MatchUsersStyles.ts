import Styled from 'styled-components/native';

export const Wrapper = Styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
`;

export const PlayerWrapper = Styled.View`
  width: 35%;
`;

export const VersusComponentWrapper = Styled.View`
  width: 30%;
  align-items: center;
  justify-content: center;
`;

export const VersusImage = Styled.Image`
  width: 60px;
  height: 60px;
`;
