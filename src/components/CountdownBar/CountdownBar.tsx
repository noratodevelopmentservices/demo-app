import React, {useRef, useState, useEffect} from 'react';
import {Animated} from 'react-native';
import {useInterval} from '../../core/hooks/UseInterval';
import {Player} from '@react-native-community/audio-toolkit';
import {Wrapper, styles} from './CountdownBarStyles';

interface CountdownBarProps {
  duration: number;
  gradientColors?: string[];
  secondsBeforeChangingColor?: number;
}

export const CountdownBar: React.FC<CountdownBarProps> = ({
  duration,
  gradientColors = ['#429AB1', '#429AB1', '#EA0000'],
  secondsBeforeChangingColor = 10,
}) => {
  const durationValueRef = useRef(new Animated.Value(0));
  const [durationValue] = useState(durationValueRef.current);
  const durationInSecs = duration / 1000;
  const [secondsLeft, setSecondsLeft] = useState(durationInSecs - 1);
  let player: Player | undefined;
  if (!player) {
    player = new Player('countdown.mp3');
  }

  let widthValue = durationValue.interpolate({
    inputRange: [0, duration],
    outputRange: ['100%', '0%'],
  });

  let colorValue = durationValue.interpolate({
    inputRange: [0, duration - secondsBeforeChangingColor * 1000, duration],
    outputRange: gradientColors,
  });
  useInterval(() => {
    if (secondsLeft > 0) {
      player!.play();
      setSecondsLeft(secondsLeft - 1);
    }
  }, 1000);

  useEffect(() => {
    Animated.timing(durationValue, {
      toValue: duration,
      useNativeDriver: false,
      duration: duration,
    }).start();
  }, [duration, durationValue]);
  return (
    <Wrapper>
      <Animated.View
        style={[
          styles.animatedBar,
          {
            width: widthValue,
            backgroundColor: colorValue,
          },
        ]}
      />
      <Animated.View
        style={[
          styles.animatedCountdown,
          {
            borderColor: colorValue,
          },
        ]}>
        <Animated.Text style={[styles.animatedText, {color: colorValue}]}>
          {secondsLeft}
        </Animated.Text>
      </Animated.View>
    </Wrapper>
  );
};
