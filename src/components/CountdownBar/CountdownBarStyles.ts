import {StyleSheet} from 'react-native';
import Styled from 'styled-components/native';
import {scale, verticalScale} from '@themes';

export const styles = StyleSheet.create({
  animatedBar: {
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius: 8,
    height: verticalScale(40),
  },
  animatedCountdown: {
    width: scale(36),
    height: verticalScale(36),
    borderRadius: 18,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
  },
  animatedText: {
    fontSize: 16,
  },
});

export const Wrapper = Styled.View`
  width: 100%;
  height: ${verticalScale(40)}px;
  border-radius: 8px;
  shadow-opacity: 0.6;
  shadow-radius: 3px;
  shadow-color: black;
  shadow-offset: 1px 1px;
  justify-content: center;
  align-items: center;
  background-color: white;
  overflow: hidden;
`;
