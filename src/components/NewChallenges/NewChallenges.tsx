import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Check from '../../assets/icons/Check.svg';
import {LanguageService} from '@services';
import {styles, Label, CheckContainer} from './NewChallengesStyles';

interface NewChallengesProps {
  gradientColors?: string[];
}

export const NewChallenges: React.FC<NewChallengesProps> = ({
  gradientColors = ['#5BABFD', '#154DAB'],
}) => {
  return (
    <LinearGradient colors={gradientColors} style={styles.gradientWrapper}>
      <CheckContainer>
        <Check />
      </CheckContainer>
      <Label>
        {LanguageService.getTranslation('components.newChallenges.label')}
      </Label>
    </LinearGradient>
  );
};
