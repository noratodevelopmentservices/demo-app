import Styled from 'styled-components/native';
import {isIOS} from '@core';
import {StyleSheet} from 'react-native';
import {scale} from '@themes';

export const Label = Styled.Text`
  width: 85%;
  flex-wrap: wrap;
  text-shadow-offset: 1px 1px;
  text-shadow-radius: 1px;
  text-shadow-color: black;
  color: white;
  line-height: 25px;
  text-transform: uppercase;
  font-size: ${isIOS ? 23 : 21}px;
  font-weight: bold;
  margin-left: 10px;
  text-align: center;
`;

export const CheckContainer = Styled.View`
  width: 15%;
`;

export const styles = StyleSheet.create({
  gradientWrapper: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: scale(220),
  },
});
