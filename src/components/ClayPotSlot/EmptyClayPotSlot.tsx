import React from 'react';
import {Wrapper, SlotLabel} from './EmptyClayPotStyles';

export const EmptyClayPotSlot: React.FC = () => {
  return (
    <Wrapper>
      <SlotLabel>Spazio Giara</SlotLabel>
    </Wrapper>
  );
};
