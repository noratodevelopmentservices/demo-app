import styled from 'styled-components/native';
import {Dimensions, StyleSheet} from 'react-native';
import { scale, verticalScale } from '@themes';
const {width} = Dimensions.get('window');

export const SlotLabel = styled.Text`
  font-size: 15px;
  color: white;
  text-align: center;
  font-weight: bold;
`;

export const OpenLabel = styled(SlotLabel)`
  margin-bottom: ${verticalScale(10)}px;
`;

export const ClayPotWrapper = styled.View`
  margin-bottom: ${verticalScale(10)}px;
`;

export const styles = StyleSheet.create({
  gradientStyle: {
    paddingHorizontal: scale(15),
    paddingTop: verticalScale(10),
    paddingBottom: verticalScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#b8b8b8',
    height: verticalScale(159),
    width: scale(width / 4 - 12),
  },
});
