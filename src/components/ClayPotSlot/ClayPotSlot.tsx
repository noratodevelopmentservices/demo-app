import React, {useEffect, useState} from 'react';
import {ClayPotSlotToExpire} from './ClayPotSlotToExpire';
import {ExpiredClayPotSlot} from './ExpiredClayPotSlot';
import {BlockedClayPotSlot} from './BlockedClayPotSlot';
import {LocationIndex, PrizeSlotIndexes} from '@entities';
const SECONDS_THRESHOLD = 50000;

interface ClayPotSlotProps {
  timestamp: number;
  level: LocationIndex;
  isBlocked: boolean;
  duration: number;
  index: PrizeSlotIndexes;
  unblock: (index: PrizeSlotIndexes) => void;
  serverTimeDifference?: number;
}

export const ClayPotSlot: React.FC<ClayPotSlotProps> = ({
  timestamp = new Date().getTime() + SECONDS_THRESHOLD,
  level,
  isBlocked,
  duration,
  index,
  unblock,
  serverTimeDifference,
}) => {
  const [isExpired, setIsExpired] = useState(false);
  useEffect(() => {
    let currentTime =
      new Date().getTime() + (serverTimeDifference ? serverTimeDifference : 0);
    let interval = setInterval(() => {
      currentTime = new Date().getTime();
      if (currentTime >= timestamp) {
        setIsExpired(true);
        clearInterval(interval);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [serverTimeDifference, timestamp]);

  if (isBlocked) {
    return (
      <BlockedClayPotSlot
        duration={duration}
        level={level}
        index={index}
        unblock={unblock}
      />
    );
  }
  if (isExpired) {
    return <ExpiredClayPotSlot />;
  }
  return <ClayPotSlotToExpire timestamp={timestamp} level={level} />;
};
