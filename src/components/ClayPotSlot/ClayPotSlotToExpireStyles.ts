import styled from 'styled-components/native';
import {scale, verticalScale} from '@themes';

export const TimeWrapper = styled.View`
  flex-direction: row;
  background-color: white;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  width: ${scale(80)}px;
  margin-top: ${verticalScale(10)}px;
  padding-horizontal: ${scale(3)}px;
  height: ${verticalScale(20)}px;
  align-items: center;
  margin-bottom: ${verticalScale(10)}px;
`;

export const TimeLabel = styled.Text`
  color: #434343;
  font-weight: bold;
  font-size: 12px;
`;

export const ClayPotWrapper = styled.View`
  margin-bottom: ${verticalScale(10)}px;
  align-self: center;
`;

export const LevelLabel = styled.Text`
  color: white;
  font-weight: bold;
  text-transform: uppercase;
  font-size: 12px;
  align-self: center;
`;
