import React from 'react';
import Gradient from 'react-native-linear-gradient';
import {ClayPot} from '../../assets/svgs/ClayPot';
import {
  OpenLabel,
  SlotLabel,
  ClayPotWrapper,
  styles,
} from './ExpiredClayPotSlotStyles';
import {scale, verticalScale} from '@themes';

interface ExpiredClayPotSlotProps {
  backgroundGradientColors: string[];
}

export const ExpiredClayPotSlot: React.FC<ExpiredClayPotSlotProps> = ({
  backgroundGradientColors = ['#FFD973', '#FE7C00'],
}) => {
  return (
    <Gradient style={styles.gradientStyle} colors={backgroundGradientColors}>
      <OpenLabel>Apri</OpenLabel>
      <ClayPotWrapper>
        <ClayPot color={'green'} width={scale(50)} height={verticalScale(69)} />
      </ClayPotWrapper>
      <SlotLabel>Livello 1</SlotLabel>
    </Gradient>
  );
};
