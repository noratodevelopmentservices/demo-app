import styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {scale, verticalScale} from '@themes';

export const Wrapper = styled.TouchableOpacity`
  padding-horizontal: ${scale(5)}px;
  padding-top: ${verticalScale(20)}px;
  padding-bottom: ${verticalScale(10)}px;
  align-items: center;
  background-color: #00000060;
  border-width: 1px;
  border-radius: 8px;
  border-color: #b8b8b8;
  width: ${scale(100)}px;
  height: ${verticalScale(159)}px;
`;
export const TimeWrapper = styled.View`
  flex-direction: row;
`;

export const ClockWrapper = styled.View`
  margin-right: ${scale(5)}px;
  margin-bottom: ${verticalScale(10)}px;
`;

export const TimeLabel = styled.Text`
  color: white;
  font-weight: bold;
  font-size: 16px;
`;

export const ClayPotWrapper = styled.View`
  margin-bottom: ${verticalScale(10)}px;
`;

export const LevelLabel = styled.Text`
  color: white;
  font-weight: bold;
  text-transform: uppercase;
  font-size: 12px;
`;

export const styles = StyleSheet.create({
  animatedView: {marginTop: verticalScale(-45)},
});
