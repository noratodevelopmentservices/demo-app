import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {scale, verticalScale} from '@themes';
const {width} = Dimensions.get('window');

export const Wrapper = styled.View`
  padding-horizontal: ${scale(5)}px;
  padding-top: ${verticalScale(20)}px;
  padding-bottom: ${verticalScale(10)}px;
  align-items: center;
  justify-content: center;
  background-color: #00000060;
  border-width: 1px;
  border-radius: 8px;
  border-color: #b8b8b8;
  height: ${verticalScale(159)}px;
  width: ${scale(width / 4 - 12)}px;
`;

export const SlotLabel = styled.Text`
  font-size: 18px;
  color: white;
  text-align: center;
  font-weight: bold;
`;
