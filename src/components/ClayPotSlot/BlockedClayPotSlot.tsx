import React from 'react';
import LottieView from 'lottie-react-native';
import {Clock} from '../../assets/svgs/Clock';
import {ClayPot} from '../../assets/svgs/ClayPot';
import {LocationIndex, PrizeSlotIndexes} from '@entities';
import {
  Wrapper,
  ClayPotWrapper,
  ClockWrapper,
  LevelLabel,
  TimeLabel,
  TimeWrapper,
  styles,
} from './BlockedClayPotSlotStyles';
import {scale, verticalScale} from '@themes';

interface BlockedClayPotSlotProps {
  duration: number;
  level: LocationIndex;
  index: PrizeSlotIndexes;
  unblock: (index: PrizeSlotIndexes) => void;
}

export const BlockedClayPotSlot: React.FC<BlockedClayPotSlotProps> = ({
  duration,
  level,
  index,
  unblock,
}) => {
  return (
    <Wrapper onPress={() => unblock(index)}>
      <LottieView
        style={styles.animatedView}
        source={require('../../assets/animations/unblock.json')}
        autoPlay
        loop
      />
      <TimeWrapper>
        <ClockWrapper>
          <Clock width={scale(16)} height={verticalScale(18)} />
        </ClockWrapper>
        <TimeLabel>{duration}h</TimeLabel>
      </TimeWrapper>
      <ClayPotWrapper>
        <ClayPot
          color={'#D0FF86'}
          width={scale(50)}
          height={verticalScale(69)}
        />
      </ClayPotWrapper>
      <LevelLabel>Livello {level}</LevelLabel>
    </Wrapper>
  );
};
