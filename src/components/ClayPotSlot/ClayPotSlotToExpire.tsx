import React from 'react';
import Gradient from 'react-native-linear-gradient';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import {ClayPot} from '../../assets/svgs/ClayPot';
import {UseCountDownLabel} from '../../core/hooks/UseCountDownLabel';
import {LocationIndex} from '@entities';
import {
  TimeWrapper,
  TimeLabel,
  LevelLabel,
  ClayPotWrapper,
} from './ClayPotSlotToExpireStyles';
import {scale, verticalScale} from '@themes';

interface ClayPotSlotProps {
  timestamp: number;
  level: LocationIndex;
}

export const ClayPotSlotToExpire: React.FC<ClayPotSlotProps> = ({
  timestamp,
  level,
}) => {
  const timeLabel = UseCountDownLabel(timestamp);
  return (
    <>
      <TouchableOpacity
        style={{
          height: verticalScale(210),
          justifyContent: 'flex-end',
        }}>
        <Gradient
          colors={['#F8A363', '#69350D']}
          style={{
            overflow: 'visible',
            paddingTop: verticalScale(10),
            paddingBottom: verticalScale(10),
            width: scale(100),
            borderWidth: 1,
            borderRadius: 8,
            borderColor: '#b8b8b8',
          }}>
          <TimeWrapper>
            <LottieView
              style={{width: scale(18), height: verticalScale(18)}}
              source={require('../../assets/animations/clock.json')}
              autoPlay
              loop
            />

            <TimeLabel>{timeLabel}</TimeLabel>
          </TimeWrapper>

          <ClayPotWrapper>
            <ClayPot
              color={'#D0FF86'}
              width={scale(50)}
              height={verticalScale(69)}
            />
          </ClayPotWrapper>
          <LevelLabel>Livello {level}</LevelLabel>
        </Gradient>
        <LottieView
          style={{
            width: scale(100),
            position: 'absolute',
            top: 0,
          }}
          source={require('../../assets/animations/speedup.json')}
          autoPlay
          loop
        />
      </TouchableOpacity>
    </>
  );
};
