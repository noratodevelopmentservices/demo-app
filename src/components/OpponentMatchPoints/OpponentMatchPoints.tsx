import React from 'react';
import Gradient from 'react-native-linear-gradient';
import {
  Wrapper,
  PointsWrapper,
  PointsLabel,
  NameLabel,
  styles,
} from './OpponentMatchPointsStyles';

interface UserMatchPointsProps {
  name: string;
  points: number;
  gradientColors: string[];
}

export const OpponentMatchPoints: React.FC<UserMatchPointsProps> = ({
  name,
  points,
  gradientColors = ['#CC0909', '#FAC08E'],
}) => {
  return (
    <Wrapper>
      <PointsWrapper>
        <PointsLabel>{points}</PointsLabel>
      </PointsWrapper>
      <Gradient
        colors={gradientColors}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={styles.gradientBackground}>
        <NameLabel>{name}</NameLabel>
      </Gradient>
    </Wrapper>
  );
};
