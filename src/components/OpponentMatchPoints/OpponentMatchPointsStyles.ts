import Styled from 'styled-components/native';
import {StyleSheet} from 'react-native';

export const Wrapper = Styled.View`
  flex-direction: row;
  width: 100%;
`;

export const PointsWrapper = Styled.View`
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  border-width: 1px;
  background-color: white;
  border-color: #626262;
  justify-content: center;
  align-items: center;
  width: 35%;
`;

export const PointsLabel = Styled.Text`
  font-size: 21px;
  color: #616161;
`;

export const NameLabel = Styled.Text`
  font-size: 18px;
  color: white;
`;

export const styles = StyleSheet.create({
  gradientBackground: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    borderColor: '#626262',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    width: '65%',
  },
});
