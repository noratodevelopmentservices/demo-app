import styled from 'styled-components/native';

export const BlueBox = styled.View`
  margin-top: 60px;
  padding: 10px;
  background-color: #12528c;
  border-radius: 8px;
`;
