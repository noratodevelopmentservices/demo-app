import React from 'react';
import {Image} from 'react-native';
import {Images} from '@themes';
import {
  Wrapper,
  NameLabel,
  WhiteBar,
  BarContainer,
  BlueBar,
  CenterContainer,
  Icon,
  InfoLabel,
  SideContainer,
  SideInfoContainer,
} from './StatsBoxRowStyles';

interface StatsBoxRowProps {
  key: string;
  icon: string;
  value: number;
  opponentValue: number;
  name: string;
}

export const StatsBoxRow: React.FC<StatsBoxRowProps> = ({
  icon,
  value,
  opponentValue,
  name,
}) => {
  return (
    <Wrapper>
      <SideContainer>
        <SideInfoContainer>
          <Icon source={Images[icon]} />
          <InfoLabel>{value}</InfoLabel>
        </SideInfoContainer>
        <BarContainer>
          <BlueBar value={value} />
          <WhiteBar value={value} />
        </BarContainer>
      </SideContainer>
      <CenterContainer>
        <Image
          source={
            value > opponentValue
              ? Images.statsLeftArrowSelected
              : Images.statsLeftArrowUnselected
          }
        />
        <NameLabel>{name}</NameLabel>
        <Image
          source={
            opponentValue > value
              ? Images.statsRightArrowSelected
              : Images.statsRightArrowUnselected
          }
        />
      </CenterContainer>
      <SideContainer>
        <SideInfoContainer>
          <InfoLabel>{opponentValue}</InfoLabel>
          <Icon source={Images[icon]} />
        </SideInfoContainer>
        <BarContainer>
          <WhiteBar value={opponentValue} />
          <BlueBar value={opponentValue} />
        </BarContainer>
      </SideContainer>
    </Wrapper>
  );
};
