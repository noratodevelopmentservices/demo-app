import React from 'react';
import {Stats} from '@entities';
import {BlueBox} from './StatsBoxStyles';
import {StatsBoxRow} from './StatsBoxRow';

interface StatsBoxProps {
  ownStats: Stats;
  opponentStats: Stats;
}

export const StatsBox: React.FC<StatsBoxProps> = ({
  ownStats,
  opponentStats,
}) => {
  return (
    <BlueBox>
      {Object.keys(opponentStats).map((key: string) => {
        let name = opponentStats[key].name;
        let icon = opponentStats[key].icon;
        let value = 0;
        if (ownStats && ownStats[key]) {
          value = ownStats[key].value;
        }
        const opponentValue = opponentStats[key].value;
        return (
          <StatsBoxRow
            key={key}
            icon={icon}
            value={value}
            opponentValue={opponentValue}
            name={name}
          />
        );
      })}
    </BlueBox>
  );
};
