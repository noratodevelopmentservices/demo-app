import styled from 'styled-components/native';

export const Wrapper = styled.View`
  flex-direction: row;
`;

export const SideContainer = styled.View`
  width: 37%;
  padding: 5px;
`;

export const SideInfoContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  margin-bottom: 5px;
`;

export const Icon = styled.Image`
  width: 20px;
  height: 20px;
  resize-mode: contain;
`;

export const InfoLabel = styled.Text`
  color: white;
  font-size: 16px;
`;
export const BarContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const BlueBar = styled.View`
  width: ${({value}: {value: number}) => 100 - value}%;
  height: 6px;
  background-color: #13305d;
`;

export const WhiteBar = styled.View`
  width: ${({value}: {value: number}) => value}%;
  background-color: white;
  height: 10px;
`;

export const CenterContainer = styled.View`
  width: 26%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const NameLabel = styled.Text`
  color: white;
  font-size: 10px;
  text-transform: uppercase;
`;
