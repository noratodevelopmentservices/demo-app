import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {ClayPotSlot, StoryWrapper} from '@components';
import {LocationIndex, PrizeSlotIndexes} from '@entities';

const TIMESTAMP = 100000;

storiesOf('Molecules', module).add('Blocked Clay Pot Slot', () => (
  <StoryWrapper>
    <ClayPotSlot
      unblock={() => console.log('unblock!')}
      isBlocked={true}
      index={PrizeSlotIndexes.TWO}
      timestamp={new Date().getTime() + TIMESTAMP}
      level={LocationIndex.ONE}
      duration={2}
    />
  </StoryWrapper>
));
