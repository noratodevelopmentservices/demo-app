import React, {useState} from 'react';
import {GlowingComponent} from '@components';
import {storiesOf} from '@storybook/react-native';
import {LayoutChangeEvent, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Component: React.FC = () => {
  const [wrapperWidth, setWrapperWidth] = useState<number>(0);
  const [wrapperHeight, setWrapperHeight] = useState<number>(0);
  const onLayout = (e: LayoutChangeEvent) => {
    const {width, height} = e?.nativeEvent?.layout;
    setWrapperWidth(Number(width));
    setWrapperHeight(Number(height));
  };
  return (
    <View
      style={{
        backgroundColor: 'grey',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <GlowingComponent width={wrapperWidth} height={wrapperHeight}>
        <TouchableOpacity>
          <LinearGradient
            colors={['#FFBA50', '#FFFF4F']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{
              padding: 5,
              borderRadius: 5,
              width: 200,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onLayout={onLayout}>
            <Text
              style={{
                fontSize: 48,
                color: 'white',
                fontWeight: 'bold',
                textShadowColor: '#FFA011',
                textShadowOffset: {width: 2, height: 2},
                textShadowRadius: 2,
              }}>
              GIOCA
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </GlowingComponent>
    </View>
  );
};

storiesOf('Atoms', module).add('Glowing Component', () => <Component />);
