import React from 'react';
import {ChooseLocationCarousel, StoryWrapper} from '@components';
import {storiesOf} from '@storybook/react-native';
import {locations} from '../../services/mockData/locations';

storiesOf('Atoms', module).add('Choose location carousel', () => (
  <StoryWrapper>
    <ChooseLocationCarousel
      locations={locations}
      points={{one: 30, two: 30, three: 35, four: 50}}
      onPlayPressed={() => console.log('play pressed')}
    />
  </StoryWrapper>
));
