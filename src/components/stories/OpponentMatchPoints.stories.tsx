import React from 'react';
import {OpponentMatchPoints} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Atoms', module).add('Opponent Match Points', () => (
  <View style={{padding: 10, width: 180}}>
    <OpponentMatchPoints name={'Sara'} points={0} />
  </View>
));
