import React from 'react';
import {CountdownBar} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Molecules', module).add('Countdown Bar', () => (
  <View
    style={{
      padding: 10,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'grey',
      flex: 1,
    }}>
    <CountdownBar duration={30000} />
  </View>
));
