import React from 'react';
import {EntityButton} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Atoms', module).add('Entity Button', () => (
  <View>
    <EntityButton
      label={'112.124'}
      icon={'coin'}
      onButtonPressed={() => console.log('button pressed')}
    />
  </View>
));
