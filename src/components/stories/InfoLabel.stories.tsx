import React from 'react';
import {InfoLabel} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Atoms', module).add('Custom Rectangle', () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <InfoLabel label={'534'} />
  </View>
));
