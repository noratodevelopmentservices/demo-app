import React from 'react';
import {MatchChatContent} from '../MatchChat/MatchChatContent';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Molecules', module).add('Match Chat Content', () => (
  <View>
    <MatchChatContent />
  </View>
));
