import React from 'react';
import {StatsBox, StoryWrapper} from '@components';
import {storiesOf} from '@storybook/react-native';
import {ownStats} from '../../services/mockData/user';
import {opponentStats} from '../../services/mockData';

storiesOf('Molecules', module).add('Stats Box', () => (
  <StoryWrapper style={{backgroundColor: 'grey'}}>
    <StatsBox ownStats={ownStats} opponentStats={opponentStats} />
  </StoryWrapper>
));
