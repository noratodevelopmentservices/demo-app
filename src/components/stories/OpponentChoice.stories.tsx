import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {OpponentChoice} from '../OpponentChoice';
import {StoryWrapper} from '@components';

storiesOf('Molecules', module).add('Opponent Choice', () => (
  <StoryWrapper style={{backgroundColor: 'grey'}}>
    <OpponentChoice
      onAnimationEnded={() => console.log('animation ended')}
      opponentName={'Giuseppe'}
      opponentPoints={819}
    />
  </StoryWrapper>
));
