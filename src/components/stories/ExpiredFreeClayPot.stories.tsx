import React from 'react';
import {FreeClayPot} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Molecules', module).add('Expired Free Clay Pot', () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <FreeClayPot timestamp={0} claimFreeClayPot={() => console.log('claim!')} />
  </View>
));
