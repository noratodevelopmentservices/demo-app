import React from 'react';
import {View} from 'react-native';
import {RotatingComponent} from '@components';
import {Clock} from '../../assets/svgs/Clock';
import {storiesOf} from '@storybook/react-native';

storiesOf('Atoms', module).add('Rotating component', () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'grey',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <RotatingComponent>
      <Clock />
    </RotatingComponent>
  </View>
));
