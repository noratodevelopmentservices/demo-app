import React from 'react';
import {ClayPotSlot, StoryWrapper} from '@components';
import {storiesOf} from '@storybook/react-native';
import {LocationIndex, PrizeSlotIndexes} from '@entities';

storiesOf('Molecules', module).add('Clay Pot Slot', () => (
  <StoryWrapper>
    <ClayPotSlot
      unblock={() => console.log('unblock!')}
      isBlocked={false}
      index={PrizeSlotIndexes.TWO}
      timestamp={new Date().getTime() + 100000}
      level={LocationIndex.ONE}
      duration={2}
    />
  </StoryWrapper>
));
