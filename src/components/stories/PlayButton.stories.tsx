import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {StoryWrapper, PlayButton} from '@components';

storiesOf('Atoms', module).add('Play Button', () => (
  <StoryWrapper style={{backgroundColor: 'grey'}}>
    <PlayButton onPress={() => console.log('Play button pressed')} />
  </StoryWrapper>
));
