import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {NewChallenges} from '../NewChallenges';
import {StoryWrapper} from '@components';

storiesOf('Atoms', module).add('New Challenges', () => (
  <StoryWrapper>
    <NewChallenges />
  </StoryWrapper>
));
