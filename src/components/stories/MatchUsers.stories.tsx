import React from 'react';
import {MatchUsers} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Molecules', module).add('Match Users', () => (
  <View
    style={{
      flex: 1,
      padding: 10,
      backgroundColor: 'grey',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <MatchUsers
      userName={'Mario'}
      userPoints={12}
      opponentName={'Giuseppe'}
      opponentPoints={8}
    />
  </View>
));
