import React from 'react';
import {ChooseLocationCard, StoryWrapper} from '@components';
import {storiesOf} from '@storybook/react-native';
import {CoatOfArmsShape, Location} from '@entities';

const location: Location = {
  name: 'Lucerne',
  shape: CoatOfArmsShape.GREEN_HEXAGON,
  coatOfArms: 'caltanissettaStemma',
  win: 11,
  lose: 6,
  availablePoints: 30,
  prize: 600,
  unblocks: 40,
  backgroundGradientColors: ['blue', 'black'],
};
storiesOf('Molecules', module).add('Choose Location Card', () => (
  <StoryWrapper>
    <ChooseLocationCard
      location={location}
      onPlayPressed={() => console.log('play pressed')}
      active={true}
      pointsWon={12}
    />
  </StoryWrapper>
));
