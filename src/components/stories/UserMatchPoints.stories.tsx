import React from 'react';
import {UserMatchPoints} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Atoms', module).add('User Match Points', () => (
  <View style={{padding: 10, width: 180}}>
    <UserMatchPoints name={'Mario'} points={15} />
  </View>
));
