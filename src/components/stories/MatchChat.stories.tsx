import React from 'react';
import {ImageBackground} from 'react-native';
import {MatchChat} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import {Images} from '@themes';

storiesOf('Atoms', module).add('Match Chat', () => (
  <ImageBackground
    source={Images.trapaniBackground}
    style={{flex: 1, paddingHorizontal: 10, paddingTop: 100}}>
    <View style={{width: 80}}>
      <MatchChat />
    </View>
  </ImageBackground>
));
