import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {StoryWrapper, ProfileIcon} from '@components';

storiesOf('Atoms', module).add('Profile Icon', () => (
  <StoryWrapper style={{backgroundColor: 'grey'}}>
    <ProfileIcon />
  </StoryWrapper>
));
