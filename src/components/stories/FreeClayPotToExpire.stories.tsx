import React from 'react';
import {FreeClayPot} from '@components';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';

storiesOf('Molecules', module).add('Free Clay Pot to expire', () => (
  <View
    style={{
      flex: 1,
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <FreeClayPot
      timestamp={new Date().getTime() + 1000000}
      claimFreeClayPot={() => console.log('claim!')}
    />
  </View>
));
