import React, {useRef, useEffect, useState} from 'react';
import {Animated, Easing} from 'react-native';
import {OpponentName} from './OpponentName';
import {
  Wrapper,
  styles,
  IconImage,
  OverflowView,
  iconSize,
} from './OpponentChoiceStyle';
import FastImage from 'react-native-fast-image';

const ANIMATION_DURATION = 6000;
const ANIMATION_DELAY = 1000;

interface OpponentChoiceProps {
  onAnimationEnded: () => void;
  opponentName: string;
  opponentPoints: number;
  opponentAnimationCanStart: boolean;
  opponentsFaces: {id: string; url: string}[];
}
export const OpponentChoice: React.FC<OpponentChoiceProps> = ({
  onAnimationEnded,
  opponentName,
  opponentPoints,
  opponentAnimationCanStart,
  opponentsFaces,
}) => {
  const scrollValueRef = useRef(new Animated.Value(0));
  const [scrollValue] = useState(scrollValueRef.current);
  const [showName, setShowName] = useState(false);
  const animation = Animated.sequence([
    Animated.delay(ANIMATION_DELAY),
    Animated.timing(scrollValue, {
      toValue: iconSize * (opponentsFaces.length - 1) * -1,
      duration: ANIMATION_DURATION,
      easing: Easing.out(Easing.exp),
      useNativeDriver: true,
    }),
  ]);
  useEffect(() => {
    if (opponentAnimationCanStart) {
      animation.start(() => {
        setShowName(true);
        onAnimationEnded();
      });
    }
  }, [animation, onAnimationEnded, opponentAnimationCanStart]);

  return (
    <Wrapper>
      <OverflowView>
        <Animated.View
          style={[
            styles.animatedIconWrapper,
            {transform: [{translateY: scrollValue}]},
          ]}>
          {opponentsFaces.map((item) => {
            return (
              <FastImage
                key={item.id}
                source={{uri: item.url, priority: FastImage.priority.normal}}
                style={{
                  width: 66,
                  height: 66,
                  borderRadius: 33,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
            );
          })}
        </Animated.View>
      </OverflowView>
      {showName && <OpponentName name={opponentName} points={opponentPoints} />}
    </Wrapper>
  );
};
