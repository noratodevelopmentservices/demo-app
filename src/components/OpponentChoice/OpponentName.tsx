import React from 'react';
import Cup from '../../assets/icons/Cup.svg';
import {NameLabel, PointsLabel, PointsWrapper} from './OpponentNameStyles';

interface OpponentNameProps {
  name: string;
  points: number;
}

export const OpponentName: React.FC<OpponentNameProps> = ({name, points}) => {
  return (
    <>
      <NameLabel>{name}</NameLabel>
      <PointsWrapper>
        <Cup />
        <PointsLabel>{points}</PointsLabel>
      </PointsWrapper>
    </>
  );
};
