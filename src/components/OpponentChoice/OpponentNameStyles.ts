import Styled from 'styled-components/native';

export const NameLabel = Styled.Text`
	align-self: center;
	margin-top: 15px;
	font-size: 24px;
	color: white;
	font-weight: bold;
`;

export const PointsWrapper = Styled.View`
	flex-direction: row;
	align-items: center;
	align-self: center;
`;

export const PointsLabel = Styled.Text`
	color: white;
	margin-left: 10px;
	font-size: 21px;
	font-weight: bold;
`;
