import Styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {scale} from '@themes';

export const iconSize = scale(66);

export const Wrapper = Styled.View`
  width: 130px;
  height: 140px;
  shadow-offset: 2px 2px;
  shadow-color: #FFEF00;
  shadow-opacity: 1;
  shadow-radius: 40px;
`;

export const OverflowView = Styled.View`
  align-self: center;
  width: 72px;
  height: 72px;
  border-radius: 36px;
  border-width: 3px;
  border-color: white;
  overflow: hidden;
`;

export const IconImage = Styled.Image`
  width: 66px;
  height: 66px;
  border-radius: 33px;
  resize-mode: cover;
`;

export const styles = StyleSheet.create({
  animatedIconWrapper: {
    height: 72,
    width: 72,
  },
});
