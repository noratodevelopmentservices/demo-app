import Styled from 'styled-components/native';
import {isIOS} from '@core';

export const CupWrapper = Styled.View`
  position: absolute;
  left: 8px;
  top: 7px;
`;

export const LabelText = Styled.Text`
  position: absolute;
  text-align: right;
  width: 58px;
  top: ${isIOS ? 9 : 6}px;
  left: 30px;
  color: white;
  font-size: 15px;
`;
