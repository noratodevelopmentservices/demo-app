import React from 'react';
import {View, ViewStyle} from 'react-native';
import Svg, {Polygon} from 'react-native-svg';
import Cup from '@assets/icons/Cup.svg';
import {CupWrapper, LabelText} from './InfoLabelStyles';

export interface InfoLabelProps {
  label: string;
  style?: ViewStyle;
}

export const InfoLabel: React.FC<InfoLabelProps> = ({label, style}) => {
  return (
    <View style={style}>
      <Svg height="34" width="95">
        <Polygon points="5,0 95,0 90,34 0,34" fill="grey" />
      </Svg>
      <CupWrapper>
        <Cup />
      </CupWrapper>
      <LabelText>{label}</LabelText>
    </View>
  );
};
