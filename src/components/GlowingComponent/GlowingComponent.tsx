import React, {useRef, useState, useEffect, useCallback} from 'react';
import {Animated, View} from 'react-native';
import Spark from '../../assets/svgs/Spark.svg';

interface GlowingButtonProps {
  width?: number;
  height?: number;
}

export const GlowingComponent: React.FC<GlowingButtonProps> = ({
  width = 0,
  height = 0,
  children,
}) => {
  const moveRightValueRef = useRef(new Animated.Value(-10));
  const moveLeftValueRef = useRef(new Animated.Value(0));
  const opacityValueRef = useRef(new Animated.Value(0));
  const opacity2ValueRef = useRef(new Animated.Value(0));

  const [moveRightValue] = useState(moveRightValueRef.current);
  const [moveLeftValue] = useState(moveLeftValueRef.current);

  const [opacityValue] = useState(opacityValueRef.current);
  const [opacity2Value] = useState(opacity2ValueRef.current);

  const fadeIn = Animated.timing(opacityValue, {
    useNativeDriver: true,
    toValue: 2,
    duration: 500,
  });

  const fadeIn2 = Animated.timing(opacity2Value, {
    useNativeDriver: true,
    toValue: 2,
    duration: 500,
  });

  const moveRight = Animated.timing(moveRightValue, {
    useNativeDriver: true,
    toValue: width - 15,
    duration: 2500,
  });

  const moveLeft = Animated.timing(moveLeftValue, {
    useNativeDriver: true,
    toValue: 0 - width + 13,
    duration: 2500,
  });

  const fadeOut = Animated.sequence([
    Animated.delay(2000),
    Animated.timing(opacityValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 500,
    }),
  ]);

  const fadeOut2 = Animated.sequence([
    Animated.delay(2000),
    Animated.timing(opacity2Value, {
      useNativeDriver: true,
      toValue: 0,
      duration: 500,
    }),
  ]);

  const moveBack = Animated.timing(moveRightValue, {
    useNativeDriver: true,
    toValue: 0,
    duration: 1,
  });

  const moveBack2 = Animated.timing(moveLeftValue, {
    useNativeDriver: true,
    toValue: 0,
    duration: 1,
  });

  const fadeInAndMoveRight = Animated.sequence([
    Animated.parallel([fadeIn, moveRight, fadeOut]),
    moveBack,
    Animated.delay(1500),
  ]);

  const fadeInAndMoveLeft = Animated.sequence([
    Animated.parallel([fadeIn2, moveLeft, fadeOut2]),
    moveBack2,
    Animated.delay(1500),
  ]);

  const animation = useCallback(
    () => Animated.loop(fadeInAndMoveRight).start(),
    [fadeInAndMoveRight],
  );

  const animationLeft = useCallback(
    () => Animated.loop(fadeInAndMoveLeft).start(),
    [fadeInAndMoveLeft],
  );
  useEffect(() => {
    if (width > 0) {
      moveLeftValueRef.current.setValue(width - 10);
      animation();
      animationLeft();
    }
  }, [animation, animationLeft, width]);
  return (
    <View>
      {children}
      <Animated.View
        style={{
          top: 0,
          left: 0,
          transform: [{translateX: moveRightValue}, {scaleY: opacityValue}],
          position: 'absolute',
          opacity: opacityValue,
        }}>
        <Spark />
      </Animated.View>
      <Animated.View
        style={{
          top: height - 2,
          left: width - 10,
          transform: [{translateX: moveLeftValue}, {scaleY: opacity2Value}],
          position: 'absolute',
          opacity: opacity2Value,
        }}>
        <Spark />
      </Animated.View>
    </View>
  );
};
