import Styled from 'styled-components/native';
import {StyleSheet} from 'react-native';

export const Wrapper = Styled.View`
  justify-content: center;
`;

export const ToggleButtonWrapper = Styled.TouchableOpacity`
  background-color: #73737390;
  border-radius: 8px;
  align-items: center;
  justify-content: center;
  padding: 9px;
`;

export const ToggleButtonImage = Styled.Image`
  width: 24px;
  height: 24px;
`;

export const styles = StyleSheet.create({
  animatedViewWrapper: {
    position: 'absolute',
    left: 0,
  },
});
