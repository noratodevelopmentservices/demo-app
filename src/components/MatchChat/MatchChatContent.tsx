import React from 'react';
import {Images} from '@themes';
import {
  MainWrapper,
  SmileyIcon,
  SmileyIconWrapper,
  ChatButton,
  ChatButtonLabel,
  ChatButtonsWrapper,
  SmileysWrapper,
} from './MatchChatContentStyles';

export interface ChatResource {
  value: string;
  key: string;
  image?: number;
}

const words: ChatResource[][] = [
  [
    {value: 'Salutamu', key: '1'},
    {value: "Unn'a sacciu", key: '2'},
    {value: 'A sacciu!', key: '3'},
  ],
  [
    {value: 'Va eccati!', key: '4'},
    {value: 'Aricchiuni!', key: '5'},
    {value: 'Rugnusu!', key: '6'},
  ],
];

const smileyImages: ChatResource[] = [
  {image: Images.smileySpacchiusu, key: '7', value: 'spacchiusu'},
  {image: Images.smileyLaughing, key: '8', value: 'risata'},
  {image: Images.smileyCrying, key: '9', value: 'pianto'},
  {image: Images.smileyWow, key: '10', value: 'wow'},
];

interface MatchChatContentProps {
  toggleAnimation: () => void;
  sendChatMessage: (chatObject: ChatResource) => void;
}

export const MatchChatContent: React.FC<MatchChatContentProps> = ({
  toggleAnimation,
  sendChatMessage,
}) => {
  const onButtonPressed = (object: ChatResource) => {
    sendChatMessage(object);
    toggleAnimation();
  };
  return (
    <MainWrapper>
      <SmileysWrapper>
        {smileyImages.map((smiley, index) => {
          return (
            <SmileyIconWrapper
              key={index}
              onPress={() => onButtonPressed(smileyImages[index])}>
              <SmileyIcon source={smiley.image!} />
            </SmileyIconWrapper>
          );
        })}
      </SmileysWrapper>
      {words.map((row, index) => {
        return (
          <ChatButtonsWrapper key={`row-${index}`}>
            {row.map((word, buttonIndex) => {
              return (
                <ChatButton
                  key={word.key}
                  onPress={() => onButtonPressed(words[index][buttonIndex])}>
                  <ChatButtonLabel>{word.value}</ChatButtonLabel>
                </ChatButton>
              );
            })}
          </ChatButtonsWrapper>
        );
      })}
    </MainWrapper>
  );
};
