import Styled from 'styled-components/native';
import {scale, verticalScale} from '@themes';
import {Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

export const MainWrapper = Styled.View`
  width: ${scale(width * 0.9)}px;
  background-color: #00000099;
  padding-vertical: ${scale(10)}px;
  padding-horizontal: 5px;
`;

export const SmileysWrapper = Styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: flex-end;
  margin-bottom: 10px;
`;

export const SmileyIconWrapper = Styled.TouchableOpacity`
  
`;

export const ChatButtonsWrapper = Styled.View`
  flex-direction: row;
  justify-content: space-around;
  padding-vertical: ${verticalScale(5)}px;
`;

export const SmileyIcon = Styled.Image`
  width: ${scale(30)}px;
  height: ${verticalScale(30)}px;
  margin-right: ${scale(20)}px;
`;

export const ChatButton = Styled.TouchableOpacity`
  background-color: white;
  border-radius: 5px;
  align-items: center;
  justify-content: center;
  padding: 5px;
  width: ${scale(100)}px;
`;

export const ChatButtonLabel = Styled.Text`
  font-weight: bold;
`;
