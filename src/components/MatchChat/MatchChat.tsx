import React, {useRef, useState} from 'react';
import {Dimensions, Animated, Easing, TouchableOpacity} from 'react-native';
import {Images} from '@themes';
import {ChatResource, MatchChatContent} from './MatchChatContent';
import {
  Wrapper,
  styles,
  ToggleButtonImage,
  ToggleButtonWrapper,
} from './MatchChatStyles';

const {width} = Dimensions.get('window');

interface MatchChatProps {
  sendChatMessage: (chatMessage: ChatResource) => void;
}

export const MatchChat: React.FC<MatchChatProps> = ({sendChatMessage}) => {
  const [contentVisible, setContentVisible] = useState(false);
  const contentPositionValueRef = useRef(new Animated.Value(width * 0.95 * -1));
  const [contentPositionValue] = useState(contentPositionValueRef.current);
  const moveInAnimation = Animated.timing(contentPositionValue, {
    toValue: 0,
    useNativeDriver: true,
    duration: 300,
    easing: Easing.linear,
  });

  const moveOutAnimation = Animated.timing(contentPositionValue, {
    toValue: width * 0.95 * -1,
    useNativeDriver: true,
    duration: 300,
    easing: Easing.linear,
  });

  const manageAnimation = () => {
    if (!contentVisible) {
      setContentVisible(true);
      moveInAnimation.start();
    } else {
      moveOutAnimation.start(() => setContentVisible(false));
    }
  };

  return (
    <Wrapper>
      <Animated.View
        style={[
          styles.animatedViewWrapper,
          {transform: [{translateX: contentPositionValue}]},
        ]}>
        <TouchableOpacity onPress={manageAnimation}>
          <MatchChatContent
            toggleAnimation={manageAnimation}
            sendChatMessage={sendChatMessage}
          />
        </TouchableOpacity>
      </Animated.View>

      {!contentVisible && (
        <ToggleButtonWrapper onPress={manageAnimation}>
          <ToggleButtonImage source={Images.chatIcon} />
        </ToggleButtonWrapper>
      )}
    </Wrapper>
  );
};
