import React, {useEffect, useState} from 'react';
import {ClayPot} from '../../assets/svgs/ClayPot';
import Styled from 'styled-components/native';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import { UseCountDownLabel } from '../../core/hooks/UseCountDownLabel';

interface FreeClayPotProps {
  timestamp?: number;
}

export const Wrapper = Styled.View`
  padding: 10px;
  background-color: #B1B1B190;
  border-color: #979797;
  border-radius: 8px;
  border-width: 1px;
  flex-direction: row;
  align-items: center;
`;

export const TitleWrapper = Styled.View`
  margin-right: 20px;
`;

export const TitleLabel = Styled.Text`
  font-size: 12px;
  color: white;
  margin-bottom: 3px;
`;

const SecondRowWrapper = Styled.View`
  flex-direction: row;
  align-items: center;
`;

const ClockWrapper = Styled.View`
  margin-right: 5px;
`;

const TimeLabel = Styled.Text`
  font-size: 12px;
  color: white;
`;

export const FreeClayPotToExpire: React.FC<FreeClayPotProps> = ({
  timestamp,
}) => {
  const label = UseCountDownLabel(timestamp!);
  return (
    <Wrapper>
      <TitleWrapper>
        <TitleLabel>GIARA GRATUITA</TitleLabel>
        <SecondRowWrapper>
          <ClockWrapper>
            <LottieView
              style={{width: 24, height: 24}}
              source={require('../../assets/animations/whiteClock.json')}
              autoPlay
              loop
            />
          </ClockWrapper>
          <TimeLabel>{label}</TimeLabel>
        </SecondRowWrapper>
      </TitleWrapper>
      <ClayPot />
    </Wrapper>
  );
};
