import React from 'react';
import Styled from 'styled-components/native';
import {ClayPot} from '../../assets/svgs/ClayPot';
import Gradient from 'react-native-linear-gradient';
import {TouchableOpacity} from 'react-native-gesture-handler';

const TitleWrapper = Styled.View`
  margin-right: 20px;
`;

const Shadow = Styled.Text`
  shadow-color: grey;
  shadow-opacity: 1;
  shadow-offset: 0px 0px;
  shadow-radius: 1px;
`;

const TitleLabel = Styled(Shadow)`
  font-size: 14px;
  color: white;
  margin-bottom: 3px;
`;

const TimeLabel = Styled(Shadow)`
  font-size: 18px;
  font-weight: bold;
  color: white;
`;

interface ExpiredFreeClayPotProps {
  openFreeClayPot: () => void;
}

export const ExpiredFreeClayPot: React.FC<ExpiredFreeClayPotProps> = ({
  openFreeClayPot,
}) => {
  return (
    <TouchableOpacity onPress={openFreeClayPot}>
      <Gradient
        colors={['#F8CB4D', '#D46505']}
        style={{
          padding: 10,
          borderRadius: 8,
          flexDirection: 'row',
          alignItems: 'center',
          width: 180,
        }}>
        <TitleWrapper>
          <TitleLabel>GIARA GRATUITA</TitleLabel>
          <TimeLabel>APRI</TimeLabel>
        </TitleWrapper>
        <ClayPot color={'green'} />
      </Gradient>
    </TouchableOpacity>
  );
};
