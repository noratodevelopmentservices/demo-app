import React, {useState, useEffect} from 'react';

import {FreeClayPotToExpire} from './FreeClayPotToExpire';
import {ExpiredFreeClayPot} from './ExpiredFreeClayPot';

interface FreeClayPotProps {
  timestamp?: number;
  claimFreeClayPot: () => void;
  serverTimeDifference?: number;
}

export const FreeClayPot: React.FC<FreeClayPotProps> = ({
  timestamp,
  claimFreeClayPot,
  serverTimeDifference,
}) => {
  const [expired, setExpired] = useState(false);

  useEffect(() => {
    console.log('server time difference ', serverTimeDifference);
    let currentTime =
      new Date().getTime() + (serverTimeDifference ? serverTimeDifference : 0);
    let interval = setInterval(() => {
      currentTime =
        new Date().getTime() +
        (serverTimeDifference ? serverTimeDifference : 0);
      if (currentTime >= timestamp!) {
        setExpired(true);
        clearInterval(interval);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [serverTimeDifference, timestamp]);

  if (expired) {
    return <ExpiredFreeClayPot openFreeClayPot={claimFreeClayPot} />;
  }
  return <FreeClayPotToExpire timestamp={timestamp} />;
};
