import Styled from 'styled-components/native';
import {scale, verticalScale} from '@themes';

const WhiteText = Styled.Text`
  color: white;
`;

export const PrizeWrapper = Styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: ${verticalScale(20)}px;
  margin-bottom: ${scale(20)}px;
`;

export const PrizeLabel = Styled(WhiteText)`
  font-size: 27px;
  margin-right: ${scale(10)}px;
`;

export const CoinLabel = Styled.Text`
  color: #FFB039;
  font-size: 37px;
  font-weight: bold;
  margin-right: ${scale(10)}px;
`;

export const CoinWrapper = Styled.View`
  margin-top: ${verticalScale(4)}px;
`;
