import Styled from 'styled-components/native';

export const InactiveViewWrapper = Styled.View`
  width: 100%;
  height: 85%;
  border-radius: 8px;
  background-color: #00000030;
  position: absolute;
  justify-content: center;
  align-items: center;
  padding-top: 160px;
`;

export const UnlockViewWrapper = Styled.View`
  flex-direction: row;
  align-items: center;
`;

export const LockIcon = Styled.Image`
  width: 60px;
  height: 60px;
  margin-right: 10px;
`;

export const UnblocksAtLabel = Styled.Text`
  color: white;
  font-size: 21px;
`;

export const UnblocksAtValueWrapper = Styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const UnblocksAtValue = Styled.Text`
  color: white;
  font-size: 40px;
  font-weight: bold;
  margin-right: 5px;
`;
