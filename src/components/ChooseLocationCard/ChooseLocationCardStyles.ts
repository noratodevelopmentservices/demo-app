import Styled from 'styled-components/native';
import {scale, verticalScale} from '@themes';
import {StyleSheet, Dimensions} from 'react-native';
const {height} = Dimensions.get('window');

export const styles = StyleSheet.create({
  mainContainer: {
    opacity: 0.9,
    borderRadius: 8,
    paddingHorizontal: scale(10),
    paddingVertical: verticalScale(30),
    alignItems: 'center',
    height: height > 750 ? '85%' : '100%',
  },
});

export const Background = Styled.ImageBackground`
  width: ${scale(244)}px;
  height: ${scale(244)}px;
  align-items: center;
  resize-mode: stretch;
  justify-content: center;
`;

const TextStyle = Styled.Text`
  color: white;
  text-shadow-color: black;
  text-shadow-radius: 1px;
  text-shadow-offset: 1px 1px;
  font-weight: bold;
  text-transform: uppercase;
`;

export const MainTitle = Styled(TextStyle)`
  font-size: 31px;
`;

export const UnlocksWrapper = Styled.View`
  flex-direction: row;
  width: 100%;
  height: ${verticalScale(60)}px;
  margin-top: ${verticalScale(30)}px;
  padding: ${scale(10)}px;
  justify-content: center; 
`;

export const LocationLogoImage = Styled.Image`
  width: ${scale(124)}px;
  height: ${verticalScale(140)}px;
  resize-mode: contain;
`;

export const InactiveBlackOverlay = Styled.View`
  width: 100%;
  height: ${height > 750 ? 85 : 95}%;
  background-color: #00000099;
  position: absolute;
  border-radius: 8px;
`;
