import React from 'react';
import {Images, scale, verticalScale} from '@themes';
import {View} from 'react-native';
import NumberFormat from 'react-number-format';
import Cup from '../../assets/icons/Cup.svg';
import {
  InactiveViewWrapper,
  LockIcon,
  UnblocksAtLabel,
  UnblocksAtValue,
  UnblocksAtValueWrapper,
  UnlockViewWrapper,
} from './UnlockViewStyles';

interface UnlockViewProps {
  unblocks: number;
}

export const UnlockView: React.FC<UnlockViewProps> = ({unblocks}) => {
  return (
    <InactiveViewWrapper>
      <UnlockViewWrapper>
        <LockIcon source={Images.unlocks} />
        <View>
          <UnblocksAtLabel>Si sblocca a</UnblocksAtLabel>
          <UnblocksAtValueWrapper>
            <NumberFormat
              value={unblocks}
              thousandSeparator={'.'}
              decimalSeparator={','}
              displayType={'text'}
              renderText={(value: string) => (
                <UnblocksAtValue>{value}</UnblocksAtValue>
              )}
            />
            <Cup width={scale(30)} height={verticalScale(30)} />
          </UnblocksAtValueWrapper>
        </View>
      </UnlockViewWrapper>
    </InactiveViewWrapper>
  );
};
