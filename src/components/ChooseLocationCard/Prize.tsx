import React from 'react';
import NumberFormat from 'react-number-format';
import {Text} from 'react-native';
import {Coin} from '../../assets/icons/Coin';
import {PrizeWrapper, CoinLabel, CoinWrapper, PrizeLabel} from './PrizeStyles';
import {scale, verticalScale} from '@themes';
import {LanguageService} from '@services';

interface PrizeProps {
  prize: number;
}

export const Prize: React.FC<PrizeProps> = ({prize}) => {
  return (
    <PrizeWrapper>
      <PrizeLabel>
        {LanguageService.getTranslation(
          'components.chooseLocationCard.prize.label',
        )}
      </PrizeLabel>
      <CoinLabel>
        <NumberFormat
          value={prize}
          thousandSeparator={'.'}
          decimalSeparator={','}
          displayType={'text'}
          renderText={(value: string) => <Text>{value}</Text>}
        />
      </CoinLabel>
      <CoinWrapper>
        <Coin width={scale(21)} height={verticalScale(21)} />
      </CoinWrapper>
    </PrizeWrapper>
  );
};
