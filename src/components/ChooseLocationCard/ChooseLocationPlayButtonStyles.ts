import Styled from 'styled-components/native';
import {StyleSheet} from 'react-native';
import {scale, verticalScale} from '@themes';

const TextStyle = Styled.Text`
  color: white;
  text-shadow-color: black;
  text-shadow-radius: 1px;
  text-shadow-offset: 1px 1px;
  font-weight: bold;
  text-transform: uppercase;
`;

export const PlayButtonLabel = Styled(TextStyle)`
  fontSize: 21px;
`;

export const CoinInButtonWrapper = Styled.View`
  margin-top: ${verticalScale(2)}px;
  margin-left: ${scale(10)}px;
  shadow-radius: 1px;
  shadow-offset: 1px 1px;
  shadow-color: black;
  shadow-opacity: 1;
`;

export const styles = StyleSheet.create({
  playButtonWrapper: {
    borderRadius: 8,
    flexDirection: 'row',
    paddingVertical: verticalScale(10),
    paddingHorizontal: scale(10),
  },
});
