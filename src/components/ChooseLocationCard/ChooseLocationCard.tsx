import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {Images} from '@themes';
import {
  Background,
  UnlocksWrapper,
  InactiveBlackOverlay,
  LocationLogoImage,
  MainTitle,
  styles,
} from './ChooseLocationCardStyles';
import {Location} from '@entities';
import {LocationInfo} from '../LocationInfo/LocationInfo';
import {Prize} from './Prize';
import {ChooseLocationPlayButton} from './ChooseLocationPlayButton';
import {UnlockView} from './UnlockView';

interface ChooseLocationCardProps {
  onPlayPressed: () => void;
  location: Location;
  active: boolean;
  pointsWon: number;
}

export const ChooseLocationCard: React.FC<ChooseLocationCardProps> = ({
  onPlayPressed,
  location,
  active,
  pointsWon,
}) => {
  const {
    name,
    coatOfArms,
    lose,
    prize,
    shape,
    unblocks,
    win,
    backgroundGradientColors,
  } = location;

  return (
    <>
      <LinearGradient
        colors={backgroundGradientColors}
        style={styles.mainContainer}>
        <Background source={Images[shape]}>
          <LocationLogoImage source={Images[coatOfArms]} />
        </Background>
        <MainTitle>{name}</MainTitle>
        {!active && <UnlocksWrapper />}
        {active && (
          <LocationInfo
            win={win}
            lose={lose}
            pointsAvailable={location.availablePoints}
            pointsWon={pointsWon}
          />
        )}
        <Prize prize={prize} />
        <ChooseLocationPlayButton onPlayPressed={onPlayPressed} prize={prize} />
      </LinearGradient>
      {!active && <InactiveBlackOverlay />}
      {!active && <UnlockView unblocks={unblocks} />}
    </>
  );
};
