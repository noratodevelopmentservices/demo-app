import React, {useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import NumberFormat from 'react-number-format';
import {LayoutChangeEvent, Text} from 'react-native';
import {Coin} from '../../assets/icons/Coin';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  PlayButtonLabel,
  CoinInButtonWrapper,
  styles,
} from './ChooseLocationPlayButtonStyles';
import {GlowingComponent} from '@components';
import {scale, verticalScale} from '@themes';
import {LanguageService} from '@services';

interface PlayButtonProps {
  onPlayPressed: () => void;
  prize: number;
}

export const ChooseLocationPlayButton: React.FC<PlayButtonProps> = ({
  onPlayPressed,
  prize,
}) => {
  const [wrapperWidth, setWrapperWidth] = useState<number>(0);
  const [wrapperHeight, setWrapperHeight] = useState<number>(0);
  const onLayout = (e: LayoutChangeEvent) => {
    const {width, height} = e?.nativeEvent?.layout;
    setWrapperWidth(Number(width));
    setWrapperHeight(Number(height));
  };
  return (
    <GlowingComponent height={wrapperHeight} width={wrapperWidth}>
      <TouchableOpacity onLayout={onLayout} onPress={onPlayPressed}>
        <LinearGradient
          colors={['#FFD973', '#FE7C00']}
          style={styles.playButtonWrapper}>
          <PlayButtonLabel>
            {LanguageService.getTranslation(
              'components.chooseLocationPlayButton.toPlay',
            )}
            {
              <NumberFormat
                value={prize / 2}
                thousandSeparator={'.'}
                decimalSeparator={','}
                displayType={'text'}
                renderText={(value: string) => <Text>{value}</Text>}
              />
            }
          </PlayButtonLabel>
          <CoinInButtonWrapper>
            <Coin width={scale(21)} height={verticalScale(21)} />
          </CoinInButtonWrapper>
        </LinearGradient>
      </TouchableOpacity>
    </GlowingComponent>
  );
};
