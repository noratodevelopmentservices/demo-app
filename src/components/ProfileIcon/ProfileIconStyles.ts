import Styled from 'styled-components/native';

export const IconImage = Styled.Image`
  width: ${(props: {size: number; borderWidth: number}) => props.size}px;
  height: ${(props: {size: number; borderWidth: number}) => props.size}px;
  border-radius: ${(props: {size: number; borderWidth: number}) =>
    props.size / 2}px;
  border-width: ${(props: {size: number; borderWidth: number}) =>
    props.borderWidth}px;
  border-color: white;
  resize-mode: cover;
`;
