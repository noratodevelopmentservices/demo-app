import React from 'react';
import {Images} from '@themes';
import {IconImage} from './ProfileIconStyles';

interface ProfileIconProps {
  imageUrl?: string;
  size?: number;
  borderWidth?: number;
  localImage?: boolean;
}
export const ProfileIcon: React.FC<ProfileIconProps> = ({
  imageUrl = 'https://www.hellomaps.net/images/joomla-maps/faceuser1.jpg',
  size = 40,
  borderWidth = 1,
  localImage = false,
}) => {
  const imageSource = localImage
    ? Images[imageUrl]
    : {
        uri: imageUrl,
      };
  return (
    <IconImage size={size} borderWidth={borderWidth} source={imageSource} />
  );
};
