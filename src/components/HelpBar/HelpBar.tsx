import React from 'react';
import {Images} from '@themes';
import {Wrapper, Help} from './HelpbarStyles';

const helps: {[key: string]: {icon: string; active: boolean}} = {
  fifty: {icon: 'fiftyHelp', active: true},
  threeQuarters: {icon: 'threeQuartersHelp', active: true},
  disturb: {icon: 'disturbHelp', active: true},
  geography: {icon: 'geographyHelp', active: false},
};

export const HelpBar: React.FC = () => {
  return (
    <Wrapper>
      {Object.keys(helps).map((key: string) => {
        const {icon} = helps[key];
        return <Help key={icon} source={Images[icon]} />;
      })}
    </Wrapper>
  );
};
