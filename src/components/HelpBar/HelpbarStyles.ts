import styled from 'styled-components/native';

export const Help = styled.Image`
  width: 50px;
  height: 50px;
  margin-right: 5px;
  shadow-opacity: 1;
  shadow-offset: 0 0;
  shadow-color: grey;
  shadow-radius: 1px;
`;

export const Wrapper = styled.View`
  flex-direction: row;
`;
