import React from 'react';
import {ViewStyle, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Gradient from 'react-native-linear-gradient';
import {scale, verticalScale} from '@themes';

interface ButtonWithGradientProps {
  colors: string[];
  onPress: () => void;
  style?: ViewStyle;
}

const styles = StyleSheet.create({
  gradientStyle: {
    paddingVertical: verticalScale(5),
    paddingHorizontal: scale(10),
    borderRadius: 5,
    alignItems: 'center',
  },
});

export const ButtonWithGradient: React.FC<ButtonWithGradientProps> = ({
  colors,
  onPress,
  children,
  style,
}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Gradient colors={colors} style={[style, styles.gradientStyle]}>
        {children}
      </Gradient>
    </TouchableOpacity>
  );
};
