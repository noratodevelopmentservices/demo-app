import Styled from 'styled-components/native';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  nameGradientWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    borderColor: '#626262',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    width: '65%',
  },
});

export const Wrapper = Styled.View`
  flex-direction: row;
  width: 100%;
`;

export const PointsWrapper = Styled.View`
  background-color: white;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  border-width: 1px;
  border-color: #626262;
  justify-content: center;
  align-items: center;
  width: 35%;
`;

export const NameLabel = Styled.Text`
  font-size: 18px;
  color: white;
`;

export const PointsLabel = Styled.Text`
  font-size: 21px;
  color: #616161;
`;
