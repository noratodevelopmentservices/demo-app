import React from 'react';
import Gradient from 'react-native-linear-gradient';
import {
  Wrapper,
  NameLabel,
  PointsLabel,
  PointsWrapper,
  styles,
} from './UserMatchPointsStyles';

interface UserMatchPointsProps {
  name: string;
  points: number;
  nameBackgroundGradientColors?: string[];
}

export const UserMatchPoints: React.FC<UserMatchPointsProps> = ({
  name,
  points,
  nameBackgroundGradientColors = ['#1D74CB', '#5297DE'],
}) => {
  return (
    <Wrapper>
      <Gradient
        colors={nameBackgroundGradientColors}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={styles.nameGradientWrapper}>
        <NameLabel>{name}</NameLabel>
      </Gradient>
      <PointsWrapper>
        <PointsLabel>{points}</PointsLabel>
      </PointsWrapper>
    </Wrapper>
  );
};
