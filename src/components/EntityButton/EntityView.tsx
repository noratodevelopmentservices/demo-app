import React from 'react';
import {View, Text} from 'react-native';
import Svg, {Polygon} from 'react-native-svg';
import {isIOS} from '@core';

export interface EntityButtonProps {
  label: string;
}

export const EntityView: React.FC<EntityButtonProps> = ({label, children}) => {
  return (
    <View>
      <Svg height="34" width="135">
        <Polygon points="5,0 35,0 30,34 0,34" fill="black" />
        <Polygon points="35,0 125,0 120,34 30,34" fill="grey" />
      </Svg>
      <Text
        style={{
          color: '#49D384',
          fontSize: 31,
          fontWeight: 'bold',
          position: 'absolute',
          left: 8,
          top: isIOS ? -3 : -8,
        }}>
        +
      </Text>

      <Text
        style={{
          color: 'white',
          fontSize: 15,
          position: 'absolute',
          left: 33,
          top: isIOS ? 8 : 5,
          textAlign: 'right',
          width: 66,
        }}>
        {label}
      </Text>
      {children}
    </View>
  );
};
