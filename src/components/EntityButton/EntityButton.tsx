import React, {ReactNode} from 'react';
import {TouchableOpacity, ViewStyle} from 'react-native';
import Styled from 'styled-components/native';
import {EntityView} from './EntityView';
import {Coin} from '../../assets/icons/Coin';
import {Gem} from '../../assets/icons/Gem';

const Icon = Styled.View`
  position: absolute;
  left: ${107 + 43 - 22 - 28}px;
  top: 6px;
`;

interface EntityButton {
  label: string;
  icon: 'coin' | 'gem';
  onButtonPressed: () => void;
  style?: ViewStyle;
}
export const EntityButton: React.FC<EntityButton> = ({
  label,
  onButtonPressed,
  style,
  icon,
}) => {
  const iconComponent: {[key: string]: ReactNode} = {
    coin: (
      <Icon>
        <Coin width={21} height={21} />
      </Icon>
    ),
    gem: (
      <Icon>
        <Gem width={21} height={21} />
      </Icon>
    ),
  };
  return (
    <TouchableOpacity style={[style, {width: 110}]} onPress={onButtonPressed}>
      <EntityView label={label}>{iconComponent[icon]}</EntityView>
    </TouchableOpacity>
  );
};
