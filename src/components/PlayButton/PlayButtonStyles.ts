import {StyleSheet} from 'react-native';
import Styled from 'styled-components/native';
import {scale} from '@themes';

export const styles = StyleSheet.create({
  gradientWrapper: {
    padding: scale(5),
    borderRadius: 5,
    width: scale(200),
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export const PlayLabel = Styled.Text`
  font-size: 36px;
  color: white;
  font-weight: bold;
  text-shadow-color: #FFA011;
  text-shadow-offset: 2px 2px;
  text-shadow-radius: 2px;
`;
