import React, {useState} from 'react';
import {LayoutChangeEvent, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {GlowingComponent} from '@components';
import {PlayLabel, styles} from './PlayButtonStyles';
import LanguageService from '../../services/LanguageService';

interface PlayButtonProps {
  onPress: () => void;
  backgroundGradientColors?: string[];
}
export const PlayButton: React.FC<PlayButtonProps> = ({
  onPress,
  backgroundGradientColors = ['#FFBA50', '#FFFF4F'],
}) => {
  const [wrapperWidth, setWrapperWidth] = useState<number>(0);
  const [wrapperHeight, setWrapperHeight] = useState<number>(0);
  const onLayout = (e: LayoutChangeEvent) => {
    const {width, height} = e?.nativeEvent?.layout;
    setWrapperWidth(Number(width));
    setWrapperHeight(Number(height));
  };
  return (
    <GlowingComponent width={wrapperWidth} height={wrapperHeight}>
      <TouchableOpacity onPress={onPress}>
        <LinearGradient
          colors={backgroundGradientColors}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          style={styles.gradientWrapper}
          onLayout={onLayout}>
          <PlayLabel>
            {LanguageService.getTranslation('components.playButton.label')}
          </PlayLabel>
        </LinearGradient>
      </TouchableOpacity>
    </GlowingComponent>
  );
};
