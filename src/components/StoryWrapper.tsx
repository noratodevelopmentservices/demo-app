import React from 'react';
import {StyleSheet, View, ViewStyle} from 'react-native';

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
});

interface StoryWrapperProps {
  style?: ViewStyle;
}

export const StoryWrapper: React.FC<StoryWrapperProps> = ({
  children,
  style,
}) => <View style={[styles.mainWrapper, style]}>{children}</View>;
