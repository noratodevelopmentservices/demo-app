import React from 'react';
import {ProfileIcon} from '../ProfileIcon';
import {EntityButton, InfoLabel} from '@components';
import Styled from 'styled-components/native';

const Wrapper = Styled.View`
  flex-direction: row;
  width: 95%;
  margin-bottom: 10px;
  justify-content: space-between;
  align-items: center;
`;

interface InforBarProps {
  profileImage: string;
  points: number;
  coins: number;
  gems: number;
  onAddCoinsPressed: () => void;
  onAddGemsPressed: () => void;
}

export const InfoBar: React.FC<InforBarProps> = ({
  profileImage,
  points,
  coins,
  gems,
  onAddCoinsPressed,
  onAddGemsPressed,
}) => {
  return (
    <Wrapper>
      <ProfileIcon
        imageUrl={profileImage === '' ? 'userHead' : profileImage}
        localImage={profileImage === ''}
      />
      <InfoLabel label={String(points)} />
      <EntityButton
        label={String(coins)}
        icon={'coin'}
        style={{marginRight: 10}}
        onButtonPressed={onAddCoinsPressed}
      />
      <EntityButton
        label={String(gems)}
        icon={'gem'}
        onButtonPressed={onAddGemsPressed}
      />
    </Wrapper>
  );
};
