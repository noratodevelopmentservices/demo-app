import {StyleSheet} from 'react-native';
import Styled from 'styled-components/native';

export const InfoWrapper = Styled.View`
  flex-direction: row;
  background-color: #19191980;
  border-radius: 8px;
  width: 100%;
  margin-top: 30px;
  padding: 10px;
  justify-content: space-around;  
`;

export const SideInfoWrapper = Styled.View`
  align-items: center; 
`;

export const LeftInfoWrapper = Styled(SideInfoWrapper)`
  margin-right: 20px;
`;

export const WhiteText = Styled.Text`
  color: white;
`;

export const CentralInfoWrapper = Styled.View`
  flex-direction: row;
  background-color: transparent;
  border-radius: 8px;
  border-width: 1px;
  border-color: grey;
  width: 50%;
  margin-right: 10px;
`;

export const CupWrapper = Styled.View`
  position: absolute;
  left: -13px; 
  top: 5px;
`;

export const CentralInfoLabel = Styled(WhiteText)`
  align-self: center;
  width: 100%;
  textAlign: center;
`;

export const styles = StyleSheet.create({
  progressBar: {
    position: 'absolute',

    height: '100%',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#8E1313',
    alignSelf: 'flex-start',
  },
});
