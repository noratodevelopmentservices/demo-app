import React from 'react';
import {View} from 'react-native';
import Cup from '../../assets/icons/Cup.svg';
import {
  InfoWrapper,
  CupWrapper,
  WhiteText,
  CentralInfoLabel,
  CentralInfoWrapper,
  LeftInfoWrapper,
  SideInfoWrapper,
  styles,
} from './LocationInfoStyles';

interface LocationInfoProps {
  win: number;
  lose: number;
  pointsAvailable?: number;
  pointsWon: number;
}
export const LocationInfo: React.FC<LocationInfoProps> = ({
  win,
  pointsWon,
  pointsAvailable,
  lose,
}) => {
  const progress = pointsWon / (pointsAvailable ? pointsAvailable : 1);
  return (
    <InfoWrapper>
      <LeftInfoWrapper>
        <WhiteText>Vittoria</WhiteText>
        <WhiteText>{win}</WhiteText>
      </LeftInfoWrapper>
      {pointsAvailable && (
        <CentralInfoWrapper>
          <View
            style={[
              styles.progressBar,
              {
                width: `${(pointsWon / pointsAvailable) * 100}%`,
                borderTopRightRadius: progress === 1 ? 8 : 0,
                borderBottomRightRadius: progress === 1 ? 8 : 0,
              },
            ]}
          />
          <CupWrapper>
            <Cup />
          </CupWrapper>
          <CentralInfoLabel>{`${pointsWon}/${pointsAvailable}`}</CentralInfoLabel>
        </CentralInfoWrapper>
      )}
      <SideInfoWrapper>
        <WhiteText>Sconfitta</WhiteText>
        <WhiteText>{lose}</WhiteText>
      </SideInfoWrapper>
    </InfoWrapper>
  );
};
