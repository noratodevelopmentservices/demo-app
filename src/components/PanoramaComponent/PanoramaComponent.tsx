import React, {useState, useRef} from 'react';
import {Animated, Easing} from 'react-native';
import {styles} from './PanoramaComponentStyles';

export const PanoramaComponent: React.FC = ({children}) => {
  const animationValueRef = useRef(new Animated.Value(0));
  const [animationValue] = useState(animationValueRef.current);
  const animationRight = Animated.timing(animationValue, {
    useNativeDriver: true,
    toValue: 60,
    easing: Easing.linear,
    duration: 8000,
  });

  const animationLeft = Animated.timing(animationValue, {
    useNativeDriver: true,
    toValue: 0,
    easing: Easing.linear,
    duration: 8000,
  });

  Animated.loop(
    Animated.sequence([
      animationRight,
      Animated.delay(1500),
      animationLeft,
      Animated.delay(1500),
    ]),
  ).start();
  return (
    <Animated.View
      style={[
        styles.animatedWrapper,
        {transform: [{translateX: animationValue}]},
      ]}>
      {children}
    </Animated.View>
  );
};
