import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import {ThemeProvider} from 'styled-components/native';
import {PersistGate} from 'redux-persist/integration/react';
import {enableScreens} from 'react-native-screens';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {Player} from '@react-native-community/audio-toolkit';
import {Images, theme} from '@themes';
import ReduxObject from './adapters/redux';
import {AppStackNavigator} from '@modules';
import {navigationRef} from '@services';
import {StartupWrapper} from './core/StartupWrapper';
import Config from 'react-native-config';
import {AppQuestionTestStackNavigator} from './modules/app/AppQuestionTestStackNavigator';
import {setUserOnlineStatusAction, userIdSelector} from './adapters/redux/user';
import {AppState} from './adapters/redux/common';
import Styled from 'styled-components/native';

const {store, persistor} = ReduxObject();
console.disableYellowBox = true;

const LoaderWrapper = Styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: grey;       
`;

const LoaderImage = Styled.Image`
  width: 70%;
`;

enableScreens();
export const App: React.FC = () => {
  let player: Player | undefined;
  if (!player) {
    player = new Player('tarantella.mp3');
  }
  player.volume = 0.2;

  useEffect(() => {
    if (player) {
      // player.play();
    }
  }, [player]);

  useEffect(() => {
    return () => {
      const state = store.getState() as AppState;
      const uuid = userIdSelector(state);
      store.dispatch(setUserOnlineStatusAction(false, uuid!));
    };
  }, []);

  console.log('match test', Config.MATCH_TEST);

  return (
    <Provider store={store}>
      <PersistGate
        persistor={persistor}
        loading={
          <LoaderWrapper>
            <LoaderImage source={Images.logo} />
          </LoaderWrapper>
        }>
        <StartupWrapper>
          <SafeAreaProvider>
            <NavigationContainer ref={navigationRef}>
              <ThemeProvider theme={theme}>
                <StatusBar barStyle="dark-content" />
                {Config.MATCH_TEST === 'true' ? (
                  <AppQuestionTestStackNavigator />
                ) : (
                  <AppStackNavigator />
                )}
              </ThemeProvider>
            </NavigationContainer>
          </SafeAreaProvider>
        </StartupWrapper>
      </PersistGate>
    </Provider>
  );
};
