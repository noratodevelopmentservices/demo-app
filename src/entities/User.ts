import {Stats} from './Stats';
import {LocationStats} from './Location';
import {FreeBox} from './FreeBox';
import {PrizeSlots, PrizeSlotValue} from './PrizeSlot';

export interface User {
  id: string;
  name: string;
  imageUrl: string;
  points: number;
  coins: number;
  gems: number;
  stats: Stats;
  locationStats: LocationStats;
  freeBox: FreeBox;
}

export const totalPoints: (points: {[key: string]: number}) => number = (
  points,
) =>
  Object.keys(points)
    .map((pointsKey: string) => points[pointsKey])
    .reduce((accumulator, currentValue) => accumulator + currentValue);

export const isSpeedup = (prizeSlots: PrizeSlots) => {
  const keys: PrizeSlotValue[] = Object.keys(prizeSlots!) as PrizeSlotValue[];
  return (
    keys
      .map((key: PrizeSlotValue) => prizeSlots![key])
      .find((prizeSlot) => prizeSlot.expiry !== undefined) !== undefined
  );
};
