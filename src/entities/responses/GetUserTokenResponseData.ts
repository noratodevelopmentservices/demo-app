export interface GetUserTokenResponseData {
  token: string;
}
