import {Opponent} from './Opponent';

export interface Match {
  matchId: string;
  opponent: Opponent;
}
