export interface LocationStat {
  played: number;
  won: number;
}

export interface LocationStats {
  [key: string]: LocationStat;
}

export const LocationIndexNumber = [
  'one',
  'two',
  'three',
  'four',
  'five',
  'six',
  'seven',
  'eight',
  'nine',
];

export enum LocationIndex {
  ONE = 'one',
  TWO = 'two',
  THREE = 'three',
  FOUR = 'four',
  FIVE = 'five',
  SIX = 'six',
  SEVEN = 'seven',
  EIGHT = 'eight',
  NINE = 'nine',
}

export enum CoatOfArmsShape {
  GREEN_PENTAGON = 'greenPentagon',
  GREEN_HEXAGON = 'greenHexagon',
  CIRCLE = 'circle',
  SQUARE_DIAMOND = 'squareDiamond',
  SQUARE = 'square',
}

export interface Location {
  name: string;
  shape: CoatOfArmsShape;
  coatOfArms: string;
  win: number;
  lose: number;
  availablePoints?: number;
  prize: number;
  unblocks: number;
  backgroundGradientColors: string[];
}
