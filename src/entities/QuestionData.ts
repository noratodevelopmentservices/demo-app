export interface QuestionData {
  question: string;
  answers: [
    {answer: string; correct: boolean},
    {answer: string; correct: boolean},
    {answer: string; correct: boolean},
    {answer: string; correct: boolean},
  ];
}
