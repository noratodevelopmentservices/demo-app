import {LocationIndex} from './Location';

export type PrizeSlot = {
  blocked: boolean;
  expiry: number;
  level: LocationIndex;
  duration: number;
  type: PrizeSlotTypes;
};

export enum PrizeSlotTypes {
  ONE = 'one',
  TWO = 'two',
  THREE = 'three',
  FOUR = 'four',
  FIVE = 'five',
}

export enum PrizeSlotIndexes {
  ONE = 'one',
  TWO = 'two',
  THREE = 'three',
  FOUR = 'four',
}

export type PrizeSlotValue = 'one' | 'two' | 'three' | 'four' | 'five';

export const prizeSlotType: {[key in PrizeSlotTypes]: string} = {
  one: 'Giara di frumento',
  two: 'Giara di farina',
  three: 'Giara di vino',
  four: 'Giara di olio',
  five: 'Giara di sale',
};

export type PrizeSlots = {
  [key in PrizeSlotValue]: PrizeSlot;
};
