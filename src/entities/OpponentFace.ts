export interface OpponentFace {
  id: string;
  url: string;
}
