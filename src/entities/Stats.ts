export interface Stats {
  [key: string]: {
    value: number;
    name: string;
    icon: string;
  };
}
