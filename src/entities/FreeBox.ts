export interface FreeBox {
  type: string;
  expiration: number;
  claimed: boolean;
}
