import {Stats} from './Stats';

export interface Opponent {
  id?: string;
  name?: string;
  imageUrl?: string;
  points?: number;
  stats?: Stats;
}
