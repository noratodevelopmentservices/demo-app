import {StyleSheet} from 'react-native';
import Styled from 'styled-components/native';
import {isIOS} from '@core';

export const styles = StyleSheet.create({
  backgroundGradient: {width: '100%', height: '65%', position: 'absolute'},
  playerWrapper: {alignItems: 'center'},
});

export const InfoBarWrapper = Styled.View`
  flex: 1;
  position: absolute;
  align-items: center;
  justify-content: center;
  padding-top: ${isIOS ? 40 : 10}px;
`;

export const BottomBodyWrapper = Styled.View`
  flex: 1;
  padding-top: 120px;
  padding-horizontal: 30px;
`;

export const PlayersWrapper = Styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;

export const PlayerIconWrapper = Styled.View`
  align-items: center;
  shadow-opacity: 1;
  shadow-color: #4982E9;
  shadow-offset: 2px 2px;
  shadow-radius: 40px;
`;

export const PlayerCartoonIconWrapper = Styled.View`
  position: absolute;
  left: 60px;
  top: 40px;
`;

export const PlayerNameText = Styled.Text`
  align-self: center;
  margin-top: 15px;
  font-size: 24px;
  color: white;
  font-weight: bold;
`;

export const PlayerPointsLineWrapper = Styled.View`
  flex-direction: row;
  align-items: center;
  align-self: center;
`;

export const PlayerPointsText = Styled.Text`
  color: white;
  margin-left: 10px;
  font-size: 21px;
  font-weight: bold;
`;

export const VersusText = Styled.Text`
  color: white;
  font-size: 31px;
  font-weight: bold;
`;

export const BottomInfoText = Styled.Text`
  margin-top: 10px;
  align-self: center;
  color: white;
  font-size: 24px;
  text-shadow-color: #00000050;
  text-shadow-radius: 2px;
  text-shadow-offset: 2px 2px;
`;
