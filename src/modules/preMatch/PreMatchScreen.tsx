import React, {useRef, useState, useEffect} from 'react';
import {View, Animated} from 'react-native';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import {Dispatch} from 'redux';
import {Opponent, Stats, totalPoints} from '@entities';
import {
  ProfileIcon,
  OpponentChoice,
  InfoBar,
  StatsBox,
  AnimatedBackgroundImage,
} from '@components';
import Cup from '../../assets/icons/Cup.svg';
import {AppState} from '../../adapters/redux/common';
import {
  userCoinsSelector,
  userGemsSelector,
  userIdSelector,
  userImageUrlSelector,
  userNameSelector,
  userPointsSelector,
  userstatsSelector,
} from '../../adapters/redux/user';
import {
  styles,
  InfoBarWrapper,
  BottomBodyWrapper,
  BottomInfoText,
  PlayerCartoonIconWrapper,
  PlayerIconWrapper,
  PlayerNameText,
  PlayerPointsLineWrapper,
  PlayerPointsText,
  PlayersWrapper,
  VersusText,
} from './PreMatchScreenStyles';
import {
  createMatchAction,
  matchOpponentSelector,
} from '../../adapters/redux/match';
import {opponentFacesSelector} from '../../adapters/redux/opponent';
import {LanguageService} from '@services';
import {useInterval} from '../../core/hooks/UseInterval';
import {navigateTo} from '../../adapters/redux/navigation/actions';
import {AppStackParamList} from '../app/routes';
import {StackNavigationProp} from '@react-navigation/stack';

type PreMatchScreenNavProp = StackNavigationProp<AppStackParamList, 'Main'>;

interface ChooseLocationScreenProps {
  navigation: PreMatchScreenNavProp;
  points?: {[key: string]: number};
  coins?: number;
  imageUrl?: string;
  gems?: number;
  userName?: string;
  userStats?: Stats;
  createMatch?: (userId?: string) => void;
  userId?: string;
  opponent?: Opponent;
  opponentFaces?: {id: string; url: string}[];
  navigateToMatchScreen?: () => void;
}

export const PreMatchScreenComponent: React.FC<ChooseLocationScreenProps> = ({
  imageUrl,
  points,
  coins,
  gems,
  userName,
  userStats,
  createMatch,
  userId,
  opponent,
  opponentFaces,
  navigation,
}) => {
  const player1ValueRef = useRef(new Animated.Value(0));
  const [opponentsAnimationCanStart, setOpponentsAnimationCanStart] = useState(
    false,
  );
  const [player1Value] = useState(player1ValueRef.current);
  const [showOpponentData, setShowOpponentData] = useState(false);
  const [countdown, setCountdown] = useState(8);

  useEffect(() => {
    createMatch!(userId);
  }, [createMatch, userId]);

  useInterval(() => {
    if (countdown > 0) {
      setCountdown(countdown - 1);
    }
  }, 1000);

  useEffect(() => {
    if (countdown === 0) {
      navigation.navigate('Match');
    }
  }, [countdown, navigation]);

  useEffect(() => {
    if (opponent && opponent?.imageUrl) {
      opponentFaces!.push({
        id: userId!,
        url: opponent?.imageUrl!,
      });
      setOpponentsAnimationCanStart(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [opponent]);

  Animated.sequence([
    Animated.delay(500),
    Animated.timing(player1Value, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }),
  ]).start();
  return (
    <>
      <AnimatedBackgroundImage
        imageSource={
          'http://residencetrapanirdv.com/wp-content/uploads/2019/06/trapani-header.jpg'
        }
      />
      <LinearGradient
        colors={['#001533', 'transparent']}
        style={styles.backgroundGradient}
      />
      <InfoBarWrapper>
        <InfoBar
          profileImage={imageUrl ? imageUrl : ''}
          points={points ? totalPoints(points) : 0}
          coins={coins ? coins : 0}
          gems={gems ? gems : 0}
          onAddCoinsPressed={() => console.log('add coins!')}
          onAddGemsPressed={() => console.log('add gems!')}
        />
      </InfoBarWrapper>
      <BottomBodyWrapper>
        <PlayersWrapper>
          <Animated.View
            style={[styles.playerWrapper, {opacity: player1Value}]}>
            <PlayerIconWrapper>
              <ProfileIcon size={72} borderWidth={3} imageUrl={imageUrl} />
              <PlayerCartoonIconWrapper>
                <ProfileIcon
                  borderWidth={3}
                  size={38}
                  localImage={true}
                  imageUrl={'userHead'}
                />
              </PlayerCartoonIconWrapper>
              <PlayerNameText>{userName}</PlayerNameText>
              <PlayerPointsLineWrapper>
                <Cup />
                <PlayerPointsText>
                  {points ? totalPoints(points) : 0}
                </PlayerPointsText>
              </PlayerPointsLineWrapper>
            </PlayerIconWrapper>
          </Animated.View>
          {showOpponentData && <VersusText>VS</VersusText>}
          <Animated.View style={{opacity: player1Value}}>
            <OpponentChoice
              opponentAnimationCanStart={opponentsAnimationCanStart}
              onAnimationEnded={() => setShowOpponentData(true)}
              opponentPoints={opponent?.points!}
              opponentName={opponent?.name!}
              opponentsFaces={opponentFaces!}
            />
          </Animated.View>
        </PlayersWrapper>
        {showOpponentData && (
          <View>
            <StatsBox ownStats={userStats!} opponentStats={opponent?.stats!} />
            <BottomInfoText>
              {`${LanguageService.getTranslation(
                'preMatchScreen.matchWillStart',
              )}${countdown} ${
                countdown === 1
                  ? LanguageService.getTranslation('preMatchScreen.second')
                  : LanguageService.getTranslation('preMatchScreen.seconds')
              }`}
            </BottomInfoText>
          </View>
        )}
      </BottomBodyWrapper>
    </>
  );
};
const mapStateToProps = (state: AppState) => ({
  imageUrl: userImageUrlSelector(state),
  coins: userCoinsSelector(state),
  gems: userGemsSelector(state),
  points: userPointsSelector(state),
  userId: userIdSelector(state),
  userName: userNameSelector(state),
  userStats: userstatsSelector(state),
  opponent: matchOpponentSelector(state),
  opponentFaces: opponentFacesSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  createMatch: (userId?: string) => dispatch(createMatchAction({userId})),
  navigateToMatchScreen: () => dispatch(navigateTo({routeName: 'Match'})),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PreMatchScreenComponent);
