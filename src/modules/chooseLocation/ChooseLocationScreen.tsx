import React from 'react';
import {ImageBackground, View} from 'react-native';
import {ChooseLocationCarousel, InfoBar} from '@components';
import {isIOS} from '@core';
import {Images} from '@themes';
import {StackNavigationProp} from '@react-navigation/stack';
import {AppStackParamList} from '../app/routes';
import {AppState} from '../../adapters/redux/common';
import {
  userCoinsSelector,
  userGemsSelector,
  userImageUrlSelector,
  userLocationStatsSelector,
  userPointsSelector,
} from '../../adapters/redux/user';
import {connect} from 'react-redux';
import {locations} from '../../services/mockData/locations';
import {totalPoints} from '@entities';

type ChooseLocationScreenNavProp = StackNavigationProp<
  AppStackParamList,
  'Main'
>;

interface ChooseLocationScreenProps {
  navigation: ChooseLocationScreenNavProp;
  imageUrl?: string;
  points?: {[key: string]: number};
  coins?: number;
  gems?: number;
}
export const ChooseLocationScreen: React.FC<ChooseLocationScreenProps> = ({
  navigation,
  imageUrl = '',
  points,
  coins = 0,
  gems = 0,
}) => {
  return (
    <ImageBackground
      blurRadius={isIOS ? 4 : 1}
      source={Images.chooseLocationBackground}
      style={{
        height: '100%',
        width: '100%',
      }}>
      <View
        style={{
          paddingHorizontal: 10,
          paddingTop: isIOS ? 40 : 10,
        }}>
        <InfoBar
          profileImage={imageUrl}
          points={points ? totalPoints(points) : 0}
          coins={coins}
          gems={gems}
          onAddCoinsPressed={() => console.log('add coins!')}
          onAddGemsPressed={() => console.log('add gems!')}
        />
      </View>
      <View style={{marginTop: isIOS ? 50 : 20}}>
        <ChooseLocationCarousel
          locations={locations}
          points={points}
          onPlayPressed={(location: string) => navigation.navigate('PreMatch')}
        />
      </View>
    </ImageBackground>
  );
};

const mapStateToProps = (state: AppState) => ({
  points: userPointsSelector(state),
  imageUrl: userImageUrlSelector(state),
  coins: userCoinsSelector(state),
  gems: userGemsSelector(state),
  locationStats: userLocationStatsSelector(state),
});

export default connect(mapStateToProps, null)(ChooseLocationScreen);
