import React, {useState} from 'react';
import {UnblockModal} from '../UnblockModal';
import {storiesOf} from '@storybook/react-native';
import {Modal, View, TouchableOpacity, Text} from 'react-native';
import {PrizeSlotIndexes, PrizeSlotTypes} from '../../../entities/PrizeSlot';
import {LocationIndex} from '@entities';

const prizeSlot = {
  blocked: true,
  expiry: 1588105539000,
  level: LocationIndex.ONE,
  duration: 2,
  type: PrizeSlotTypes.ONE,
};

const Component: React.FC = () => {
  const [isModalVisible, setModalVisible] = useState(false);
  return (
    <View
      style={{
        backgroundColor: 'grey',
        width: 400,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Modal visible={isModalVisible} transparent={true} animationType="fade">
        <UnblockModal
          unblockButtonEnabled={false}
          unblock={() => console.log('unblock')}
          index={PrizeSlotIndexes.TWO}
          toggleModal={() => setModalVisible(!isModalVisible)}
          open={() => console.log('open')}
          selectedClayPotIndex={PrizeSlotIndexes.TWO}
          selectedClayPotToUnblock={prizeSlot}
          speedup={true}
          speedupPrize={(index) => console.log(index)}
        />
      </Modal>
      <TouchableOpacity onPress={() => setModalVisible(!isModalVisible)}>
        <Text>Toggle Modal</Text>
      </TouchableOpacity>
    </View>
  );
};

storiesOf('Molecules', module).add('Unblock Modal', () => <Component />);
