import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {ClayPot} from '../../assets/svgs/ClayPot';
import {
  PrizeSlot,
  PrizeSlotIndexes,
  prizeSlotType,
  PrizeSlotTypes,
} from '../../entities/PrizeSlot';
import {LocationIndex} from '@entities';
import {Images} from '@themes';
import styled from 'styled-components/native';
import {ButtonWithGradient} from '@components';
import {Gem} from '../../assets/icons/Gem';
import {UseCountDownLabel} from '../../core/hooks/UseCountDownLabel';

const TransparentWrapper = styled.View`
  flex: 1;
  background-color: #00000099;
`;

const MainContainer = styled.View`
  padding: 20px;
  flex: 1;
  justify-content: center;
`;

const ContentWrapper = styled.View`
  background-color: #0d3d8d;
  border-radius: 12px;
  margin-top: 80px;
  height: 610px;
  padding-top: 120px;
`;

const HeaderWrapper = styled.View`
  position: absolute;
  width: 100%;
  height: 60px;
  background-color: #6FA5FF;
  border-top-left-radius: 12px;
  border-top-right-radius: 12px
  align-items: center;
  justify-content: center;
`;

const CloseButton = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  top: 0px;
  width: 35px;
  height: 35px;
  background-color: white;
  border-top-right-radius: 12px;
  border-bottom-left-radius: 12px;
  align-items: center;
  justify-content: center;
`;

const CloseLabel = styled.Text`
  color: blue;
  font-size: 18px;
  font-weight: bold;
`;

const TitleLabel = styled.Text`
  color: white;
  font-size: 21px;
  font-weight: bold;
`;

const CentralContentWrapper = styled.View`
  flex: 1;
  padding-horizontal: 20px;
`;

const BackgroundClayPotWrapper = styled.View`
  align-self: center;
  position: absolute;
  top: -10px;
  shadow-color: #a9ffc1;
  shadow-offset: 0px 0px;
  shadow-radius: 40px;
  shadow-opacity: 1;
`;

const WhiteBox = styled.View`
  width: 100%;
  height: 280px;
  background-color: white;
  border-radius: 12px;
  margin-top: 30px;
  padding-top: 100px;
  align-items: center;
`;

const Label = styled.Text`
  font-size: 21px;
  color: #173e65;
  font-weight: bold;
`;

const LabelWithMarginTop = styled(Label)`
  margin-top: 10px;
`;

const Cards = styled.Image`
  width: 65px;
  height: 67px;
  margin-top: 20px;
`;

const ForegroundClayPot = styled.View`
  align-self: center;
  position: absolute;
  top: -10px;
`;

const Duration = styled.Text`
  align-self: center;
  color: white;
  font-size: 21px;
  font-weight: bold;
  position: absolute;
  bottom: 110px;
`;

const ButtonsContainer = styled.View`
  flex-direction: row;
  align-self: center;
  position: absolute;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  bottom: 40px;
`;

const ButtonLabel = styled.Text`
  color: #f1f1f1;
  font-size: 18px;
  font-weight: bold;
`;

const SecondRowLabel = styled(ButtonLabel)`
  margin-right: 5px;
`;

const OpenSecondRowWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

const styles = StyleSheet.create({
  button: {height: 60, width: 150, justifyContent: 'center'},
});

interface UnblockModalProps {
  toggleModal: () => void;
  selectedClayPotToUnblock?: PrizeSlot;
  unblock: (index: PrizeSlotIndexes) => void;
  open: (index: PrizeSlotIndexes) => void;
  speedupPrize: (index: PrizeSlotIndexes) => void;
  selectedClayPotIndex?: PrizeSlotIndexes;
  index: PrizeSlotIndexes;
  unblockButtonEnabled?: boolean;
  speedup?: boolean;
}

export const UnblockModal: React.FC<UnblockModalProps> = ({
  toggleModal,
  selectedClayPotToUnblock,
  unblock,
  open,
  selectedClayPotIndex,
  index,
  unblockButtonEnabled = true,
  speedup = false,
  speedupPrize,
}) => {
  let type: PrizeSlotTypes = PrizeSlotTypes.ONE;
  let level: LocationIndex = LocationIndex.ONE;
  let duration = 3;
  if (selectedClayPotToUnblock) {
    type = selectedClayPotToUnblock.type;
    level = selectedClayPotToUnblock.level;
    duration = selectedClayPotToUnblock.duration;
  }
  const timeLabel = UseCountDownLabel(selectedClayPotToUnblock?.expiry!);
  return (
    <TransparentWrapper>
      <MainContainer>
        <ContentWrapper>
          <HeaderWrapper>
            <CloseButton onPress={toggleModal}>
              <CloseLabel>X</CloseLabel>
            </CloseButton>
            <TitleLabel>
              {`${prizeSlotType[type]} - Livello ${level}`}
            </TitleLabel>
          </HeaderWrapper>
          <CentralContentWrapper>
            <BackgroundClayPotWrapper>
              <ClayPot color={'green'} width={100} height={130} />
            </BackgroundClayPotWrapper>
            <WhiteBox>
              <Label>Il bottino</Label>
              <Cards source={Images.cards} />
              <LabelWithMarginTop>X8</LabelWithMarginTop>
            </WhiteBox>
            <ForegroundClayPot>
              <ClayPot color={'#B4751E'} width={100} height={130} />
            </ForegroundClayPot>
          </CentralContentWrapper>
          <Duration>
            Tempo di sblocco: {speedup ? timeLabel : duration + 'h'}
          </Duration>
          <ButtonsContainer>
            <ButtonWithGradient
              style={styles.button}
              colors={['#96DF32', '#3CAC16']}
              onPress={() => open(index)}>
              <ButtonLabel>APRI SUBITO</ButtonLabel>
              <OpenSecondRowWrapper>
                <SecondRowLabel>38</SecondRowLabel>
                <Gem width={18} height={18} />
              </OpenSecondRowWrapper>
            </ButtonWithGradient>
            {unblockButtonEnabled && !speedup && (
              <ButtonWithGradient
                style={styles.button}
                colors={['#FFC565', '#F56A0F']}
                onPress={() => unblock(index)}>
                <ButtonLabel>SBLOCCA</ButtonLabel>
              </ButtonWithGradient>
            )}
            {speedup && (
              <ButtonWithGradient
                style={styles.button}
                colors={['#FFC565', '#F56A0F']}
                onPress={() => speedupPrize(index)}>
                <ButtonLabel>RIDUCI DI</ButtonLabel>
                <ButtonLabel>15 MINUTI</ButtonLabel>
              </ButtonWithGradient>
            )}
            {!unblockButtonEnabled && !speedup && (
              <View
                style={[
                  styles.button,
                  {
                    backgroundColor: '#DEDEDE',
                    borderRadius: 8,
                    padding: 10,
                    alignItems: 'center',
                  },
                ]}>
                <Text style={{color: '#5B5B5B', fontStyle: 'italic'}}>
                  Esiste già una giara
                </Text>
                <Text style={{color: '#5B5B5B', fontStyle: 'italic'}}>
                  in fase di sblocco
                </Text>
              </View>
            )}
          </ButtonsContainer>
        </ContentWrapper>
      </MainContainer>
    </TransparentWrapper>
  );
};
