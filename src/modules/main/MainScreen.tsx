import React, {useEffect, useState} from 'react';
import {Modal, View} from 'react-native';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {Images} from '@themes';
import {
  ClayPotSlot,
  FreeClayPot,
  PanoramaComponent,
  PlayButton,
  NewChallenges,
  InfoBar,
} from '@components';
import {StackNavigationProp} from '@react-navigation/stack';
import {MainScreenStackParamList} from '../app/routes';
import {
  getOpponentsFacesAction,
  opponentFacesSelector,
} from '../../adapters/redux/opponent';
import {AppState} from '../../adapters/redux/common';
import {
  freeClayPotsSelector,
  prizeSlotsSelector,
  serverTimeDifferenceSelector,
  unblockClayPotSlotAction,
  userCoinsSelector,
  userGemsSelector,
  userIdSelector,
  userImageUrlSelector,
  userPointsSelector,
} from '../../adapters/redux/user';
import {isIOS} from '@core';
import {
  PrizeSlot,
  PrizeSlotIndexes,
  PrizeSlots,
  FreeBox,
  isSpeedup,
  totalPoints,
} from '@entities';
import {EmptyClayPotSlot} from '../../components/ClayPotSlot/EmptyClayPotSlot';
import {UnblockModal} from './UnblockModal';

import {
  BackgroundImage,
  Character,
  ClayPotsContainer,
  FreeClayPotContainer,
  FreeMatchContainer,
  InfoBarWrapper,
  PlayButtonWrapper,
} from './styles/MainScreenStyles';
import {emptyMatchDataAction} from '../../adapters/redux/match';
import FastImage, {Source} from 'react-native-fast-image';

type MainScreenNavigationProp = StackNavigationProp<
  MainScreenStackParamList,
  'Main'
>;

interface MainScreenProps {
  navigation: MainScreenNavigationProp;
  getOpponentsFaces: () => void;
  uid?: string;
  imageUrl?: string;
  points?: {[key: string]: number};
  coins?: number;
  gems?: number;
  freeClayPots?: FreeBox[];
  getUserData: (token: string) => void;
  prizeSlots?: PrizeSlots;
  unblockClayPotSlotRedux: (index: PrizeSlotIndexes, uid: string) => void;
  serverTimeDifference?: number;
  emptyMatchData?: () => void;
  opponentsFaces?: {id: string; url: string}[];
}

export const MainScreen: React.FC<MainScreenProps> = ({
  navigation,
  getOpponentsFaces,
  uid,
  imageUrl = '',
  points = {one: 0},
  coins = 0,
  gems = 0,
  freeClayPots,
  prizeSlots,
  unblockClayPotSlotRedux,
  serverTimeDifference,
  emptyMatchData,
  opponentsFaces,
}) => {
  useEffect(() => {
    getOpponentsFaces();
    emptyMatchData!();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (opponentsFaces && opponentsFaces.length > 0) {
      const sources = opponentsFaces.map(({url}) => {
        if (url) {
          return {
            uri: url,
          };
        }
      }) as Source[];
      FastImage.preload(sources);
    }
  }, [opponentsFaces]);
  const [selectedClayPotToUnblock, setSelectedClayPotToUnblock] = useState<
    PrizeSlot | undefined
  >();
  const [selectedClayPotIndex, setSelectedClayPotIndex] = useState<
    PrizeSlotIndexes | undefined
  >();
  const claimFreeClayPot = () => console.log('claim free clay pot');
  const [isModalVisible, setModalVisible] = useState(false);
  const unblockClayPot = (index: PrizeSlotIndexes) => {
    setSelectedClayPotToUnblock(prizeSlots?.[index]);
    setSelectedClayPotIndex(index);
    setModalVisible(true);
  };
  const openClayPotSlot = (index: PrizeSlotIndexes) => {
    console.log('claim', index);
  };
  const unblockClayPotSlot = (index: PrizeSlotIndexes) => {
    unblockClayPotSlotRedux(index, uid!);
    setModalVisible(false);
  };

  return (
    <>
      {prizeSlots && (
        <Modal visible={isModalVisible} transparent={true} animationType="fade">
          <UnblockModal
            index={selectedClayPotIndex!}
            toggleModal={() => setModalVisible(!isModalVisible)}
            selectedClayPotToUnblock={selectedClayPotToUnblock}
            selectedClayPotIndex={selectedClayPotIndex}
            open={openClayPotSlot}
            unblock={unblockClayPotSlot}
            speedupPrize={() => console.log('speed up')}
            speedup={isSpeedup(prizeSlots)}
            unblockButtonEnabled={!isSpeedup}
          />
        </Modal>
      )}
      <PanoramaComponent>
        <BackgroundImage
          blurRadius={isIOS ? 4 : 1}
          source={Images.background}
        />
      </PanoramaComponent>
      <Character source={Images.character} />

      <View
        style={{justifyContent: 'space-between', flex: 1, paddingBottom: 10}}>
        <View style={{alignItems: 'center'}}>
          <InfoBarWrapper>
            <InfoBar
              profileImage={imageUrl}
              points={points ? totalPoints(points) : 0}
              coins={coins}
              gems={gems}
              onAddCoinsPressed={() => console.log('add coins!')}
              onAddGemsPressed={() => console.log('add gems!')}
            />
          </InfoBarWrapper>
          <FreeMatchContainer>
            <FreeClayPotContainer>
              <FreeClayPot
                serverTimeDifference={serverTimeDifference}
                timestamp={freeClayPots?.[0].expiration}
                claimFreeClayPot={claimFreeClayPot}
              />
            </FreeClayPotContainer>
            <NewChallenges />
          </FreeMatchContainer>
        </View>
        <View style={{alignItems: 'center'}}>
          <PlayButtonWrapper>
            <PlayButton onPress={() => navigation.navigate('ChooseLocation')} />
          </PlayButtonWrapper>

          <ClayPotsContainer>
            {prizeSlots?.one ? (
              <ClayPotSlot
                unblock={unblockClayPot}
                index={PrizeSlotIndexes.ONE}
                level={prizeSlots.one.level}
                timestamp={prizeSlots.one.expiry}
                isBlocked={prizeSlots.one.blocked}
                duration={prizeSlots.one.duration}
              />
            ) : (
              <EmptyClayPotSlot />
            )}
            {prizeSlots?.two ? (
              <ClayPotSlot
                unblock={unblockClayPot}
                index={PrizeSlotIndexes.TWO}
                level={prizeSlots.two.level}
                timestamp={prizeSlots.two.expiry}
                isBlocked={prizeSlots.two.blocked}
                duration={prizeSlots.two.duration}
              />
            ) : (
              <EmptyClayPotSlot />
            )}
            {prizeSlots?.three ? (
              <ClayPotSlot
                unblock={unblockClayPot}
                index={PrizeSlotIndexes.THREE}
                level={prizeSlots.three.level}
                timestamp={prizeSlots.three.expiry}
                isBlocked={prizeSlots.three.blocked}
                duration={prizeSlots.three.duration}
              />
            ) : (
              <EmptyClayPotSlot />
            )}
            {prizeSlots?.four ? (
              <ClayPotSlot
                index={PrizeSlotIndexes.FOUR}
                unblock={unblockClayPot}
                level={prizeSlots.four.level}
                timestamp={prizeSlots.four.expiry}
                isBlocked={prizeSlots.four.blocked}
                duration={prizeSlots.four.duration}
              />
            ) : (
              <EmptyClayPotSlot />
            )}
          </ClayPotsContainer>
        </View>
      </View>
    </>
  );
};

const mapStateToProps = (state: AppState) => ({
  points: userPointsSelector(state),
  imageUrl: userImageUrlSelector(state),
  coins: userCoinsSelector(state),
  gems: userGemsSelector(state),
  freeClayPots: freeClayPotsSelector(state),
  prizeSlots: prizeSlotsSelector(state),
  uid: userIdSelector(state),
  serverTimeDifference: serverTimeDifferenceSelector(state),
  opponentsFaces: opponentFacesSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  emptyMatchData: () => dispatch(emptyMatchDataAction()),
  getOpponentsFaces: () => dispatch(getOpponentsFacesAction()),
  unblockClayPotSlotRedux: (index: PrizeSlotIndexes, uid: string) =>
    dispatch(unblockClayPotSlotAction({index, uid})),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
