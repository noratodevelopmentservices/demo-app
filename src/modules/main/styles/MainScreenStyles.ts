import Styled from 'styled-components/native';
import {isIOS} from '@core';
import {verticalScale} from '@themes';

export const BackgroundImage = Styled.Image`
  position: absolute;
  left: -70px;
  top: 0px;
  flex: 1;
  height: 100%;
  width: 120%;
  resize-mode: cover;
`;

export const FreeMatchContainer = Styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Character = Styled.Image`
	position: absolute;
	top: ${verticalScale(300)}px;
  align-self: center;
  width: 280px;
  height: 480px;
`;

export const ClayPotsContainer = Styled.View`
  padding-horizontal: 5px;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;   
`;

export const PlayButtonWrapper = Styled.TouchableOpacity`
  align-self: center;
  margin-bottom: 30px;
`;

export const InfoBarWrapper = Styled.View`
  padding-top: ${isIOS ? 40 : 10}px;
  padding-horizontal: 5px;
`;

export const FreeClayPotContainer = Styled.View`
  margin-right: 10px;
`;
