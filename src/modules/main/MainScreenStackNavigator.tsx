import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MainScreen, ChooseLocationScreen} from '@modules';
import {MainScreenStackParamList} from '../app/routes';

const Stack = createStackNavigator<MainScreenStackParamList>();

export const MainScreenStackNavigator = () => {
  return (
    <Stack.Navigator headerMode={'none'}>
      <Stack.Screen name={'Main'} component={MainScreen} />
      <Stack.Screen name={'ChooseLocation'} component={ChooseLocationScreen} />
    </Stack.Navigator>
  );
};
