import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {MatchScreenComponent} from '../match';
import {QuestionData} from '@entities';

const questionData: QuestionData = {
  question:
    'Jaka,  il famoso artista reggae trapanese, ha dedicato una delle sue canzoni a una località sita in quale comune?',
  answers: [
    {answer: 'Paceco', correct: false},
    {answer: 'Petrosino', correct: false},
    {answer: 'Erice', correct: true},
    {answer: 'Marsala', correct: false},
  ],
};

const questions = [questionData];

storiesOf('Framework', module).add('Match Screen', () => (
  <MatchScreenComponent
    matchQuestions={questions}
    getMatchQuestions={() => console.log('get Match qujestions')}
    sendAnswer={() => console.log('send answer')}
  />
));
