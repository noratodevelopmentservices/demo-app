import React from 'react';
import {storiesOf} from '@storybook/react-native';
import {PreMatchScreenComponent} from '../preMatch';

storiesOf('Framework', module).add('Pre Match Screen', () => (
  <PreMatchScreenComponent
    points={{one: 125}}
    coins={500}
    gems={20}
    userName={'Mario'}
    navigation={{navigate: () => console.log('navigate')}}
  />
));
