import Styled from 'styled-components/native';
import {isIOS} from '@core';
import {StyleSheet} from 'react-native';

export const Wrapper = Styled.ImageBackground`
  flex: 1;
  padding-horizontal: 10px;
  padding-top: ${isIOS ? 50 : 10}px;
`;

export const HelpBarWrapper = Styled.View`
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 10px;
`;

export const QuestionImage = Styled.Image`
  width: 100%;
`;

export const QuestionWrapper = Styled.View`
  background-color: white;
  border-radius: 8px;
  shadow-radius: 4px;
  shadow-color: black;
  shadow-offset: 0px 0px;
  shadow-opacity: 1;
  padding: 5px;
  margin-top: 15px;
  margin-bottom: 5px;
`;

export const QuestionText = Styled.Text`
  font-size: 18px;
`;

export const AnswerWrapper = Styled.View`
  flex-direction: row;
  padding-vertical: ${isIOS ? 8 : 4}px;
  align-items: center;
  width: 100%;
`;

export const AnswerLetterWrapper = Styled.View`
  width: 15%;
`;

export const styles = StyleSheet.create({
  answerLetterGradient: {
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
});

export const AnswerLetter = Styled.Text`
  color: white;
  font-size: 32px;
  font-weight: bold;
  shadow-opacity: 1;
  shadow-offset: 0px 0px;
  shadow-color: black;
  shadow-radius: 3px;
`;

export const AnswerTextWrapper = Styled.TouchableOpacity`
  background-color: white;
  border-radius: 8px;
  shadow-radius: 4px;
  shadow-color: black;
  shadow-offset: 0px 0px;
  shadow-opacity: 1;
  padding: 10px;
  width: 85%;
`;

export const AnswerText = Styled.Text`
  font-size: 27px;
  color: ${(props: {color: string}) => props.color};
`;

export const MatchChatWrapper = Styled.View`
  width: 80px;
  position: absolute;
  top: ${isIOS ? 155 : 115}px;
  left: 10px;
`;
