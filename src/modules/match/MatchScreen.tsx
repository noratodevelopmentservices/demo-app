import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {Images} from '@themes';
import {CountdownBar, MatchChat, MatchUsers, HelpBar} from '@components';
import Gradient from 'react-native-linear-gradient';
import {ScrollView} from 'react-native-gesture-handler';
import {
  Wrapper,
  styles,
  AnswerLetter,
  AnswerLetterWrapper,
  AnswerText,
  AnswerTextWrapper,
  AnswerWrapper,
  HelpBarWrapper,
  MatchChatWrapper,
  QuestionImage,
  QuestionText,
  QuestionWrapper,
} from './MatchScreenStyles';
import {QuestionData} from '@entities';
import {
  getMatchQuestionsAction,
  matchIdSelector,
  matchQuestionsSelector,
  sendAnswerAction,
  sendChatMessageAction,
} from '../../adapters/redux/match';
import {AppState} from '../../adapters/redux/common';
import {ChatResource} from '../../components/MatchChat/MatchChatContent';

const answersStyle = [
  {key: 'A', colors: ['#FF5B5B', '#793131'], color: '#800909'},
  {key: 'B', colors: ['#A2C242', '#606E37'], color: '#416F05'},
  {key: 'C', colors: ['#E6AE2E', '#955B15'], color: '#C26312'},
  {key: 'D', colors: ['#3EACE8', '#075473'], color: '#186585'},
];

interface AnswerStyle {
  key: string;
  colors: string[];
  color: string;
}

interface MatchScreenProps {
  getMatchQuestions: (matchId: string) => void;
  matchQuestions?: QuestionData[];
  sendAnswer: (currentIndex: number, answerIndex: number) => void;
  sendChatMessage?: (chatMessage: ChatResource, matchId: string) => void;
  matchId?: string;
}

export const MatchScreenComponent: React.FC<MatchScreenProps> = ({
  matchQuestions,
  getMatchQuestions,
  sendAnswer,
  sendChatMessage,
  matchId,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  useEffect(() => {
    getMatchQuestions('test');
  }, [getMatchQuestions]);

  const sendChat = (chatMessage: ChatResource) => {
    sendChatMessage!(chatMessage, matchId!);
  };
  if (matchQuestions && matchQuestions.length > 0) {
    const questionData = matchQuestions[currentIndex];
    return (
      <Wrapper source={Images.trapaniBackground}>
        <CountdownBar duration={20000} />
        <MatchUsers
          userName={'Mario'}
          userPoints={0}
          opponentName={'Antonio'}
          opponentPoints={0}
        />
        <HelpBarWrapper>
          <HelpBar />
        </HelpBarWrapper>
        <ScrollView>
          <QuestionImage source={Images.jaka} />
          <QuestionWrapper>
            <QuestionText>{questionData.question}</QuestionText>
          </QuestionWrapper>
          {questionData.answers.map((answer, index) => {
            return (
              <AnswerWrapper key={answer.answer}>
                <AnswerLetterWrapper>
                  <Gradient
                    colors={answersStyle[index].colors}
                    style={styles.answerLetterGradient}>
                    <AnswerLetter>{answersStyle[index].key}</AnswerLetter>
                  </Gradient>
                </AnswerLetterWrapper>
                <AnswerTextWrapper
                  onPress={() => sendAnswer(currentIndex, index)}>
                  <AnswerText color={answersStyle[index].color}>
                    {answer.answer}
                  </AnswerText>
                </AnswerTextWrapper>
              </AnswerWrapper>
            );
          })}
        </ScrollView>
        <MatchChatWrapper>
          <MatchChat sendChatMessage={sendChat} />
        </MatchChatWrapper>
      </Wrapper>
    );
  }
  return null;
};

const mapStateToProps = (state: AppState) => ({
  matchQuestions: matchQuestionsSelector(state),
  matchId: matchIdSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  getMatchQuestions: (matchId: string) =>
    dispatch(getMatchQuestionsAction({matchId})),
  sendAnswer: (currentIndex: number, answerIndex: number) =>
    dispatch(sendAnswerAction({questionIndex: currentIndex, answerIndex})),
  sendChatMessage: (chatMessage: ChatResource, matchId: string) =>
    dispatch(sendChatMessageAction(chatMessage, matchId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MatchScreenComponent);
