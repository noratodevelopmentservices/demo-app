import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AppStackParamList} from './routes';
import {MatchScreen} from '../match';

const Stack = createStackNavigator<AppStackParamList>();

export const AppQuestionTestStackNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Match"
      mode="modal">
      <Stack.Screen name={'Match'} component={MatchScreen} />
    </Stack.Navigator>
  );
};
