import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AppStackParamList} from './routes';
import {AppTabBar} from './AppTabBar';
import {PreMatchScreen} from '../preMatch';
import {MatchScreen} from '../match';

const Stack = createStackNavigator<AppStackParamList>();

export const AppStackNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Main"
      mode="modal">
      <Stack.Screen name={'Main'} component={AppTabBar} />
      <Stack.Screen name={'PreMatch'} component={PreMatchScreen} />
      <Stack.Screen name={'Match'} component={MatchScreen} />
    </Stack.Navigator>
  );
};
