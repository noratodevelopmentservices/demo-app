import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {News} from '../../assets/icons/News';
import {Store} from '../../assets/icons/Store';
import {Main} from '../../assets/icons/Main';
import {Profile} from '../../assets/icons/Profile';
import {Events} from '../../assets/icons/Events';
import {isIOS} from '@core';

const icons: (isFocused: boolean) => {[key: string]: React.ReactNode} = (
  isFocused,
) => ({
  news: <News color={isFocused ? '#BDFF82' : undefined} />,
  store: <Store color={isFocused ? '#BDFF82' : undefined} />,
  main: <Main color={isFocused ? '#BDFF82' : undefined} />,
  profile: <Profile color={isFocused ? '#BDFF82' : undefined} />,
  events: <Events color={isFocused ? '#BDFF82' : undefined} />,
  undefined: <News />,
});

export const TabBar: React.FC<BottomTabBarProps> = ({
  state,
  descriptors,
  navigation,
}) => {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
        const iconName = options.tabBarIcon
          ? (options.tabBarIcon({
              focused: isFocused,
              color: 'grey',
              size: 21,
            }) as string)
          : 'undefined';
        const Icon: React.ReactNode = icons(isFocused)[iconName];
        if (isFocused) {
          return (
            <TouchableOpacity
              key={route.key}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                width: 120,
                backgroundColor: '#00000090',
                alignItems: 'center',
                justifyContent: 'center',
                paddingBottom: isIOS ? 10 : 0,
                borderRightWidth: 1,
                borderRightColor: 'white',
              }}>
              <View
                style={{width: '100%', height: 2, backgroundColor: '#BDFF82'}}
              />
              <View style={{paddingTop: 10}}>{Icon}</View>
              <Text
                style={{
                  padding: 3,
                  color: '#BDFF82',
                  fontSize: 16,
                  fontWeight: 'bold',
                  textTransform: 'uppercase',
                }}>
                {label}
              </Text>
            </TouchableOpacity>
          );
        }
        return (
          <TouchableOpacity
            key={route.key}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              backgroundColor: '#00000060',
              alignItems: 'center',
              justifyContent: 'center',
              borderRightWidth: 1,
              borderRightColor: 'white',
            }}>
            {Icon}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
