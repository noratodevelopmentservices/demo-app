export type AppStackParamList = {
  Main: undefined;
  PreMatch: undefined;
  Match: undefined;
};

export type AppTabParamList = {
  News: undefined;
  Store: undefined;
  Main: undefined;
  Profile: undefined;
  Events: undefined;
};

export type MainScreenStackParamList = {
  Main: undefined;
  ChooseLocation: undefined;
};
