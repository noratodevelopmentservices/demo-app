import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {TabBar} from './TabBar';
import {MailScreen} from '../mail';
import {ShoppingScreen} from '../shopping';
import {ProfileScreen} from '../profile';
import {EventsScreen} from '../events';
import {MainScreenStackNavigator} from '../main/MainScreenStackNavigator';
import {AppTabParamList} from './routes';

const Tab = createBottomTabNavigator<AppTabParamList>();

export const AppTabBar: React.FC = () => {
  return (
    <Tab.Navigator
      initialRouteName={'Main'}
      tabBar={(props) => <TabBar {...props} />}>
      <Tab.Screen
        name={'News'}
        component={MailScreen}
        options={{
          tabBarLabel: 'News',
          tabBarIcon: () => 'news',
        }}
      />
      <Tab.Screen
        name={'Store'}
        component={ShoppingScreen}
        options={{
          tabBarLabel: 'Store',
          tabBarIcon: () => 'store',
        }}
      />
      <Tab.Screen
        name={'Main'}
        component={MainScreenStackNavigator}
        options={{
          tabBarLabel: 'Main',
          tabBarIcon: () => 'main',
        }}
      />
      <Tab.Screen
        name={'Profile'}
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: () => 'profile',
        }}
      />
      <Tab.Screen
        name={'Events'}
        component={EventsScreen}
        options={{
          tabBarLabel: 'Events',
          tabBarIcon: () => 'events',
        }}
      />
    </Tab.Navigator>
  );
};
