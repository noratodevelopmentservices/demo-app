import React from 'react';
import {View, Text} from 'react-native';

export const ShoppingScreen: React.FC = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Shopping Screen</Text>
    </View>
  );
};
