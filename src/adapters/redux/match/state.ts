import {MatchState} from './types';

export const MATCH_INITIAL_STATE: MatchState = {
  matchId: undefined,
  questions: [],
  opponent: {
    name: undefined,
    points: undefined,
  },
};
