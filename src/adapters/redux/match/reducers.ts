import {MATCH_INITIAL_STATE} from './state';
import {BaseAction} from '../common';
import {
  EMPTY_MATCH_DATA_ACTION,
  SAVE_MATCH_DATA_ACTION,
  SAVE_MATCH_QUESTIONS_ACTION,
} from './constants';

export const matchReducer = (
  state: any = MATCH_INITIAL_STATE,
  action: BaseAction,
) => {
  switch (action.type) {
    case SAVE_MATCH_QUESTIONS_ACTION:
      return {...state, questions: action.payload.questions};
    case SAVE_MATCH_DATA_ACTION:
      const {
        matchData: {matchId, opponent},
      } = action.payload;
      return {...state, matchId, opponent};
    case EMPTY_MATCH_DATA_ACTION:
      return MATCH_INITIAL_STATE;
    default:
      return state;
  }
};
