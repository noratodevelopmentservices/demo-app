import {BaseAction} from '../common';
import {Match, Opponent, QuestionData} from '@entities';

export interface MatchState {
  readonly matchId?: string;
  readonly questions?: QuestionData[];
  readonly opponent?: Opponent;
}

export interface GetMatchQuestionsActionType extends BaseAction {
  payload: GetMatchQuestionsActionPayload;
}

export interface GetMatchQuestionsActionPayload {
  matchId: string;
}

export interface SaveMatchQuestionActionType extends BaseAction {
  payload: SaveMatchQuestionsActionPayload;
}

export interface SaveMatchQuestionsActionPayload {
  questions: QuestionData[];
}

export interface SendAnswerActionType extends BaseAction {
  payload: SendAnswerActionPayload;
}

export interface SendAnswerActionPayload {
  questionIndex: number;
  answerIndex: number;
}

export interface CreateMatchActionType extends BaseAction {
  payload: CreateMatchActionPayload;
}

export interface CreateMatchActionPayload {
  userId?: string;
}

export interface SaveMatchDataActionType extends BaseAction {
  payload: SaveMatchDataActionPayload;
}

export interface SaveMatchDataActionPayload {
  matchData: Match;
}

export interface SendChatMessageActionType extends BaseAction {
  payload: SendChatMessageActionPayload;
}

export interface SendChatMessageActionPayload {
  matchId: string;
  chatMessage: {key: string};
}
