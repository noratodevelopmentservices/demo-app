export const GET_MATCH_QUESTIONS_ACTION = 'match/sagas/getMatchQuestionsAction';
export const SAVE_MATCH_QUESTIONS_ACTION = 'match/saveMatchQuestionsAction';
export const SEND_ANSWER_ACTION = 'match/sagas/senAnswerAction';
export const CREATE_MATCH_ACTION = 'match/sagas/createMatchAction';
export const SAVE_MATCH_DATA_ACTION = 'match/saveMatchDataAction';
export const EMPTY_MATCH_DATA_ACTION = 'match/emptyMatchDataAction';
export const SEND_CHAT_MESSAGE_ACTION = 'match/sagas/sendChatMessage';
