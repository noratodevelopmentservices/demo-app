import {
  CREATE_MATCH_ACTION,
  EMPTY_MATCH_DATA_ACTION,
  GET_MATCH_QUESTIONS_ACTION,
  SAVE_MATCH_DATA_ACTION,
  SAVE_MATCH_QUESTIONS_ACTION,
  SEND_ANSWER_ACTION,
  SEND_CHAT_MESSAGE_ACTION,
} from './constants';
import {
  CreateMatchActionPayload,
  CreateMatchActionType,
  GetMatchQuestionsActionPayload,
  GetMatchQuestionsActionType,
  SaveMatchDataActionPayload,
  SaveMatchDataActionType,
  SaveMatchQuestionActionType,
  SaveMatchQuestionsActionPayload,
  SendAnswerActionPayload,
  SendAnswerActionType,
  SendChatMessageActionType,
} from './types';
import {BaseAction} from '../common';
import {ChatResource} from '../../../components/MatchChat/MatchChatContent';

export const getMatchQuestionsAction: (
  payload: GetMatchQuestionsActionPayload,
) => GetMatchQuestionsActionType = (payload) => ({
  type: GET_MATCH_QUESTIONS_ACTION,
  payload,
});

export const saveMatchQuestionAction: (
  payload: SaveMatchQuestionsActionPayload,
) => SaveMatchQuestionActionType = (payload) => ({
  type: SAVE_MATCH_QUESTIONS_ACTION,
  payload,
});

export const sendAnswerAction: (
  payload: SendAnswerActionPayload,
) => SendAnswerActionType = (payload) => ({
  type: SEND_ANSWER_ACTION,
  payload,
});

export const createMatchAction: (
  payload: CreateMatchActionPayload,
) => CreateMatchActionType = (payload) => ({
  type: CREATE_MATCH_ACTION,
  payload,
});

export const saveMatchDataAction: (
  payload: SaveMatchDataActionPayload,
) => SaveMatchDataActionType = (payload) => ({
  type: SAVE_MATCH_DATA_ACTION,
  payload,
});

export const emptyMatchDataAction: () => BaseAction = () => ({
  type: EMPTY_MATCH_DATA_ACTION,
});

export const sendChatMessageAction: (
  chatMessage: ChatResource,
  matchId: string,
) => SendChatMessageActionType = (chatMessage, matchId) => {
  return {
    type: SEND_CHAT_MESSAGE_ACTION,
    payload: {
      matchId,
      chatMessage: {key: chatMessage.key},
    },
  };
};
