import {AppState} from '../common';

export const matchQuestionsSelector = (state: AppState) =>
  state.match?.questions;

export const matchOpponentSelector = (state: AppState) => {
  return state.match?.opponent;
};

export const matchIdSelector = (state: AppState) => state.match?.matchId;
