import {call} from 'redux-saga/effects';
import {GetMatchQuestionsActionType} from '../types';
import {GetMatchQuestionsInteractor} from '../../../../useCases/getMatchQuestionsInteractor';
import {getMatchService} from '../../../../services/MatchService';
import {StateService} from '@services';

export function* getMatchQuestionsSaga(
  action: GetMatchQuestionsActionType,
  interactor?: GetMatchQuestionsInteractor,
) {
  const {matchId} = action.payload;
  if (!interactor) {
    const matchService = getMatchService();
    const stateService = new StateService();
    interactor = new GetMatchQuestionsInteractor({matchService, stateService});
  }
  yield call([interactor, 'execute'], {matchId});
}
