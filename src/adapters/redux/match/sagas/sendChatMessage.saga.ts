import {call} from 'redux-saga/effects';
import {SendChatMessageActionType} from '../types';
import {SendChatMessageInteractor} from '../../../../useCases/SendChatMessageInteractor';
import {getMatchService} from '../../../../services/MatchService';
import {StateService} from '@services';

export function* sendChatMessageSaga(
  action: SendChatMessageActionType,
  interactor?: SendChatMessageInteractor,
) {
  const matchService = getMatchService();
  const stateService = new StateService();
  if (!interactor) {
    interactor = new SendChatMessageInteractor({matchService, stateService});
  }
  yield call([interactor, 'execute'], action.payload);
}
