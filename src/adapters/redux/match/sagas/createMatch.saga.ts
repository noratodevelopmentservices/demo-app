import {call} from 'redux-saga/effects';
import {CreateMatchActionType} from '../types';
import {CreateMatchInteractor} from '../../../../useCases/createMatchInteractor';
import {getMatchService} from '../../../../services/MatchService';
import {StateService} from '@services';

export function* createMatchSaga(
  action: CreateMatchActionType,
  interactor?: CreateMatchInteractor,
) {
  const matchService = getMatchService();
  const stateService = new StateService();
  if (!interactor) {
    interactor = new CreateMatchInteractor({matchService, stateService});
  }
  yield call([interactor, 'execute'], {userId: action.payload.userId});
}
