import {STARTUP_INITIAL_STATE} from './state';

export const startupReducer = (
  state: any = STARTUP_INITIAL_STATE,
  action: any,
) => {
  switch (action.type) {
    default:
      return state;
  }
};
