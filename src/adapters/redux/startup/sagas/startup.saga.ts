import {fork} from 'redux-saga/effects';
import {userFlowSaga} from '../../user/sagas/userFlow.saga';
import {syncWithServerTimeSaga} from '../../user/sagas';
export function* startupSaga() {
  yield fork(userFlowSaga);
  yield fork(syncWithServerTimeSaga);
}
