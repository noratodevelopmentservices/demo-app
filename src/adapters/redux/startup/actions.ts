import {BaseAction} from '../common';
import {STARTUP_ACTION} from './constants';

export const startupAction: () => BaseAction = () => ({
  type: STARTUP_ACTION,
});
