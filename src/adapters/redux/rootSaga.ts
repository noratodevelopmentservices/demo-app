import {takeLatest, all} from 'redux-saga/effects';
import {
  GET_USER_DATA_ACTION,
  getUserDataSaga,
  ANONYMOUS_LOGIN_ACTION,
  anonymousLoginSaga,
  UNBLOCK_CLAY_POT_SLOT_ACTION,
  unblockClayPotSlotSaga,
  syncWithServerTimeSaga,
  SYNC_WITH_SERVER_TIME_ACTION,
  SET_USER_ONLINE_STATUS_ACTION,
  setUserOnlineStatusSaga,
} from './user';

import {STARTUP_ACTION, startupSaga} from './startup';

import {GET_OPPONENTS_FACES_ACTION, getOpponentsFacesSaga} from './opponent';

import {
  GET_MATCH_QUESTIONS_ACTION,
  getMatchQuestionsSaga,
  CREATE_MATCH_ACTION,
  createMatchSaga,
  SEND_CHAT_MESSAGE_ACTION,
  sendChatMessageSaga,
} from './match';

export default function* root() {
  yield all([
    takeLatest(ANONYMOUS_LOGIN_ACTION, anonymousLoginSaga),
    takeLatest(GET_OPPONENTS_FACES_ACTION, getOpponentsFacesSaga),
    takeLatest(GET_USER_DATA_ACTION, getUserDataSaga),
    takeLatest(STARTUP_ACTION, startupSaga),
    takeLatest(UNBLOCK_CLAY_POT_SLOT_ACTION, unblockClayPotSlotSaga),
    takeLatest(SYNC_WITH_SERVER_TIME_ACTION, syncWithServerTimeSaga),
    takeLatest(GET_MATCH_QUESTIONS_ACTION, getMatchQuestionsSaga),
    takeLatest(CREATE_MATCH_ACTION, createMatchSaga),
    takeLatest(SEND_CHAT_MESSAGE_ACTION, sendChatMessageSaga),
    takeLatest(SET_USER_ONLINE_STATUS_ACTION, setUserOnlineStatusSaga),
  ]);
}
