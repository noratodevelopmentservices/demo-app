import {combineReducers} from 'redux';
import configureStore from './createStore';
import rootSaga from './rootSaga';
import {userReducer} from './user';
import {opponentReducer} from './opponent';
import {startupReducer} from './startup';
import {matchReducer} from './match/reducers';

export const reducers = combineReducers({
  user: userReducer,
  opponent: opponentReducer,
  startup: startupReducer,
  match: matchReducer,
});

export default () => {
  let {store, persistor, sagaMiddleware} = configureStore(reducers, rootSaga);

  return {store, persistor, sagaMiddleware};
};
