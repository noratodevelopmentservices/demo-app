export const RESET = 'navigation/reset';
export const NAVIGATE_TO = 'navigation/navigateTo';
export const BACK = 'navigation/back';
