import {NAVIGATE_TO, RESET, BACK} from './constants';
import {NavigateToActionType, ResetActionType} from './types';
import {BaseAction} from '../common';

export const reset: (payload: {routeName: string}) => ResetActionType = (
  payload,
) => ({
  type: RESET,
  payload,
});

export const navigateTo: (payload: {
  routeName: string;
  params?: any;
}) => NavigateToActionType = (payload) => ({
  type: NAVIGATE_TO,
  payload,
});

export const back: () => BaseAction = () => ({
  type: BACK,
});
