import {BACK, NAVIGATE_TO, RESET} from './constants';
import NavigationService from '../../../services/NavigationService';

export default () => (next: any) => (action: any) => {
  const {type, payload} = action;
  if (type === NAVIGATE_TO) {
    const {routeName, params} = payload;
    NavigationService.navigate({routeName}, params);
  }
  if (type === RESET) {
    const {routeName} = payload;
    NavigationService.reset({routeName});
  }
  if (type === BACK) {
    NavigationService.back();
  }
  next(action);
};
