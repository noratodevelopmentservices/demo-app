import {BaseAction} from '../common';

export interface NavigateToActionType extends BaseAction {
  payload: {
    routeName: string;
    params?: any;
  };
}

export interface ResetActionType extends BaseAction {
  payload: {
    routeName: string;
  };
}
