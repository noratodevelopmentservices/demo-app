export interface StateProvider {
  put(action: any): void;
  select(selector: any): any;
  all(sideEffects: any[]): any;
  call(sideEffect: any, params?: any): any;
}

export class StateAdapter implements StateProvider {
  public constructor(private stateAdapter: any) {}

  public async put(action: any): Promise<any> {
    const {stateAdapter: adaptee} = this;
    return await adaptee.callPut(action);
  }

  public async select(selector: any): Promise<any> {
    const {stateAdapter: adaptee} = this;

    return await adaptee.callSelect(selector);
  }

  public async all(sideEffects: any): Promise<any> {
    const {stateAdapter: adaptee} = this;
    return await adaptee.callAll(sideEffects);
  }

  public async call(sideEffect: any, params?: any): Promise<any> {
    const {stateAdapter: adaptee} = this;
    return await adaptee.callCall(sideEffect, params);
  }
}
