import {put, select, all, call} from 'redux-saga/effects';

class ReduxSagaStateManager {
  public store: any = undefined;

  public setStore = (store: any) => (this.store = store);

  public async callPut(putMsg: any) {
    const putEffect = function* () {
      yield put(putMsg);
    };

    return (this.store as any).runSaga(putEffect).done;
  }

  public async callAll(effects: any) {
    const allEffect = function* () {
      yield all(effects);
    };
    return (this.store as any).runSaga(allEffect).done;
  }

  public async callCall(effect: any, params?: any) {
    const callEffect = function* () {
      yield call(effect, params);
    };
    return (this.store as any).runSaga(callEffect).done;
  }

  public async callSelect(selector: any) {
    let selectEffect = function* () {
      return yield select(selector);
    };
    return (this.store as any).runSaga(selectEffect).done;
  }
}

export default new ReduxSagaStateManager();
