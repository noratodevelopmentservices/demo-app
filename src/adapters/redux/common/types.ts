import {UserState} from '../user';
import {OpponentState} from '../opponent';
import {MatchState} from '../match';

export interface BaseAction {
  type: string;
  payload?: any;
}

export interface AppState {
  readonly user?: UserState;
  readonly opponent?: OpponentState;
  readonly match?: MatchState;
}
