import createSagaMiddleware from 'redux-saga';
import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import ReduxSagaStateManager from './common/sagas/ReduxSagaStateManager';
import Logger from 'redux-logger';

export default (rootReducer: any, rootSaga: any) => {
  const middleware = [];
  const enhancers = [];

  const sagaMiddleware = createSagaMiddleware({});
  middleware.push(sagaMiddleware);

  middleware.push(Logger);
  enhancers.push(applyMiddleware(...middleware));

  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2,
  };

  const pReducer = persistReducer(persistConfig, rootReducer);
  const store = {
    ...createStore(pReducer, compose(...enhancers)),
    runSaga: sagaMiddleware.run,
  };
  const persistor = persistStore(store, undefined, () =>
    ReduxSagaStateManager.setStore(store),
  );

  let sagaManager = sagaMiddleware.run(rootSaga);

  return {
    store,
    sagaManager,
    sagaMiddleware,
    persistor,
  };
};
