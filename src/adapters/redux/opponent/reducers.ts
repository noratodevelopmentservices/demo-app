import {BaseAction} from '../common';
import {SAVE_OPPONENTS_FACES_ACTION} from './constants';
import {OPPONENT_INITIAL_STATE} from './state';
import {OpponentState} from './types';

export const opponentReducer: (
  state: any,
  action: BaseAction,
) => OpponentState = (state = OPPONENT_INITIAL_STATE, action) => {
  const {type, payload} = action;
  switch (type) {
    case SAVE_OPPONENTS_FACES_ACTION:
      const {opponentsFaces} = payload;
      return {...state, opponentsFaces};
    default:
      return state;
  }
};
