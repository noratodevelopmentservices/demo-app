export const GET_OPPONENTS_FACES_ACTION =
  'opponent/saga/getOpponentsFacesAction';
export const SAVE_OPPONENTS_FACES_ACTION = 'opponent/saveOpponentsFacesAction';
export const GET_OPPONENT_DATA_ACTION = 'opponent/saga/getOpponentDataAction';
