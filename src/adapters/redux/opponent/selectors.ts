import {AppState} from '../common';

export const opponentFacesSelector = (state: AppState) =>
  state.opponent?.opponentsFaces;
