import {BaseAction} from '../common';
import {
  GET_OPPONENTS_FACES_ACTION,
  SAVE_OPPONENTS_FACES_ACTION,
} from './constants';
import {OpponentFace} from '@entities';
import {SaveOpponentsFacesActionType} from './types';

export const getOpponentsFacesAction: () => BaseAction = () => ({
  type: GET_OPPONENTS_FACES_ACTION,
});

export const saveOpponentsFacesAction: (
  opponentsFaces: OpponentFace[],
) => SaveOpponentsFacesActionType = (opponentsFaces) => ({
  type: SAVE_OPPONENTS_FACES_ACTION,
  payload: {
    opponentsFaces,
  },
});

