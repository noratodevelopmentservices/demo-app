import {call} from 'redux-saga/effects';
import {BaseAction} from '../../common';
import {GetOpponentsFacesInteractor} from '../../../../useCases/getOpponentsFacesInteractor';
import {OpponentService, StateService} from '@services';

export function* getOpponentsFacesSaga(
  action: BaseAction,
  interactor?: GetOpponentsFacesInteractor,
) {
  if (!interactor) {
    const stateService = new StateService();
    const opponentService = OpponentService;
    interactor = new GetOpponentsFacesInteractor({
      stateService,
      opponentService,
    });
  }

  yield call([interactor, 'execute'], null);
}
