import {BaseAction} from '../common';
import {OpponentFace} from '@entities';

export interface OpponentState {
  readonly opponentsFaces?: OpponentFace[];
}

export interface SaveOpponentsFacesActionType extends BaseAction {
  payload: SaveOpponentsFacesActionPayload;
}

export interface SaveOpponentsFacesActionPayload {
  opponentsFaces: OpponentFace[];
}
