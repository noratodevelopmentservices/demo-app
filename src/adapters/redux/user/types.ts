import {BaseAction} from '../common';
import {
  User,
  Stats,
  FreeBox,
  LocationStats,
  PrizeSlotIndexes,
  PrizeSlots,
} from '@entities';

export interface UserState {
  readonly id?: string;
  readonly token?: string;
  readonly name?: string;
  readonly imageUrl?: string;
  readonly points?: {[key: string]: number};
  readonly coins?: number;
  readonly gems?: number;
  readonly stats?: Stats;
  readonly freeBoxes?: FreeBox[];
  readonly locationStats?: LocationStats;
  readonly prizeSlots?: PrizeSlots;
  readonly serverTimeDifference?: number;
}

export interface GetUserDataActionType extends BaseAction {
  payload: GetUserDataActionPayload;
}

export interface GetUserDataActionPayload {
  uid: string;
}

export interface SaveUserDataActionType extends BaseAction {
  payload: SaveUserDataActionPayload;
}

export interface SaveUserDataActionPayload {
  userData: User;
}

export interface UnblockClayPotSlotActionType extends BaseAction {
  payload: UnblockClayPotSlotActionPayload;
}

export interface UnblockClayPotSlotActionPayload {
  index: PrizeSlotIndexes;
  uid: string;
}

export interface SaveSyncWithServerTimeActionType extends BaseAction {
  payload: SaveSyncWithServerTimeActionPayload;
}

export interface SaveSyncWithServerTimeActionPayload {
  difference: number;
}

export interface SetUserOnlineStatusActionType extends BaseAction {
  payload: SetUserOnlineStatusActionPayload;
}

export interface SetUserOnlineStatusActionPayload {
  isOnline: boolean;
  uuid: string;
}
