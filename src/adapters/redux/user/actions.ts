import {
  ANONYMOUS_LOGIN_ACTION,
  GET_USER_DATA_ACTION,
  REGISTER_ACTION,
  SAVE_SYNC_WITH_SERVER_TIME_ACTION,
  SAVE_USER_DATA_ACTION, SET_USER_ONLINE_STATUS_ACTION,
  SYNC_WITH_SERVER_TIME_ACTION,
  UNBLOCK_CLAY_POT_SLOT_ACTION,
} from './constants';
import {
  GetUserDataActionPayload,
  GetUserDataActionType,
  SaveSyncWithServerTimeActionType,
  SaveUserDataActionPayload,
  SaveUserDataActionType, SetUserOnlineStatusActionType,
  UnblockClayPotSlotActionPayload,
  UnblockClayPotSlotActionType,
} from './types';
import {BaseAction} from '../common';

export const anonymousLoginAction: () => BaseAction = () => ({
  type: ANONYMOUS_LOGIN_ACTION,
});

export const getUserDataAction: (
  payload: GetUserDataActionPayload,
) => GetUserDataActionType = (payload) => ({
  type: GET_USER_DATA_ACTION,
  payload,
});

export const saveUserDataAction: (
  payload: SaveUserDataActionPayload,
) => SaveUserDataActionType = (payload) => ({
  type: SAVE_USER_DATA_ACTION,
  payload,
});

export const registerAction: () => BaseAction = () => ({
  type: REGISTER_ACTION,
});

export const unblockClayPotSlotAction: (
  payload: UnblockClayPotSlotActionPayload,
) => UnblockClayPotSlotActionType = (payload) => ({
  type: UNBLOCK_CLAY_POT_SLOT_ACTION,
  payload,
});

export const syncWithServerTimeAction: () => BaseAction = () => ({
  type: SYNC_WITH_SERVER_TIME_ACTION,
});

export const saveSyncWithServerTimeAction: (data: {
  difference: number;
}) => SaveSyncWithServerTimeActionType = ({difference}) => ({
  type: SAVE_SYNC_WITH_SERVER_TIME_ACTION,
  payload: {
    difference,
  },
});

export const setUserOnlineStatusAction: (
  isOnline: boolean,
  uuid: string,
) => SetUserOnlineStatusActionType = (isOnline, uuid: string) => ({
  type: SET_USER_ONLINE_STATUS_ACTION,
  payload: {
    isOnline,
    uuid,
  },
});
