import {AppState} from '../common';

export const userIdSelector = (state: AppState) => state.user?.id;
export const userNameSelector = (state: AppState) => state.user?.name;
export const userImageUrlSelector = (state: AppState) => state.user?.imageUrl;
export const userPointsSelector = (state: AppState) => state.user?.points;
export const userCoinsSelector = (state: AppState) => state.user?.coins;
export const userGemsSelector = (state: AppState) => state.user?.gems;
export const freeClayPotsSelector = (state: AppState) => state.user?.freeBoxes;
export const userstatsSelector = (state: AppState) => state.user?.stats;
export const userLocationStatsSelector = (state: AppState) =>
  state.user?.locationStats;
export const prizeSlotsSelector = (state: AppState) => state.user?.prizeSlots;
export const serverTimeDifferenceSelector = (state: AppState) =>
  state.user?.serverTimeDifference;
