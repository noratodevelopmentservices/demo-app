import {
  SaveSyncWithServerTimeActionPayload,
  SaveUserDataActionPayload,
  UserState,
} from './types';
import {USER_INITIAL_STATE} from './state';
import {
  SAVE_SYNC_WITH_SERVER_TIME_ACTION,
  SAVE_USER_DATA_ACTION,
} from './constants';

export const userReducer: (state: any, action: any) => UserState = (
  state = USER_INITIAL_STATE,
  action,
) => {
  switch (action.type) {
    case SAVE_USER_DATA_ACTION:
      let payloadSaveUserData: SaveUserDataActionPayload = action.payload;
      return {
        ...state,
        ...payloadSaveUserData.userData,
      };
    case SAVE_SYNC_WITH_SERVER_TIME_ACTION:
      let payloadSyncServer: SaveSyncWithServerTimeActionPayload =
        action.payload;
      return {
        ...state,
        serverTimeDifference: payloadSyncServer.difference,
      };
    default:
      return state;
  }
};
