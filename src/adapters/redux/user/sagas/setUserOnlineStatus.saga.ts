import {call} from 'redux-saga/effects';
import {SetUserOnlineStatusActionType} from '../types';
import {SetUserOnlineStatusInteractor} from '../../../../useCases/setUserOnlineStatusInteractor';
import {getUserService} from '../../../../services/UserService';

export function* setUserOnlineStatusSaga(
  action: SetUserOnlineStatusActionType,
  interactor?: SetUserOnlineStatusInteractor,
) {
  const userService = getUserService();
  const {isOnline, uuid} = action.payload;
  if (!interactor) {
    interactor = new SetUserOnlineStatusInteractor({userService});
  }
  yield call([interactor, 'execute'], {isOnline, uuid});
}
