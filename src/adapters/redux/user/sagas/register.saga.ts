import {call} from 'redux-saga/effects';
import {BaseAction} from '../../common';
import {RegisterInteractor} from '../../../../useCases/registerInteractor';
import {StateService, UserService} from '@services';

export function* register(action: BaseAction, interactor: RegisterInteractor) {
  if (!interactor) {
    const userService = UserService;
    const stateService = new StateService();
    interactor = new RegisterInteractor({userService, stateService});
  }

  yield call([interactor, 'execute'], null);
}
