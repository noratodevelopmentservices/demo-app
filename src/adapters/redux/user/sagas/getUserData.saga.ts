import {call} from 'redux-saga/effects';
import {GetUserDataActionType} from '../types';
import {GetUserDataInteractor} from '../../../../useCases/getUserDataInteractor';
import {StateService, UserService} from '@services';

export function* getUserDataSaga(
  action: GetUserDataActionType,
  interactor?: GetUserDataInteractor,
) {
  const {uid} = action.payload;
  if (!interactor) {
    const stateService = new StateService();
    const userService = UserService;
    interactor = new GetUserDataInteractor({stateService, userService});
  }

  yield call([interactor, 'execute'], {uid});
}
