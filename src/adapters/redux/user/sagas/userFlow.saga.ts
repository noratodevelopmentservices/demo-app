import {take, fork, call, put, select} from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';
import auth from '@react-native-firebase/auth';
import firestore, {
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';
import {
  anonymousLoginAction,
  getUserDataAction,
  saveUserDataAction,
} from '../actions';
import {userIdSelector} from '../selectors';
import {User} from '@entities';

export function* userFlowSaga() {
  yield fork(read);
}

function* read() {
  const channel = yield call(subscribe);
  while (true) {
    const action = yield take(channel);
    yield put(action);
  }

  // auth()
  //   .signOut()
  //   .then(() => console.log('logout'))
  //   .catch(() => console.log('logout error'));
}

function* subscribe() {
  // noinspection JSPotentiallyInvalidConstructorUsage
  const uid = yield select(userIdSelector);
  return eventChannel((emit) => {
    const setUser = (user: any) => {
      if (user) {
        emit(getUserDataAction({uid: user.uid}));
      } else {
        emit(anonymousLoginAction());
      }
    };
    auth().onAuthStateChanged(setUser);

    const setUserData = (doc: FirebaseFirestoreTypes.DocumentSnapshot) => {
      const user: User = doc.data() as User;
      emit(saveUserDataAction({userData: user}));
    };
    firestore().doc(`users/${uid}`).onSnapshot(setUserData);
    return () => {
      console.log('unsubscribe');
    };
  });
}
