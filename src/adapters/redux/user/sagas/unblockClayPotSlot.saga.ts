import {call} from 'redux-saga/effects';
import {UnblockClayPotSlotActionType} from '../types';
import {StateService, UserService} from '@services';
import {UnblockClayPutSlotInteractor} from '../../../../useCases/UnblockClayPutSlotInteractor';

export function* unblockClayPotSlotSaga(
  action: UnblockClayPotSlotActionType,
  interactor?: UnblockClayPutSlotInteractor,
) {
  const {index, uid} = action.payload;
  const userService = UserService;
  const stateService = new StateService();
  if (!interactor) {
    interactor = new UnblockClayPutSlotInteractor({userService, stateService});
  }

  yield call([interactor, 'execute'], {index, uid});
}
