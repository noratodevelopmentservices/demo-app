import {call} from 'redux-saga/effects';
import {AnonymLoginInteractor} from '@useCases';
import {StateService, UserService} from '@services';
import {DeviceInfoService} from '../../../../services/DeviceInfoService';
import {BaseAction} from '../../common';

export function* anonymousLoginSaga(
  action: BaseAction,
  anonymousInteractor?: AnonymLoginInteractor,
) {
  const stateService = new StateService();
  const userService = UserService;
  const deviceInfoService = new DeviceInfoService();
  if (!anonymousInteractor) {
    anonymousInteractor = new AnonymLoginInteractor({
      stateService,
      userService,
      deviceInfoService,
    });
  }
  yield call([anonymousInteractor, 'execute'], action.payload);
}
