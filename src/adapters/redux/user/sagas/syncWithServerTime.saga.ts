import {BaseAction} from '../../common';
import {getApiService} from '../../../../services/RealServices/ApiService';
import {StateService} from '@services';
import {SyncWithServerTimeInteractor} from '../../../../useCases/syncWithServerTimeInteractor';
import {call} from 'redux-saga/effects';

export function* syncWithServerTimeSaga(
  action?: BaseAction,
  interactor?: SyncWithServerTimeInteractor,
) {
  const apiService = getApiService();
  const stateService = new StateService();
  if (!interactor) {
    interactor = new SyncWithServerTimeInteractor({apiService, stateService});
  }

  yield call([interactor, 'execute'], {});
}
