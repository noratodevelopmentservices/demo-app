import {UserState} from './types';

export const USER_INITIAL_STATE: UserState = {
  id: undefined,
  token: undefined,
  name: undefined,
  imageUrl: undefined,
  points: undefined,
  coins: undefined,
  gems: undefined,
  stats: undefined,
  locationStats: undefined,
  serverTimeDifference: undefined,
};
