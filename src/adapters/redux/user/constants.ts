export const GET_USER_DATA_ACTION = 'user/saga/getUserAction';
export const SAVE_USER_DATA_ACTION = 'user/saveUserDataAction';
export const REGISTER_ACTION = 'user/saga/registerAction';
export const ANONYMOUS_LOGIN_ACTION = 'user/saga/anonymousLoginAction';
export const UNBLOCK_CLAY_POT_SLOT_ACTION =
  'user/saga/unblockClayPotSlotAction';
export const SYNC_WITH_SERVER_TIME_ACTION =
  'user/saga/syncWithServerTimeAction';

export const SAVE_SYNC_WITH_SERVER_TIME_ACTION =
  'user/saveSyncWithServerTimeAction';
export const SET_USER_ONLINE_STATUS_ACTION =
  'user/sagas/setUserOnlineStatusAction';
