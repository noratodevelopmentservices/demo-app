import isObject from './is-object';
import serialize from './serialize';
import {combineURL, isAbsoluteURL} from './url';

export type AllowedFetchMethod =
  | 'GET'
  | 'POST'
  | 'PUT'
  | 'PATCH'
  | 'DELETE'
  | 'HEAD'
  | 'OPTIONS';
export type AllowedResponseType =
  | 'json'
  | 'text'
  | 'blob'
  | 'arraybuffer'
  | 'formdata';

export interface AuthToken {
  token: string;
  type?: string;
}

export interface RequestConfig {
  json?: object;
  form?: object;
  params?: object;
  headers?: object;
  responseType?: AllowedResponseType;
}

const DEFAULT_FETCH_OPTIONS = {
  headers: {
    'Content-Type': 'application/json',
  },
};

class ApiClient {
  private authType: string = 'Bearer';
  private responseType: AllowedResponseType = 'json';
  public baseUrl: string;
  private config: any;

  public constructor(
    baseUrl: string = '',
    config: any = DEFAULT_FETCH_OPTIONS,
  ) {
    this.baseUrl = baseUrl;
    this.config = config;

    const {headers, ...rest} = config;

    if (headers && isObject(headers)) {
      this.setHeaders(headers);
    }

    this.config = {...this.config, ...rest};
  }

  public setHeader(name: string, val: any) {
    this.setHeaders({[name]: val});
    return this;
  }

  public setHeaders(headers: RequestConfig['headers']) {
    this.config.headers = {
      ...this.config.headers,
      ...headers,
    };
    return this;
  }

  public setToken(auth: {[p: string]: string} | string) {
    let authToken;

    if (authToken) {
      this.setHeader('Authorization', `${authToken}`);
    }
    return this;
  }

  public setResponseType(responseType: AllowedResponseType) {
    this.responseType = responseType;
  }

  public get(url: string, config?: RequestConfig) {
    this.setMethod('GET');
    return this.request(url, config);
  }

  public post(url: string, config?: RequestConfig) {
    this.setMethod('POST');
    return this.request(url, config);
  }

  public patch(url: string, config?: RequestConfig) {
    this.setMethod('PATCH');
    return this.request(url, config);
  }

  public del(url: string, config?: RequestConfig) {
    this.setMethod('DELETE');
    return this.request(url, config);
  }

  private setMethod(method: AllowedFetchMethod) {
    this.config.method = method; // .toLowerCase();
  }

  private request(
    url: string,
    config: RequestConfig = {},
  ): Promise<Response | Error> {
    const {responseType, headers, json, form, params, ...rest} = config;

    // Ignore method to avoid confusion
    if ('method' in rest) {
      delete (rest as any).method;
    }

    if (responseType) {
      this.setResponseType(responseType);
    }

    if (headers && isObject(headers)) {
      this.setHeaders(headers);
    }

    if (!isAbsoluteURL(url)) {
      url = combineURL(this.baseUrl, url);
    }

    // if ("corsproxy" in this.config) {
    //   url = (this.config as any).corsproxy + url;
    // }

    const {method} = this.config;
    if ('body' in rest && (method === 'GET' || method === 'HEAD')) {
      throw new Error(
        '`GET` or `HEAD` method can not have a request body, use `query` instead',
      );
    }

    if (params && isObject(params)) {
      (rest as any).params = params;
    }

    if (isObject(json)) {
      (rest as any).body = JSON.stringify(json);
    }

    if (form && isObject(form)) {
      (rest as any).body = serialize(form);
      this.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
    return fetch(url, {...this.config, ...rest})
      .then(this.handleErrors)
      .then((response) => response.json())
      .then((responseJson) => {
        return this.resolveResponse(responseJson);
      })
      .catch((err: any) => {
        throw {status: err.message, stack: err.stack};
      });
  }

  private handleErrors(response: any) {
    if (!response.ok) {
      throw Error(response.status);
    }
    return response;
  }

  private resolveResponse(response: any) {
    if (!this.responseType) {
      return response;
    }
    switch (this.responseType) {
      case 'json':
        return response;
      case 'text':
        return response.text();
      case 'blob':
        return response.blob();
      case 'arraybuffer':
        return response.arrayBuffer();
      case 'formdata':
        return response.formData();
      default:
        throw new Error('Invalid response type');
    }
  }
}

export default function http(baseUrl: string = '', config?: RequestConfig) {
  return new ApiClient(baseUrl, config);
}

const request = http();
export const get = (url: string, config?: RequestConfig) =>
  request.get(url, config);
export const post = (url: string, config?: RequestConfig) =>
  request.post(url, config);

// * Enable those under if you need them
// export const put = (url: string, config?: IRequestOptions) => request.put(url, config);
export const patch = (url: string, config?: RequestConfig) =>
  request.patch(url, config);
export const del = (url: string, config?: RequestConfig) =>
  request.del(url, config);
// export const head = (url: string, config?: IRequestOptions) => request.head(url, config);
// export const options = (url: string, config?: IRequestOptions) => request.options(url, config);
