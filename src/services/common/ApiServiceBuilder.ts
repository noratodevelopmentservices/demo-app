export class ApiServiceBuilder {
  private _url: string = '';
  private _token: any;
  private _clientId: string = '';
  private _contentType: string = '';
  private _httpClient: any;
  private _apiKey: string = '';
  private _bearer: boolean = false;

  public withUrl(url: string): ApiServiceBuilder {
    this._url = url;
    return this;
  }

  public withBearer(bearer: boolean): ApiServiceBuilder {
    this._bearer = bearer;
    return this;
  }

  public withToken(token: any): ApiServiceBuilder {
    let _token;

    if (typeof token === 'object') {
      _token = token;
    } else {
      _token = {
        type: 'Authorization',
        token,
      };
    }

    this._token = _token;
    return this;
  }

  public withClientId(clientId: string): ApiServiceBuilder {
    this._clientId = clientId;
    return this;
  }

  public withContentType(contentType: string): ApiServiceBuilder {
    this._contentType = contentType;
    return this;
  }

  public withHttpClient(httpClient: any): ApiServiceBuilder {
    this._httpClient = httpClient;
    return this;
  }

  public withApiKey(apiKey: string): ApiServiceBuilder {
    this._apiKey = apiKey;
    return this;
  }

  public get bearer(): boolean {
    return this._bearer;
  }

  public set bearer(value: boolean) {
    this._bearer = value;
  }

  public get url(): string {
    return this._url;
  }

  public get token(): any {
    return this._token;
  }

  public get clientId(): string {
    return this._clientId;
  }

  public get contentType(): string {
    return this._contentType;
  }

  public get httpClient(): any {
    return this._httpClient;
  }

  public get apiKey(): string {
    return this._apiKey;
  }

  public set apiKey(value: string) {
    this._apiKey = value;
  }

  public build<S>(service: new (builder: ApiServiceBuilder) => S): S {
    if (!this.httpClient) {
      throw new Error('Http Client missing');
    }
    // eslint-disable-next-line new-cap
    return new service(this);
  }
}
