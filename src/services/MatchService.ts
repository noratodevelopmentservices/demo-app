import {isMock} from '@core';
import {MockMatchService} from './MockServices/MockMatchService';
import {MatchServiceContract} from './contracts/MatchService.contract';

export const getMatchService: () => MatchServiceContract = () => {
  if (isMock) {
    return new MockMatchService();
  }
  return new MockMatchService();
};
