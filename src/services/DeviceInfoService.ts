import {DeviceInfoServiceContract} from './contracts/DeviceInfoService.contract';
import {getUniqueId} from 'react-native-device-info';

export class DeviceInfoService implements DeviceInfoServiceContract {
  getDeviceId = () => {
    return getUniqueId();
  };
}
