export interface NavigationServiceContract {
  navigate: (routePath: any, params?: any) => void;
  reset: (routePath: any) => void;
  back: () => void;
}
