import {Opponent, OpponentFace, Response} from '@entities';

export interface OpponentServiceContract {
  getOpponentsFaces: () => Promise<Response<OpponentFace[]>>;
  getOpponentData: () => Promise<Response<Opponent>>;
}
