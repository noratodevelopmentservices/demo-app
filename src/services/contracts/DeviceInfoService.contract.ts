export interface DeviceInfoServiceContract {
  getDeviceId: () => string;
}
