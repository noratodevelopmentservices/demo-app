import {Response, QuestionData, Match} from '@entities';
import {SendChatMessageActionPayload} from '../../adapters/redux/match';

export interface MatchServiceContract {
  getMatchQuestions: (data: {
    matchId: string;
  }) => Promise<Response<QuestionData[]>>;

  createMatch: (userId: string) => Promise<Response<Match>>;
  sendChatMessage: (
    data: SendChatMessageActionPayload,
  ) => Promise<Response<null>>;
}
