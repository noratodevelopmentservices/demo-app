export interface LanguageServiceContract {
  getTranslation: (word: string) => string;
}
