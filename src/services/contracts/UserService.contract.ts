import {User, Response, PrizeSlotIndexes} from '@entities';

export interface UserServiceContract {
  signInAnonymously: () => void;
  getUserData: (data: {uid: string}) => Promise<Response<User>>;
  claimClayPotSlot: (index: PrizeSlotIndexes, uid: string) => void;
  setUserOnlineStatus: (isOnline: boolean, uuid: string) => Promise<void>;
}
