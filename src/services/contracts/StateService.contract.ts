export interface StateServiceContract {
  select(selector: any): any;
  put(action: any): any;
  all(effects: any): any;
  call(effect: any, params?: any): any;
}
