import {Response} from '@entities';

export interface ApiServiceContract {
  syncWithServerTime: () => Promise<Response<{timestamp: number}>>;
}
