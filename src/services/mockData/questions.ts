import {QuestionData} from '@entities';

const questionData: QuestionData = {
  question:
    'Jaka,  il famoso artista reggae trapanese, ha dedicato una delle sue canzoni a una località sita in quale comune?',
  answers: [
    {answer: 'Paceco', correct: false},
    {answer: 'Petrosino', correct: false},
    {answer: 'Erice', correct: true},
    {answer: 'Marsala', correct: false},
  ],
};

export const questions = [questionData];
