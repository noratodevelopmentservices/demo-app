import {Opponent} from '@entities';

export const opponents = [
  {
    id: '1',
    url:
      'https://wi-images.condecdn.net/image/jgov7eBrRvb/crop/2040/f/6-facial-recognition-hero.jpg',
  },
  {
    id: '2',
    url:
      'https://images.unsplash.com/photo-1542080681-b52d382432af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
  },
  {
    id: '3',
    url:
      'https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg',
  },
  {
    id: '4',
    url:
      'https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  },
  {
    id: '5',
    url:
      'https://www.eaclinic.co.uk/wp-content/uploads/2019/01/woman-face-eyes-1000x1000.jpg',
  },
  {
    id: '6',
    url:
      'https://wi-images.condecdn.net/image/jgov7eBrRvb/crop/2040/f/6-facial-recognition-hero.jpg',
  },
  {
    id: '7',
    url:
      'https://images.unsplash.com/photo-1542080681-b52d382432af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
  },
  {
    id: '8',
    url:
      'https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg',
  },
  {
    id: '9',
    url:
      'https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  },
  {
    id: '10',
    url:
      'https://www.eaclinic.co.uk/wp-content/uploads/2019/01/woman-face-eyes-1000x1000.jpg',
  },
  {
    id: '11',
    url:
      'https://www.uni-regensburg.de/Fakultaeten/phil_Fak_II/Psychologie/Psy_II/beautycheck/english/durchschnittsgesichter/m(01-32)_gr.jpg',
  },
  {
    id: '12',
    url:
      'https://wi-images.condecdn.net/image/jgov7eBrRvb/crop/2040/f/6-facial-recognition-hero.jpg',
  },
  {
    id: '13',
    url:
      'https://images.unsplash.com/photo-1542080681-b52d382432af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
  },
  {
    id: '14',
    url:
      'https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg',
  },
  {
    id: '15',
    url:
      'https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  },
  {
    id: '16',
    url:
      'https://www.eaclinic.co.uk/wp-content/uploads/2019/01/woman-face-eyes-1000x1000.jpg',
  },
  {
    id: '17',
    url:
      'https://wi-images.condecdn.net/image/jgov7eBrRvb/crop/2040/f/6-facial-recognition-hero.jpg',
  },
  {
    id: '18',
    url:
      'https://images.unsplash.com/photo-1542080681-b52d382432af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
  },
  {
    id: '19',
    url:
      'https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg',
  },
  {
    id: '20',
    url:
      'https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  },
  {
    id: '21',
    url:
      'https://www.eaclinic.co.uk/wp-content/uploads/2019/01/woman-face-eyes-1000x1000.jpg',
  },
];

export const opponentStats = {
  history: {
    value: 81,
    name: 'Storia',
    icon: 'historyIcon',
  },
  people: {
    value: 68,
    name: 'Personaggi',
    icon: 'peopleIcon',
  },
  geography: {
    value: 48,
    name: 'Geografia',
    icon: 'geographyIcon',
  },
  music: {
    value: 56,
    name: 'Musica',
    icon: 'musicIcon',
  },
  tradition: {
    value: 46,
    name: 'Tradizioni',
    icon: 'traditionIcon',
  },
  proverb: {
    value: 33,
    name: 'Proverbi',
    icon: 'proverbIcon',
  },
};

export const opponent: Opponent = {
  id: '1',
  name: 'Sara',
  points: 454,
  imageUrl:
    'https://scontent.fqls2-1.fna.fbcdn.net/v/t1.0-9/101893523_10220990393622600_4670122548887814144_o.jpg?_nc_cat=107&_nc_sid=8bfeb9&_nc_oc=AQngOrtvEwo8c5iNLuoEPRGCMMBCjROX7-sFHHtmZRAYPliTj9BOAPxLPEH1R2RLpYA&_nc_ht=scontent.fqls2-1.fna&oh=b4eba776f9eb5d066b1e8b5d1b1ef38a&oe=5EF97864',
  stats: opponentStats,
};
