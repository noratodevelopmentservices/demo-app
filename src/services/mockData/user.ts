import {Stats, User} from '@entities';

export const ownStats: Stats = {
  history: {
    value: 31,
    name: 'Storia',
    icon: 'historyIcon',
  },
  people: {
    value: 68,
    name: 'Personaggi',
    icon: 'peopleIcon',
  },
  geography: {
    value: 75,
    name: 'Geografia',
    icon: 'geographyIcon',
  },
  music: {
    value: 45,
    name: 'Musica',
    icon: 'musicIcon',
  },
  tradition: {
    value: 41,
    name: 'Tradizioni',
    icon: 'traditionIcon',
  },
  proverb: {
    value: 34,
    name: 'Proverbi',
    icon: 'proverbIcon',
  },
};

export const user: User = {
  id: 'testId',
  name: 'Donaldo',
  imageUrl:
    'https://images-na.ssl-images-amazon.com/images/I/619NDqbTY6L._AC_SL1000_.jpg',
  points: 234,
  coins: 1820,
  gems: 120,
  stats: ownStats,
  locationStats: {
    one: {won: 5, played: 7},
  },
  freeBox: {
    type: 'one',
    expiration: new Date().getTime() + 100000,
    claimed: false,
  },
};
