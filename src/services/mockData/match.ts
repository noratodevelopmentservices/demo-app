import {Match} from '@entities';
import {opponent} from './opponent';

export const match: Match = {
  matchId: 'matchId',
  opponent,
};
