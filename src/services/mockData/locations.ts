import { CoatOfArmsShape, Location } from '@entities';

export const locations: Location[] = [
  {
    name: 'Trapani',
    shape: CoatOfArmsShape.GREEN_PENTAGON,
    coatOfArms: 'trapaniStemma',
    win: 5,
    lose: -3,
    prize: 200,
    unblocks: 0,
    availablePoints: 40,
    backgroundGradientColors: ['#358DDE', '#051437'],
  },
  {
    name: 'Enna',
    shape: CoatOfArmsShape.SQUARE_DIAMOND,
    coatOfArms: 'ennaStemma',
    win: 8,
    lose: -6,
    prize: 700,
    unblocks: 40,
    availablePoints: 40,
    backgroundGradientColors: ['#E45B14', '#0D0603'],
  },
  {
    name: 'Ragusa',
    shape: CoatOfArmsShape.GREEN_HEXAGON,
    coatOfArms: 'ragusaStemma',
    win: 12,
    lose: -9,
    prize: 3000,
    unblocks: 80,
    availablePoints: 50,
    backgroundGradientColors: ['#1E825B', '#0A3425'],
  },
  {
    name: 'Caltanissetta',
    shape: CoatOfArmsShape.CIRCLE,
    coatOfArms: 'caltanissettaStemma',
    win: 16,
    lose: -13,
    prize: 12000,
    unblocks: 130,
    availablePoints: 120,
    backgroundGradientColors: ['#3A9FF5', '#1A2B4D'],
  },
  {
    name: 'Siracusa',
    shape: CoatOfArmsShape.GREEN_HEXAGON,
    coatOfArms: 'siracusaStemma',
    win: 20,
    lose: -17,
    prize: 40000,
    unblocks: 250,
    availablePoints: 150,
    backgroundGradientColors: ['#E45B14', '#0D0603'],
  },
  {
    name: 'Agrigento',
    shape: CoatOfArmsShape.SQUARE_DIAMOND,
    coatOfArms: 'agrigentoStemma',
    win: 24,
    lose: -21,
    prize: 120000,
    unblocks: 400,
    availablePoints: 280,
    backgroundGradientColors: ['#1E825B', '#0A3425'],
  },
  {
    name: 'Messina',
    shape: CoatOfArmsShape.GREEN_PENTAGON,
    coatOfArms: 'messinaStemma',
    win: 29,
    lose: -26,
    prize: 400000,
    unblocks: 680,
    availablePoints: 570,
    backgroundGradientColors: ['#358DDE', '#051437'],
  },
  {
    name: 'Catania',
    shape: CoatOfArmsShape.CIRCLE,
    coatOfArms: 'cataniaStemma',
    win: 34,
    lose: -31,
    prize: 800000,
    unblocks: 1250,
    availablePoints: 850,
    backgroundGradientColors: ['#1E825B', '#0A3425'],
  },
  {
    name: 'Palermo',
    shape: CoatOfArmsShape.SQUARE,
    coatOfArms: 'palermoStemma',
    win: 40,
    lose: -36,
    prize: 1200000,
    unblocks: 2100,
    availablePoints: undefined,
    backgroundGradientColors: ['#3A9FF5', '#1A2B4D'],
  },
];
