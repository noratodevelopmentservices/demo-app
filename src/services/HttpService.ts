import {ApiServiceBuilder} from './common/ApiServiceBuilder';

export class HttpService {
  protected request: any;
  protected url: string = '';

  public constructor(builder: ApiServiceBuilder) {
    this.request = builder.httpClient;
    if (builder.contentType) {
      this.request.setHeader('Content-Type', builder.contentType);
    }
    this.request.setHeader('Accept', 'application/json');
    this.request.setHeader('Accept-Charset', 'utf-8');
    if (builder.token) {
      this.request.setHeader(
        'Authorization',
        `${
          builder.bearer ? `Bearer ${builder.token.token}` : builder.token.token
        }`,
      );
    }
    if (builder.apiKey) {
      this.request.setHeader('x-api-key', builder.apiKey);
    }
    this.url = builder.url;
  }
}
