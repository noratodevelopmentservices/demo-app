import {isMock} from '@core';
import {MockOpponentService} from './MockServices/MockOpponentService';
import {OpponentServiceContract} from './contracts/OpponentService.contract';

export const getOpponentService: () => OpponentServiceContract = () => {
  if (isMock) {
    return new MockOpponentService();
  }
  return new MockOpponentService();
};
