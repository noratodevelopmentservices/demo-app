import React from 'react';
import {NavigationServiceContract} from './contracts/NavigationService.contract';

export const navigationRef = React.createRef<any>();

class NavigationService implements NavigationServiceContract {
  public navigate = (routePath: any, params?: any): void => {
    if (navigationRef.current) {
      navigationRef.current.navigate({routeName: routePath.routeName, params});
    }
  };
  public reset = (route: {routeName: string}) => {
    if (navigationRef.current) {
      navigationRef.current.reset({
        index: 0,
        routes: {
          name: route.routeName,
        },
      });
    }
  };

  public back = () => {
    if (navigationRef.current) {
      navigationRef.current.goBack();
    }
  };
}

export default new NavigationService();
