import * as RNLocalize from 'react-native-localize';
import Languages, {AvailableLanguages} from '../translations';
import get from 'lodash.get';
import {LanguageServiceContract} from './contracts/LanguageService.contract';

class LanguageService implements LanguageServiceContract {
  public getTranslation: (word: string) => string = (word) => {
    const languageCode = RNLocalize.getLocales()[0]
      .languageCode as AvailableLanguages;
    const languageNode: {[key: string]: any} =
      Languages.translations[languageCode];
    if (languageNode) {
      return get(languageNode, word);
    }
    return Languages.translations.it?.[word];
  };
}

export default new LanguageService();
