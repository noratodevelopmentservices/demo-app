import {isMock} from '@core';
import {MockUserService} from './MockServices/MockUserService';
import {UserServiceContract} from './contracts/UserService.contract';
import {RealUserService} from './RealServices/RealUserService';

export const getUserService: () => UserServiceContract = () => {
  if (isMock) {
    return new MockUserService();
  }
  return new RealUserService();
};
