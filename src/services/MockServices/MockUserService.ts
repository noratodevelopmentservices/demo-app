import {UserServiceContract} from '../contracts/UserService.contract';
import {Response, User, PrizeSlotIndexes} from '@entities';

const API_DELAY = 2000;

const user: User = {
  id: 'testId',
  name: 'Mario',
  imageUrl: 'http://google.com',
  points: 0,
  coins: 0,
  gems: 0,
  stats: {
    history: 0,
    people: 0,
    geography: 0,
    music: 0,
    traditions: 0,
    proverbs: 0,
  },
  freeBox: {
    claimed: false,
    expiration: 0,
    type: 'one',
  },
  locationStats: {
    0: {played: 0, won: 0},
  },
};

export class MockUserService implements UserServiceContract {
  signInAnonymously = () => console.log('logged!');
  getUserData: (data: {uid: string}) => Promise<Response<User>> = ({uid}) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: {...user, id: uid}});
      }, API_DELAY);
    });
  };
  claimClayPotSlot: (index: PrizeSlotIndexes, uid: string) => void = (index) =>
    console.log(index);
  setUserOnlineStatus
}
