import {QuestionData, Response, Match} from '@entities';
import {questions} from '../mockData/questions';
import {match} from '../mockData/match';
import {MatchServiceContract} from '../contracts/MatchService.contract';
import {SendChatMessageActionPayload} from '../../adapters/redux/match';
const API_DELAY = 2000;

export class MockMatchService implements MatchServiceContract {
  getMatchQuestions: (data: {
    matchId: string;
  }) => Promise<Response<QuestionData[]>> = ({matchId}) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: questions});
      }, API_DELAY);
    });
  };
  createMatch: (userId: string) => Promise<Response<Match>> = (userId) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: match});
      });
    });
  };

  sendChatMessage: (
    data: SendChatMessageActionPayload,
  ) => Promise<Response<null>> = (data) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: null});
      });
    });
  };
}
