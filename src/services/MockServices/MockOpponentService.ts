import {opponents, opponent} from '../mockData';
import {OpponentServiceContract} from '../contracts/OpponentService.contract';
import {OpponentFace, Response, Opponent} from '@entities';

const API_DELAY = 2000;

export class MockOpponentService implements OpponentServiceContract {
  getOpponentsFaces = () => {
    return new Promise<Response<OpponentFace[]>>((resolve) => {
      setTimeout(() => {
        resolve({success: true, data: opponents, status: 201});
      }, API_DELAY);
    });
  };

  getOpponentData = () => {
    return new Promise<Response<Opponent>>((resolve) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: opponent});
      }, API_DELAY);
    });
  };
}
