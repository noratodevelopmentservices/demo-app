import {getOpponentService} from './OpponentService';
import {getUserService} from './UserService';

export const OpponentService = getOpponentService();
export const UserService = getUserService();
export * from './StateService';
export * from './NavigationService';
export {default as LanguageService} from './LanguageService';
