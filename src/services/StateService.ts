import {StateAdapter} from '../adapters/redux/common/StateAdapter';
import ReduxSagaStateManager from '../adapters/redux/common/sagas/ReduxSagaStateManager';
import {StateServiceContract} from './contracts/StateService.contract';

export class StateService implements StateServiceContract {
  public reduxSaga = ReduxSagaStateManager;
  public stateAdapter: StateAdapter = new StateAdapter(this.reduxSaga);

  public select(selector: any) {
    return this.stateAdapter.select(selector);
  }

  public put(action: any) {
    return this.stateAdapter.put(action);
  }

  public all(effects: any) {
    return this.stateAdapter.all(effects);
  }

  public call(effect: any, params?: any) {
    return this.stateAdapter.call(effect, params);
  }
}
