import {HttpService} from '../HttpService';
import {ApiServiceContract} from '../contracts/ApiService.contract';
import {ApiServiceBuilder} from '../common/ApiServiceBuilder';
import http from '../common/HttpClient';

class ApiService extends HttpService implements ApiServiceContract {
  syncWithServerTime = async () => {
    let response: {timestamp: number};
    try {
      response = await this.request.get('getTimestamp');
      return {success: true, data: response, status: 201};
    } catch (e) {
      return {success: false, error: e, status: 500};
    }
  };
}

export const getApiService: () => ApiService = () => {
  let apiServiceBuilder = new ApiServiceBuilder();
  return apiServiceBuilder
    .withUrl('')
    .withHttpClient(
      http('https://us-central1-localquiz-160d7.cloudfunctions.net/'),
    )
    .build(ApiService);
};
