import {UserServiceContract} from '../contracts/UserService.contract';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {Response, User} from '@entities';
import {PrizeSlotIndexes} from '../../entities/PrizeSlot';

export class RealUserService implements UserServiceContract {
  signInAnonymously = () => {
    auth()
      .signInAnonymously()
      .then(() => console.log('success'))
      .catch((error: any) => console.log('anonymous login failure', error));
  };

  getUserData: (data: {uid: string}) => Promise<Response<User>> = async ({
    uid,
  }) => {
    try {
      let user: User | undefined;
      const userObjectSnapshot = await firestore().doc(`users/${uid}`).get();
      if (userObjectSnapshot.exists) {
        user = userObjectSnapshot.data() as User;
      }
      return {
        success: true,
        status: 201,
        data: user,
      };
    } catch (error) {
      return {
        success: false,
        status: 500,
        error,
      };
    }
  };

  claimClayPotSlot = async (index: PrizeSlotIndexes, uid: string) => {
    const blockedPath = `prizeSlots.${index}.blocked`;
    const expirePath = `prizeSlots.${index}.expiry`;
    const blockedObject = {
      [blockedPath]: false,
    };
    const expiryObject = {
      [expirePath]: new Date().getTime() + 7200000,
    };
    await firestore().doc(`users/${uid}`).update(blockedObject);
    await firestore().doc(`users/${uid}`).update(expiryObject);
  };

  setUserOnlineStatus = async (isOnline: boolean, uid: string) => {
    console.log(isOnline, uid);
    await firestore()
      .doc(`users/${uid}`)
      .update({['isOnline']: isOnline});
  };
}
