const en = require('./en.json');
const it = require('./it.json');

export enum AvailableLanguages {
  IT = 'it',
  EN = 'en',
}

interface Languages {
  translations: {[key in AvailableLanguages]: any};
}

const languages: Languages = {
  translations: {
    en,
    it,
  },
};

export default languages;
