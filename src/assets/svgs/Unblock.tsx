import React from 'react';
import {View, Text} from 'react-native';
import {Svg, G, Path, Defs, LinearGradient, Stop} from 'react-native-svg';

interface UnblockProps {
  label: string;
}

export const Unblock: React.FC<UnblockProps> = ({label}) => {
  return (
    <View style={{alignItems: 'center', width: '100%'}}>
      <Svg width="77px" height="40px" viewBox="0 0 77 40">
        <Defs>
          <LinearGradient
            x1="6.65838068%"
            y1="36.5069995%"
            x2="100%"
            y2="52.3032974%"
            id="linearGradient-1">
            <Stop stopColor="#3A89D8" offset="0" stopOpacity={1} />
            <Stop stopColor="#35ABCF" offset="1" stopOpacity={1} />
          </LinearGradient>
        </Defs>
        <G
          id="Page-1"
          stroke="none"
          stroke-width="1"
          fill="none"
          fill-rule="evenodd">
          <G
            id="Home"
            transform="translate(-221.000000, -654.000000)"
            fill="url(#linearGradient-1)">
            <G id="ClayPotsLots" transform="translate(8.000000, 654.000000)">
              <G
                id="ClayPotLot-Copy-2"
                transform="translate(204.000000, 0.000000)">
                <Path
                  d="M14,0 L81,0 C83.7614237,-5.07265313e-16 86,2.23857625 86,5 L86,27.0340316 C86,29.3569955 84.4001279,31.3739961 82.1381468,31.9027709 L47.5,40 L47.5,40 L12.8618532,31.9027709 C10.5998721,31.3739961 9,29.3569955 9,27.0340316 L9,5 C9,2.23857625 11.2385763,-1.26909153e-15 14,0 Z"
                  id="Rectangle"
                />
              </G>
            </G>
          </G>
        </G>
      </Svg>
      <Text
        style={{
          color: 'white',
          fontSize: 12,
          position: 'absolute',
          left: 10,
          alignSelf: 'center',
        }}>
        {label}
      </Text>
    </View>
  );
};
