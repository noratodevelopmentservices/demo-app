import React from 'react';
import {Svg, G, Circle, Line} from 'react-native-svg';

export interface ClockProps {
  color?: string;
  width?: number;
  height?: number;
}
export const Clock: React.FC<ClockProps> = ({
  color = '#FFFFFF',
  width = 18,
  height = 18,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 18 18">
      <G
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd">
        <G
          id="Home"
          transform="translate(-17.000000, -97.000000)"
          stroke={color}>
          <G id="Free-Item" transform="translate(12.000000, 66.000000)">
            <G id="clock" transform="translate(6.000000, 32.000000)">
              <Circle id="Oval" cx="8" cy="8" r="8" />
              <Line
                x1="8.5"
                y1="12.3333333"
                x2="8.5"
                y2="15.6666667"
                id="Line-2"
                strokeLinecap="square"
              />
            </G>
          </G>
        </G>
      </G>
    </Svg>
  );
};
