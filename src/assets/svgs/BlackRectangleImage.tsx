import React from 'react';
import {View} from 'react-native';
import {Svg, G, Path} from 'react-native-svg';

export interface BlackRectangleImageProps {
  width: number;
  height: number;
  left?: number;
  top?: number;
}
export const BlackRectangleImage: React.FC<BlackRectangleImageProps> = ({
  width,
  height,
  left = 0,
  top = 0,
}) => {
  return (
    <View style={{position: 'absolute', left, top}}>
      <Svg
        width={`${width}px`}
        height={`${height}px`}
        viewBox="0 0 43 34"
        preserveAspectRatio="none">
        <G
          id="Page-1"
          stroke="none"
          stroke-width="1"
          fill="none"
          fill-rule="evenodd">
          <G
            id="Home"
            transform="translate(-153.000000, -21.000000)"
            fill="#000000">
            <G id="Coins" transform="translate(153.000000, 16.000000)">
              <Path
                d="M10.445756,5 L34.3451926,5 C38.7634706,5 42.3451926,8.581722 42.3451926,13 C42.3451926,13.2096066 42.3369548,13.4191322 42.3204983,13.6280918 L40.9029158,31.6280918 C40.5751547,35.7898967 37.1023014,39 32.9276102,39 L8.77631025,39 C4.35803225,39 0.776310249,35.418278 0.776310249,31 C0.776310249,30.7533363 0.787718308,30.5068045 0.810497861,30.2611949 L2.47994366,12.2611949 C2.86153976,8.14681768 6.31372079,5 10.445756,5 Z"
                id="Rectangle-Copy-2"
              />
            </G>
          </G>
        </G>
      </Svg>
    </View>
  );
};
