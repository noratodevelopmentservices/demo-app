import React from 'react';
import {View} from 'react-native';
import {Svg, G, Path} from 'react-native-svg';

export interface GreyRectangleImageProps {
  width: number;
  height: number;
  left?: number;
  top?: number;
}

export const GreyRectangleImage: React.FC<GreyRectangleImageProps> = ({
  width,
  height,
  left = 0,
  top = 0,
}) => {
  return (
    <View style={{position: 'absolute', left, top}}>
      <Svg width={`${width}px`} height={`${height}px`} viewBox="0 0 104 34">
        <G
          id="Page-1"
          stroke="none"
          stroke-width="1"
          fill="none"
          fill-rule="evenodd">
          <G
            id="Home"
            transform="translate(-177.000000, -21.000000)"
            fill="#929292">
            <G id="Coins" transform="translate(153.000000, 16.000000)">
              <Path
                d="M36.2108049,5 L119.280064,5 C123.698342,5 127.280064,8.581722 127.280064,13 C127.280064,13.5164516 127.230053,14.0316898 127.130733,14.5385012 L123.603261,32.5385012 C122.867643,36.2922138 119.577704,39 115.752591,39 L32.0566026,39 C27.6383246,39 24.0566026,35.418278 24.0566026,31 C24.0566026,30.3945039 24.1253441,29.790965 24.2615068,29.2009775 L28.4157091,11.2009775 C29.2534221,7.57119929 32.4856132,5 36.2108049,5 Z"
                id="Rectangle-Copy"
              />
            </G>
          </G>
        </G>
      </Svg>
    </View>
  );
};
