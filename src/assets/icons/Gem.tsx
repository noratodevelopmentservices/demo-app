import React from 'react';
import {View} from 'react-native';
import {Svg, G, Path, Polygon} from 'react-native-svg';

interface GemProps {
  width: number;
  height: number;
  left?: number;
  top?: number;
}

export const Gem: React.FC<GemProps> = ({top = 0, left = 0, width, height}) => {
  return (
    <Svg width={`${width}px`} height={`${height}px`} viewBox="0 0 512 448">
      <G
        id="Page-1"
        stroke="none"
        stroke-width="1"
        fill="none"
        fill-rule="evenodd">
        <G id="gem-(1)" fill-rule="nonzero">
          <Path
            d="M376.182,0.203 L135.818,0.203 C130.232,0.203 125.07,3.183 122.277,8.021 L2.095,216.182 C-0.698,221.02 -0.698,226.98 2.095,231.818 L122.277,439.98 C125.07,444.818 130.232,447.798 135.818,447.798 L376.182,447.798 C381.768,447.798 386.93,444.818 389.723,439.98 L509.905,231.818 C512.698,226.98 512.698,221.02 509.905,216.182 L389.723,8.021 C386.93,3.183 381.768,0.203 376.182,0.203 Z"
            id="Path"
            fill="#FFD45D"
          />
          <Path
            d="M512,224 L409.521,224 L332.76,356.953 L384,445.702 C386.338,444.352 388.327,442.398 389.723,439.979 L509.905,231.817 C511.302,229.399 512,226.699 512,224 Z"
            id="Path"
            fill="#FF6500"
          />
          <Path
            d="M332.76,356.953 L179.24,356.953 L128,445.702 C130.338,447.052 133.025,447.797 135.818,447.797 L376.182,447.797 C378.975,447.797 381.662,447.052 384,445.702 L332.76,356.953 Z"
            id="Path"
            fill="#FF9416"
          />
          <Path
            d="M179.24,356.953 L102.479,224 L0,224 C0,226.699 0.698,229.399 2.095,231.818 L122.277,439.98 C123.674,442.399 125.662,444.353 128,445.703 L179.24,356.953 Z"
            id="Path"
            fill="#FFC13F"
          />
          <Path
            d="M179.24,91.047 L128,2.297 C125.662,3.647 123.673,5.601 122.277,8.02 L2.095,216.183 C0.698,218.601 0,221.301 0,224 L102.479,224 L179.24,91.047 Z"
            id="Path"
            fill="#FFD45D"
          />
          <Path
            d="M179.24,91.047 L332.76,91.047 L384,2.297 C381.662,0.947 378.975,0.202 376.182,0.202 L135.818,0.202 C133.025,0.202 130.338,0.947 128,2.297 L179.24,91.047 Z"
            id="Path"
            fill="#FFC13F"
          />
          <Path
            d="M332.76,91.047 L409.521,224 L512,224 C512,221.301 511.302,218.601 509.905,216.183 L389.723,8.02 C388.327,5.601 386.338,3.647 384,2.297 L332.76,91.047 Z"
            id="Path"
            fill="#FF9416"
          />
          <Polygon
            id="Path"
            fill="#FF7B05"
            points="332.761 91.047 179.239 91.047 102.479 224 179.239 356.953 332.761 356.953 409.521 224"
          />
          <Path
            d="M182.006,81.738 L191.792,74.429 C194.019,72.766 196.965,75.25 195.702,77.726 L190.152,88.606 C189.559,89.769 189.91,91.19 190.976,91.943 L200.951,98.991 C203.221,100.595 201.769,104.165 199.024,103.728 L186.961,101.811 C185.672,101.606 184.428,102.379 184.041,103.626 L180.42,115.291 C179.596,117.945 175.752,117.667 175.319,114.922 L173.414,102.858 C173.21,101.569 172.091,100.625 170.786,100.642 L158.573,100.803 C155.794,100.84 154.87,97.098 157.347,95.838 L168.232,90.298 C169.395,89.706 169.947,88.35 169.527,87.114 L165.6,75.549 C164.706,72.917 167.979,70.883 169.944,72.849 L178.576,81.49 C179.5,82.413 180.96,82.519 182.006,81.738 Z"
            id="Path"
            fill="#FFFFFF"
          />
        </G>
      </G>
    </Svg>
  );
};
