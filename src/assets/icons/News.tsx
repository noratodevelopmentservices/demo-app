import React from 'react';
import {Svg, G, Path} from 'react-native-svg';

interface NewsProps {
  color?: string;
}

export const News: React.FC<NewsProps> = ({color = '#BBBBBB'}) => {
  return (
    <Svg width="30px" height="19px" viewBox="0 0 30 19">
      <G
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd">
        <G
          id="Home"
          transform="translate(-19.000000, -845.000000)"
          fill={color}
          fillRule="nonzero">
          <G id="TabBar" transform="translate(0.000000, 822.000000)">
            <G id="Messages">
              <G id="envelope" transform="translate(19.000000, 23.000000)">
                <Path
                  d="M2.72623373e-05,0.67731886 L2.72623373e-05,18.3226845 C-0.00257581258,18.6741164 0.319910279,18.9963357 0.677581834,19 L29.322464,19 C29.6801379,18.9962679 30.0024529,18.6741164 29.9999832,18.3226845 L29.9999832,0.67731886 C30.0026779,0.325890423 29.6801379,0.00365071103 29.322464,0 L0.677581834,0 C0.255780696,0.00675177969 -0.00305717479,0.348391832 2.72623373e-05,0.67731886 Z M2.49611448,1.34295952 L27.492033,1.34295952 L15.9271129,11.3042265 C15.3589334,11.7973439 14.6454622,11.8031118 14.0610359,11.3042265 L2.49611448,1.34295952 Z M28.6330797,2.14873862 L28.6330797,16.6760985 L20.1941319,9.41240499 L28.6330797,2.14873862 Z M1.36695429,2.16041004 L9.78213088,9.41240499 L1.36695429,16.6643932 L1.36695429,2.16041004 Z M10.8280877,10.3116131 L13.1577127,12.3202167 C14.3498669,13.3100479 15.5854102,13.2754408 16.8304362,12.308681 L19.1600609,10.3117488 L27.6940995,17.6571762 L2.29404973,17.6571762 L10.8280877,10.3116131 Z"
                  id="Shape"
                />
              </G>
            </G>
          </G>
        </G>
      </G>
    </Svg>
  );
};
