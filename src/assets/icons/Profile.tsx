import React from 'react';
import {Svg, G, Path} from 'react-native-svg';

interface ProfileProps {
  color?: string;
}
export const Profile: React.FC<ProfileProps> = ({color = '#BBBBBB'}) => {
  return (
    <Svg width="24px" height="22px" viewBox="0 0 24 22">
      <G
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd">
        <G
          id="Home"
          transform="translate(-307.000000, -842.000000)"
          fill={color}
          fillRule="nonzero">
          <G id="TabBar" transform="translate(0.000000, 822.000000)">
            <G id="Player" transform="translate(281.000000, 0.000000)">
              <G id="dress" transform="translate(26.000000, 20.000000)">
                <Path
                  d="M16.14025,0 C15.80525,1.743 13.88075,2.775 12,2.775 C10.188,2.775 8.26,1.728 7.873,0 L16.14025,0 Z M22.407,9 L19.25,9 C18.94375,9 18.981,9.055 18.981,9.359 L18.981,22 L5.981,22 L5.981,9.359 C5.981,9.055 5.238,9 4.935,9 L1.594,9 L0,3.601 L6.778,0.257 C7.264,2.543 9.714,3.9 12,3.9 C14.337,3.9 16.746,2.567 17.226,0.31 L24,3.613 L22.407,9 L22.407,9 Z"
                  id="Shape"
                />
              </G>
            </G>
          </G>
        </G>
      </G>
    </Svg>
  );
};
