import {MatchServiceContract} from '../services/contracts/MatchService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';
import {saveMatchDataAction} from '../adapters/redux/match';

interface CreateMatchInteractorServices {
  matchService: MatchServiceContract;
  stateService: StateServiceContract;
}

interface CreateMatchInteractorInput {
  userId?: string;
}

export class CreateMatchInteractor {
  constructor(private services: CreateMatchInteractorServices) {}

  public async execute(input: CreateMatchInteractorInput) {
    const {userId} = input;
    const {matchService, stateService} = this.services;
    try {
      if (userId) {
        const response = await matchService.createMatch(userId);
        if (response.success) {
          await stateService.put(
            saveMatchDataAction({matchData: response.data!}),
          );
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
}
