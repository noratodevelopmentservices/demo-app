import {UserServiceContract} from '../services/contracts/UserService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';
import {saveUserDataAction} from '../adapters/redux/user';

interface GetUserDataInteractorServices {
  userService: UserServiceContract;
  stateService: StateServiceContract;
}

interface GetUserDataInteractorInput {
  uid: string;
}
export class GetUserDataInteractor {
  constructor(private services: GetUserDataInteractorServices) {}

  public async execute(input: GetUserDataInteractorInput) {
    const {userService, stateService} = this.services;
    const {uid} = input;
    try {
      const userResponse = await userService.getUserData({uid});
      if (userResponse.success) {
        stateService.put(
          saveUserDataAction({userData: {...userResponse.data!, id: uid}}),
        );
      }
    } catch (e) {
      console.log(e);
    }
  }
}
