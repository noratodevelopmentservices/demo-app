import {UserServiceContract} from '../services/contracts/UserService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';
import {PrizeSlotIndexes} from '../entities/PrizeSlot';

export interface UnblockClayPutSlotInteractorServices {
  userService: UserServiceContract;
  stateService: StateServiceContract;
}

export interface UnblockClayPutSlotInteractorInput {
  index: PrizeSlotIndexes;
  uid: string;
}

export class UnblockClayPutSlotInteractor {
  constructor(private services: UnblockClayPutSlotInteractorServices) {}

  public execute = async (input: UnblockClayPutSlotInteractorInput) => {
    const {index, uid} = input;
    const {userService, stateService} = this.services;
    try {
      await userService.claimClayPotSlot(index, uid);
    } catch (e) {
      console.log(e);
    }
  };
}
