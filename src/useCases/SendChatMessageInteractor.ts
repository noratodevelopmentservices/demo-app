import {MatchServiceContract} from '../services/contracts/MatchService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';

interface SendChatMessageInteractorServices {
  matchService: MatchServiceContract;
  stateService: StateServiceContract;
}

interface SendChatMessageInteractorInput {
  matchId: string;
  chatMessage: {key: string};
}

export class SendChatMessageInteractor {
  constructor(private services: SendChatMessageInteractorServices) {}

  public async execute(input: SendChatMessageInteractorInput) {
    const {matchService, stateService} = this.services;
    const {matchId, chatMessage} = input;
    try {
      const response = await matchService.sendChatMessage({
        matchId,
        chatMessage,
      });
      console.log('response', response);
      if (!response.success) {
        console.log('chat message error!');
      }
    } catch (e) {
      console.log('chat message error!');
    }
  }
}
