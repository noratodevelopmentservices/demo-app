import {MatchServiceContract} from '../services/contracts/MatchService.contract';
import {StateService} from '@services';
import {saveMatchQuestionAction} from '../adapters/redux/match/actions';

interface GetMatchQuestionsInteractorServices {
  matchService: MatchServiceContract;
  stateService: StateService;
}

interface GetMatchQuestionsInteractorInput {
  matchId: string;
}

export class GetMatchQuestionsInteractor {
  constructor(private services: GetMatchQuestionsInteractorServices) {}
  public execute = async (input: GetMatchQuestionsInteractorInput) => {
    const {matchService, stateService} = this.services;
    const {matchId} = input;
    try {
      const {data} = await matchService.getMatchQuestions({matchId});
      await stateService.put(saveMatchQuestionAction({questions: data!}));
    } catch (e) {
      console.log(e);
    }
  };
}
