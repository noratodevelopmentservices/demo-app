import {UserServiceContract} from '../services/contracts/UserService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';

interface RegisterInteractorServices {
  userService: UserServiceContract;
  stateService: StateServiceContract;
}

export class RegisterInteractor {
  constructor(private services: RegisterInteractorServices) {}

  async execute(input: any) {
    const {userService, stateService} = this.services;
  }
}
