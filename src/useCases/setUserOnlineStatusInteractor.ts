import {UserServiceContract} from '../services/contracts/UserService.contract';

interface SetUserOnlineStatusInteractorServices {
  userService: UserServiceContract;
}

interface SetUserOnlineStatusInteractorInput {
  isOnline: boolean;
  uuid: string;
}

export class SetUserOnlineStatusInteractor {
  constructor(private services: SetUserOnlineStatusInteractorServices) {}
  public async execute(input: SetUserOnlineStatusInteractorInput) {
    const {userService} = this.services;
    const {isOnline, uuid} = input;
    console.log('interactor', isOnline, uuid);
    try {
      await userService.setUserOnlineStatus(isOnline, uuid);
    } catch (e) {
      console.log(e);
    }
  }
}
