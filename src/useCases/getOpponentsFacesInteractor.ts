import {StateServiceContract} from '../services/contracts/StateService.contract';
import {OpponentServiceContract} from '../services/contracts/OpponentService.contract';
import {saveOpponentsFacesAction} from '../adapters/redux/opponent';

interface GetOpponentsFacesInteractorServices {
  opponentService: OpponentServiceContract;
  stateService: StateServiceContract;
}
export class GetOpponentsFacesInteractor {
  constructor(private services: GetOpponentsFacesInteractorServices) {}

  public async execute(input: any) {
    const {stateService, opponentService} = this.services;
    try {
      const response = await opponentService.getOpponentsFaces();
      // console.log('response', response);
      if (response.success) {
        const {data} = response;
        await stateService.put(saveOpponentsFacesAction(data));
      }
    } catch (e) {
      console.log(e);
    }
  }
}
