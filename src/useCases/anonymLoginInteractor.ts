import {StateServiceContract} from '../services/contracts/StateService.contract';
import {UserServiceContract} from '../services/contracts/UserService.contract';
import {DeviceInfoServiceContract} from '../services/contracts/DeviceInfoService.contract';

interface AnonymLoginInteractorServices {
  stateService: StateServiceContract;
  userService: UserServiceContract;
  deviceInfoService: DeviceInfoServiceContract;
}

export class AnonymLoginInteractor {
  private readonly services: AnonymLoginInteractorServices;
  public constructor(services: AnonymLoginInteractorServices) {
    this.services = services;
  }

  public async execute(input: any) {
    const {userService} = this.services;
    try {
      userService.signInAnonymously();
    } catch (e) {
      console.log(e);
    }
  }
}
