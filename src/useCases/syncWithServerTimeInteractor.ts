import {ApiServiceContract} from '../services/contracts/ApiService.contract';
import {StateServiceContract} from '../services/contracts/StateService.contract';
import {saveSyncWithServerTimeAction} from '../adapters/redux/user';

export interface SyncWithServerTimeInteractorServices {
  apiService: ApiServiceContract;
  stateService: StateServiceContract;
}

export class SyncWithServerTimeInteractor {
  constructor(private services: SyncWithServerTimeInteractorServices) {}

  public async execute(input: any) {
    const {stateService, apiService} = this.services;
    try {
      const response = await apiService.syncWithServerTime();
      const {timestamp} = response.data!;
      const difference = new Date().getTime() - timestamp;
      await stateService.put(saveSyncWithServerTimeAction({difference}));
    } catch (e) {
      console.log(e);
    }
  }
}
