// Aspect Ratio
export const ASPECT_RATIO = {
  VIDEO: {
    WIDTH: 16,
    HEIGHT: 9,
  },
  IMAGE: {
    WIDTH: 3,
    HEIGHT: 4,
  },
};

// Grid defaults
export const gridSize = 8;

// Font defaults
export const fontFamily = 'Roboto';
export const fontFamilyCondensed = 'RobotoCondensed-Light';

export const fontSize = 18;

export const videoAspectRatio =
  ASPECT_RATIO.VIDEO.WIDTH / ASPECT_RATIO.VIDEO.HEIGHT;
export const imageAspectRatio =
  ASPECT_RATIO.IMAGE.WIDTH / ASPECT_RATIO.IMAGE.HEIGHT;
