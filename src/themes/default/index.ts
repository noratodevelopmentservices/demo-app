import {DefaultTheme} from 'styled-components/native';
import {colors, fontSize, gridSize} from '@themes';
import {Size, Sizes} from '../types';

// Global vars so we can style react-navigation components easily
export const darkBackgroundColor = colors.progressBarBackground;

const sizeFactor = (desiredPixelSize: number): number => {
  return desiredPixelSize / fontSize;
};

export const sizes: Sizes = {
  xSmall: sizeFactor(gridSize / 2),
  small: sizeFactor(gridSize),
  medium: sizeFactor(gridSize * 2),
  large: sizeFactor(gridSize * 3),
  xLarge: sizeFactor(gridSize * 5),
  xxLarge: sizeFactor(gridSize * 7),
  xxxLarge: sizeFactor(gridSize * 12),
};

export const relativeSize = (size: Size | number) => {
  if (typeof size === 'string') {
    return fontSize * sizes[size];
  } else {
    return fontSize * size;
  }
};

export const theme: DefaultTheme = {
  darkBackgroundColor,
  lightBackgroundColor: '#F4F4F4', // plug this into color index
  defaultGutterSize: relativeSize('medium'),
  accentColor: colors.recommendationCardShadow,
  fontColor: colors.b1,
};

export * from './typography';
