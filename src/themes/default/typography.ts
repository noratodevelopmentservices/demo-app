import {TypeStyle, fontSize} from '@themes';
import {isIOS} from '@core';
import {css} from 'styled-components/native';

export const getIosFontName: (
  baseName: string,
  fontWeight?: string,
  isItalic?: boolean,
  isCondensed?: boolean,
) => string | undefined = (
  baseName,
  fontWeight,
  isItalic = false,
  isCondensed = false,
) => {
  if (baseName === 'Futura') {
    if (fontWeight === 'bold' && isCondensed) {
      return 'Futura-CondensedExtraBold';
    }
    if (fontWeight === 'bold' && !isCondensed && isItalic) {
      return 'FuturaBT-HeavyItalic';
    }
    if (fontWeight === 'bold' && !isCondensed && !isItalic) {
      return 'Futura-Bold';
    }
    if (fontWeight === 'light' && isCondensed) {
      return 'Futura-CondensedLight';
    }
    if (fontWeight === 'light' && !isCondensed && isItalic) {
      return 'FuturaBT-LightItalic';
    }
    if (fontWeight === 'light' && !isCondensed && !isItalic) {
      return 'FuturaBT-LightItalic';
    }
    if (isCondensed) {
      return 'Futura-CondensedMedium';
    }
    return 'Futura';
  }
};

/**
 * Android does not recognise font families,
 * thus we have to specify each font style name specifically. iOS on the other hand
 * recognises font-weight and is able to switch font files automatically.
 */
export const fontBuilder: (
  baseName: string,
  weightName?: string,
  fontWeight?: string,
  isItalic?: boolean,
  isCondensed?: boolean,
) => TypeStyle = (baseName, weightName, fontWeight, isItalic, isCondensed) => {
  if (isIOS) {
    return {
      fontFamily: getIosFontName(baseName, fontWeight, isItalic, isCondensed),
      fontSize, // All fonts start at default font size. Override where necessary
    };
  } else {
    return {
      fontFamily: `${baseName}-${weightName}`,
      fontSize,
    };
  }
};

export const styledType = (typeStyle: TypeStyle) => {
  if (isIOS) {
    return css`
      font-family: ${typeStyle.fontFamily};
      font-weight: ${typeStyle.fontWeight};
      font-style: ${typeStyle.fontStyle};
      font-size: ${typeStyle.fontSize};
    `;
  } else {
    return css`
      font-family: ${typeStyle.fontFamily};
      font-size: ${typeStyle.fontSize};
    `;
  }
};

export const styledTypeAsStylesheet = (typeStyle: TypeStyle) => {
  if (isIOS) {
    return {
      fontFamily: typeStyle.fontFamily,
      fontWeight: typeStyle.fontWeight,
      fontStyle: typeStyle.fontStyle,
      fontSize: typeStyle.fontSize,
    };
  } else {
    return {
      fontFamily: typeStyle.fontFamily,
      fontSize: typeStyle.fontSize,
    };
  }
};
