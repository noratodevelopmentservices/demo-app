export type Size =
  | 'xSmall'
  | 'small'
  | 'medium'
  | 'large'
  | 'xLarge'
  | 'xxLarge'
  | 'xxxLarge';
export type Sizes = {[key in Size]: number};

export type Color = keyof typeof import('./colors');
export type Colors = {[key in Color]: string};

/**
 * Some components will render on a dark background.
 * Rather than use a dark mode boolean, let's from the
 * start use a type so that later on we can easily
 * extend this type on a component by component basis
 * enabling us to have different color themes without
 * having to refactor anything.
 */
export type BaseColorTheme = 'light' | 'dark';
