import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    darkBackgroundColor: string;
    lightBackgroundColor: string;
    defaultGutterSize: number;
    fontColor: string;
    accentColor: string;
  }
}
