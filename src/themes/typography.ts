import {TextStyle} from 'react-native';

export type TypeStyle = Pick<
  TextStyle,
  'fontWeight' | 'fontFamily' | 'fontStyle' | 'fontSize'
>;

interface RegularTypeWeightStyles {
  bold: TypeStyle; // 700
  regular: TypeStyle; // 400
  light: TypeStyle; // 300
}

interface CondensedypeWeightStyles {
  bold: TypeStyle;
}

interface CondensedypeWeightStyles {
  bold: TypeStyle;
}

interface AlternateTypeWeightStyles {
  bold: TypeStyle; // Condensed gothic no3 considered 'regular'
}

export interface TypeStyles {
  regular: RegularTypeWeightStyles;
}
